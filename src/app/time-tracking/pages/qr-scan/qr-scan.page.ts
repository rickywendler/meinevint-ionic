import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner, SupportedFormat } from '@capacitor-community/barcode-scanner';
import { IonRouterOutlet, Platform } from '@ionic/angular';
import { TimeTrackingProvider } from '../../../../providers/TimeTrackingProvider';
import { Project } from '../../../../models/Project';
import { EmployeeDataProvider } from '../../../../providers/EmployeeDataProvider';
import { AlertProvider } from '../../../../providers/AlertProvider';
import { LanguageProvider } from '../../../../providers/LanguageProvider';

@Component({
	selector: 'app-qr-scan',
	templateUrl: './qr-scan.page.html',
	styleUrls: ['./qr-scan.page.scss'],
})
export class QrScanPage implements OnInit {
	private projects: Project[] = null;

	constructor(
		public _lang: LanguageProvider,
		private _routerOutlet: IonRouterOutlet,
		private _router: Router,
		private platform: Platform,
		private route: ActivatedRoute,
		private tracker: TimeTrackingProvider,
		private alert: AlertProvider,
		private employee: EmployeeDataProvider
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		this.platform.backButton.subscribeWithPriority(10, () => {
			this._router.navigate(['/app/tabs/main-menu']);
		});
		BarcodeScanner.prepare();
	}

	async ionViewDidEnter() {
		this._routerOutlet.swipeGesture = false;

		BarcodeScanner.hideBackground();
		document.querySelector('body').classList.add('scanner-active');

		let success = false;
		await BarcodeScanner.startScanning(
			{
				targetedFormats: [SupportedFormat.QR_CODE],
			},
			async (result, err) => {
				console.log(err);
				console.log(result);
				if (result.hasContent) {
					if (result.content.includes('PJ')) {
						let projectId = +result.content.replace('PJ', '');

						for (let index = 0; index < this.tracker.projects.length; index++) {
							const proj = this.tracker.projects[index];
							if (proj.id == projectId) {
								this.tracker.projectId = proj.id;
								this.tracker.activityId = null;
								success = true;
								this._router.navigate(['/app/tabs/time-tracking/tracker-running']);
							}
						}
					}
					if (!success) {
						await this.alert.ShowInfo(
							this._lang._dict.TimeTracking.QRCode.NoValidQRHeader,
							this._lang._dict.TimeTracking.QRCode.NoValidQRMessage,
							true
						);
						BarcodeScanner.resumeScanning();
					}
				}
			}
		);
	}

	ionViewWillLeave() {
		BarcodeScanner.showBackground();
		document.querySelector('body').classList.remove('scanner-active');
		BarcodeScanner.stopScan();
	}

	ionViewDidLeave() {
		this._routerOutlet.swipeGesture = true;
	}

	cancel() {
		if (this.employee.hasRightForOnlyQR) {
			this._router.navigate(['/app/tabs/main-menu']);
		} else {
			this._router.navigate(['/app/tabs/time-tracking']);
		}
	}
}
