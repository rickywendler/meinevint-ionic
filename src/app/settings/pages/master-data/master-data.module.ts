import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasterDataPageRoutingModule } from './master-data-routing.module';

import { MasterDataPage } from './master-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MasterDataPageRoutingModule
  ],
  declarations: [MasterDataPage]
})
export class MasterDataPageModule {}
