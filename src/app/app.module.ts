import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { ScreenOrientation } from '@awesome-cordova-plugins/screen-orientation/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APIProvider } from '../providers/APIProvider';
import { DataProvider } from '../providers/DataProvider';
import { SettingsProvider } from '../providers/SettingsProvider';
import { LanguageProvider } from '../providers/LanguageProvider';
import { IonicStorageModule } from '@ionic/storage-angular';
import { EmployeeDataProvider } from '../providers/EmployeeDataProvider';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { TouchID } from '@awesome-cordova-plugins/touch-id/ngx';
import { BiometricWrapper } from '@awesome-cordova-plugins/biometric-wrapper/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { AlertProvider } from '../providers/AlertProvider';
import { TimeTrackingProvider } from '../providers/TimeTrackingProvider';
import { CustomTextareaComponent } from './components/custom-textarea/custom-textarea.component';
import { SelectTimePopoverComponent } from './components/select-time-popover/select-time-popover.component';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
	declarations: [AppComponent, CustomTextareaComponent, SelectTimePopoverComponent],
	entryComponents: [],
	imports: [
		BrowserModule,
		IonicModule.forRoot({
			mode: 'ios'
		}),
		AppRoutingModule,
		IonicStorageModule.forRoot(),
		HttpClientModule
	],
	providers: [
		APIProvider,
		TimeTrackingProvider,
		DataProvider,
		SettingsProvider,
		AlertProvider,
		EmployeeDataProvider,
		LanguageProvider,
		AppVersion,
		BarcodeScanner,
		Storage,
		EmailComposer,
		ScreenOrientation,
		HTTP,
		TouchID,
		BiometricWrapper,
		FingerprintAIO,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
	],
	bootstrap: [AppComponent],
	exports: [CustomTextareaComponent]
})
export class AppModule {}
