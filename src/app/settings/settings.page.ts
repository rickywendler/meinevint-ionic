import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { ModalController, NavController, AlertController, ToastController, Platform } from '@ionic/angular';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { SettingsProvider } from '../../providers/SettingsProvider';
import { EmployeeDataProvider } from '../../providers/EmployeeDataProvider';
import { APIProvider } from '../../providers/APIProvider';
import { DsgvoPage } from '../modal-pages/dsgvo/dsgvo.page';
import { DataProvider } from '../../providers/DataProvider';
import { TermsOfUsePage } from '../modal-pages/terms-of-use/terms-of-use.page';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.page.html',
	styleUrls: ['./settings.page.scss']
})
export class SettingsPage implements OnInit {
	_version = '3.0.0';
	_name = '';

	/*
	_countries;
	_knownStatesOfCounty = [203, 50, 220]; // 203 = America, 50 = Germany, 220 = Austrian
	*/

	constructor(
		private _data: DataProvider,
		public _lang: LanguageProvider,
		public _settings: SettingsProvider,
		private _appVersion: AppVersion,
		public _employeeData: EmployeeDataProvider,
		public _api: APIProvider,
		private _modalCon: ModalController,
		private _nav: NavController,
		private _alert: AlertController,
		private _toast: ToastController,
		private _platform: Platform,
		private _emailComposer: EmailComposer
	) {
		this.init();
	}

	private async init() {
		this._appVersion.getVersionNumber().then((data) => {
			this._version = data;
		});

		this._appVersion.getAppName().then((data) => {
			this._name = data;
		});

		await this._api.getEmployeeData();
	}

	ngOnInit() {}

	languageChanged(data) {
		const element = data.target as HTMLIonSelectElement;
		this._settings.currentLanguage = element.value;
	}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('settings-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		await this.platform.ready().then(async () => {
			await this.faio
				.isAvailable()
				.then((element) => {
					this.fingerIsAvailable = true;
					console.log(element);
					this.fingerType = element;
				})
				.catch((element) => {
					this.fingerIsAvailable = false;
					console.log(element);
				});
		});

		console.log('fingerprint:');
		if (this.eAS.useFingerprint == null) {
			this.storage.set('fingerprint', true);
			this.eAS.useFingerprint = true;
		}
		console.log(this.eAS.useFingerprint);
		*/
	}

	contactSupport() {
		let email = { to: 'service@compleet.com', subject: 'mycompleet support request', body: '', isHtml: true };
		this._emailComposer.open(email);
	}

	openIAB(url) {
		// const browser = this.IAB.create(url, "_blank", "location=no");
		const browser = open(url, '_system', 'location=no');
	}

	async ionViewDidEnter() {}

	public async openDSE() {
		const modal = await this._modalCon.create({
			component: DsgvoPage,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();
	}

	public async openTermsOfUse() {
		const modal = await this._modalCon.create({
			component: TermsOfUsePage,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();
	}

	navigateToMasterData() {
		this._nav.navigateForward('/app/tabs/settings/master-data');
	}

	public async logout() {
		const delAlert = await this._alert.create({
			header: this._lang._dict.Settings.Logout,
			message: this._lang._dict.Settings.LogoutMessage,
			buttons: [
				{
					text: this._lang._dict.Continue,
					handler: async () => {
						this._settings.resetSettings();
						await this._data.clearStorage().then(async () => {
							const toast = await this._toast.create({
								message: this._lang._dict.Settings.LogoutSuccessMessage,
								position: 'bottom',
								cssClass: 'success-toast',
								duration: 2000
							});

							await toast.present();
							this._nav.navigateRoot('');
						});
					}
				},
				{
					text: this._lang._dict.Cancel,
					handler: () => {
						delAlert.dismiss();
					}
				}
			]
		});

		await delAlert.present();
	}
}
