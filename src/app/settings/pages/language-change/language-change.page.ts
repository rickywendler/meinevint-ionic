import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
	selector: 'app-language-change',
	templateUrl: './language-change.page.html',
	styleUrls: ['./language-change.page.scss']
})
export class LanguageChangePage implements OnInit {
	private _selectedLanguage = '';

	constructor(
		public _lang: LanguageProvider,
		private _settings: SettingsProvider,
		private _nav: Router,
		private _platform: Platform
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('languageChange-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
*/
		this._selectedLanguage = this._settings.currentLanguage;
	}

	public set selectedLanguage(value: string) {
		this._selectedLanguage = value;
		this.save();
	}

	public get selectedLanguage(): string {
		return this._selectedLanguage;
	}

	save() {
		this._settings.currentLanguage = this._selectedLanguage;

		//this._nav.navigate(['/app/tabs/settings']);
	}
}
