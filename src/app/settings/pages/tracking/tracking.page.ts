import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { DataProvider } from '../../../../providers/DataProvider';
import { Platform } from '@ionic/angular';

@Component({
	selector: 'app-tracking',
	templateUrl: './tracking.page.html',
	styleUrls: ['./tracking.page.scss']
})
export class TrackingPage implements OnInit {
	private _showQRAtStart: boolean = false;

	constructor(
		public _lang: LanguageProvider,
		public _settings: SettingsProvider,
		private _data: DataProvider,
		private _platform: Platform
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('tracking-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/

		this._showQRAtStart = await this._data.getStorage('QRCodeSetting');
	}

	public get showQRAtStart(): boolean {
		return this._showQRAtStart;
	}

	public set showQRAtStart(value: boolean) {
		this._showQRAtStart = value;
		this._data.setStorage('QRCodeSetting', value);
	}
}
