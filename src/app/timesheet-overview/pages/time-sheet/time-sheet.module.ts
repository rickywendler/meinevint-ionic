import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeSheetPageRoutingModule } from './time-sheet-routing.module';

import { TimeSheetPage } from './time-sheet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeSheetPageRoutingModule
  ],
  declarations: [TimeSheetPage]
})
export class TimeSheetPageModule {}
