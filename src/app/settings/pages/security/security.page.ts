import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { Platform } from '@ionic/angular';

@Component({
	selector: 'app-security',
	templateUrl: './security.page.html',
	styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {
	constructor(
		public _lang: LanguageProvider,
		public _settings: SettingsProvider,
		private _platform: Platform
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('security-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/
	}

	public changePW() {
		const browser = open(
			'https://evint.gbg-ag.com/main/module/index.php?evint-login=true&reset=true',
			'_system',
			'location=no'
		);
	}
}
