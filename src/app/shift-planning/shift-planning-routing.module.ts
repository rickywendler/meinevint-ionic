import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShiftPlanningPage } from './shift-planning.page';

const routes: Routes = [
	{
		path: '',
		component: ShiftPlanningPage,
	},
	{
		path: 'select-month',
		loadChildren: () =>
			import('./modals/select-month/select-month.module').then((m) => m.SelectMonthPageModule),
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ShiftPlanningPageRoutingModule {}
