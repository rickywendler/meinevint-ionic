import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DateSelectionPage } from './date-selection.page';

const routes: Routes = [
  {
    path: '',
    component: DateSelectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DateSelectionPageRoutingModule {}
