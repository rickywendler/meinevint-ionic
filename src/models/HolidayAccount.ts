export class HolidayAccount {
	private _year: number;

	private _annualContingent: number;
	private _remainingDays: number;
	private _scheduledDays: number;
	private _approvedDays: number;
	private _availableDays: number;
	private _requestedDays: number;
	private _specialEntriesAmount: number;
	private _takenDays: number;

	constructor(
		year?: number,
		annualContingent?: number,
		scheduledDays?: number,
		approvedDays?: number,
		availableDays?: number,
		requestedDays?: number,
		specialEntriesAmount?: number,
		takenDays?: number,
		remainingDays?: number
	) {
		this._year = year ?? 0;
		this._annualContingent = annualContingent ?? 0.0;
		this._scheduledDays = scheduledDays ?? 0.0;
		this._approvedDays = approvedDays ?? 0.0;
		this._availableDays = availableDays ?? 0.0;
		this._requestedDays = requestedDays ?? 0.0;
		this._specialEntriesAmount = specialEntriesAmount ?? 0.0;
		this._takenDays = takenDays ?? 0.0;
		this._remainingDays = remainingDays ?? 0.0;
	}

	/**
	 * Getter year
	 * @return {number}
	 */
	public get year(): number {
		return this._year;
	}

	/**
	 * Setter year
	 * @param {number } value
	 */
	public set year(value: number) {
		this._year = value;
	}

	/**
	 * Getter annualContingent
	 * @return {number}
	 */
	public get annualContingent(): number {
		return this._annualContingent;
	}

	/**
	 * Setter annualContingent
	 * @param {number } value
	 */
	public set annualContingent(value: number) {
		this._annualContingent = value;
	}

	/**
	 * Getter remainingDays
	 * @return {number}
	 */
	public get remainingDays(): number {
		return this._remainingDays;
	}

	/**
	 * Setter remainingDays
	 * @param {number } value
	 */
	public set remainingDays(value: number) {
		this._remainingDays = value;
	}

	/**
	 * Getter scheduledDays
	 * @return {number}
	 */
	public get scheduledDays(): number {
		return this._scheduledDays;
	}

	/**
	 * Setter scheduledDays
	 * @param {number } value
	 */
	public set scheduledDays(value: number) {
		this._scheduledDays = value;
	}

	/**
	 * Getter approvedDays
	 * @return {number}
	 */
	public get approvedDays(): number {
		return this._approvedDays;
	}

	/**
	 * Setter approvedDays
	 * @param {number } value
	 */
	public set approvedDays(value: number) {
		this._approvedDays = value;
	}

	/**
	 * Getter availableDays
	 * @return {number}
	 */
	public get availableDays(): number {
		return this._availableDays;
	}

	/**
	 * Setter availableDays
	 * @param {number } value
	 */
	public set availableDays(value: number) {
		this._availableDays = value;
	}

	/**
	 * Getter requestedDays
	 * @return {number}
	 */
	public get requestedDays(): number {
		return this._requestedDays;
	}

	/**
	 * Setter requestedDays
	 * @param {number } value
	 */
	public set requestedDays(value: number) {
		this._requestedDays = value;
	}

	/**
	 * Getter specialEntriesAmount
	 * @return {number}
	 */
	public get specialEntriesAmount(): number {
		return this._specialEntriesAmount;
	}

	/**
	 * Setter specialEntriesAmount
	 * @param {number } value
	 */
	public set specialEntriesAmount(value: number) {
		this._specialEntriesAmount = value;
	}

	/**
	 * Getter takenDays
	 * @return {number}
	 */
	public get takenDays(): number {
		return this._takenDays;
	}

	/**
	 * Setter takenDays
	 * @param {number } value
	 */
	public set takenDays(value: number) {
		this._takenDays = value;
	}
}
