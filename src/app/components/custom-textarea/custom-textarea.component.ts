import {
	Component,
	Directive,
	OnInit,
	ViewChild,
	ElementRef,
	Input,
	Output,
	EventEmitter,
} from '@angular/core';

@Component({
	selector: 'custom-textarea',
	templateUrl: './custom-textarea.component.html',
	styleUrls: ['./custom-textarea.component.scss'],
})
export class CustomTextareaComponent implements OnInit {
	@ViewChild('Input') input: ElementRef;

	@Input() autoFocus: boolean = false;
	@Input() autoGrow: boolean = true;
	@Input() rows: number = 1;
	@Input() name: string = 'custom-textarea';

	private _value: string = '';
	@Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

	public isFocused = false;

	private _input: HTMLTextAreaElement = null;

	constructor() {}

	ngOnInit() {}

	ngAfterViewInit() {
		if (this.autoGrow as unknown as boolean) {
			this._input = this.input.nativeElement as HTMLTextAreaElement;

			this.calculateTextareaHeight();
		}
	}

	/**
	 * Method to indicate that the textarea is focused
	 * @param isFocused boolean if the textarea is focused
	 */
	focusChanged(isFocused: boolean) {
		if (isFocused) {
			this.isFocused = true;
		} else {
			this.isFocused = false;
		}
	}

	/**
	 * Method for changed event
	 * @param e event
	 */
	modelChanged(e) {
		this.value = e.target.value;
		this.valueChange.emit(this.value);

		if (this.autoGrow) {
			this.calculateTextareaHeight();
		}
	}

	/**
	 * Method to calculate the height of the textarea, based on the text which is inside
	 */
	private calculateTextareaHeight() {
		const { style } = this._input;
		style.height = style.minHeight = 'auto';
		style.minHeight = `${Math.min(
			this._input.scrollHeight,
			parseInt(this._input.style.maxHeight)
		)}px`;
		style.height = `${this._input.scrollHeight}px`;
	}

	/**
	 * Variable for Binding
	 */
	@Input() public get value(): string {
		return this._value;
	}

	public set value(val: string) {
		this._value = val;
	}
}
