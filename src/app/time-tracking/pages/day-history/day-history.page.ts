import { Component, Input, OnInit } from '@angular/core';
import { Booking } from 'src/models/Booking';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { APIProvider } from '../../../../providers/APIProvider';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { DateTime } from 'luxon';

@Component({
	selector: 'app-day-history',
	templateUrl: './day-history.page.html',
	styleUrls: ['./day-history.page.scss']
})
export class DayHistoryPage implements OnInit {
	public _timeRecords: Booking[] = [];

	constructor(public _lang: LanguageProvider, private _api: APIProvider, public _settings: SettingsProvider) {}

	ngOnInit(): void {
		this._api.getBookings(DateTime.now().setZone(this._settings.timeZone)).then((result) => {
			this._timeRecords = result;
		});
	}
}
