import { Injectable, Inject } from '@angular/core';
import { DateTime } from 'luxon';
import { SettingsProvider } from './SettingsProvider';
import { APIProvider } from './APIProvider';
import { TrackingStamp } from '../models/TrackingStamp';
import { v4 as uuidv4 } from 'uuid';
import { EmployeeDataProvider } from './EmployeeDataProvider';
import { Project } from 'src/models/Project';
import { Activity } from '../models/Activity';
import { Booking } from '../models/Booking';
import { DataProvider } from './DataProvider';

@Injectable()
export class TimeTrackingProvider {
	private _id: string = null;
	private _isPause: boolean = false;
	private _start: DateTime = null;
	private _end: DateTime = null;
	private _duration: number = null;
	private _projectId: number = null;
	private _activityId: number = null;
	private _isTracking: boolean = false;

	private _trackingInterval = null;
	private _breakInterval = null;
	private _startHasBeenSet = false;
	private _onlyQrIsUsed: boolean;

	private _projects: Project[] = [];
	private _activities: Activity[] = [];

	private _bookings: Booking[] = [];
	private _bookingWorkTime: number = 0;
	private _bookingBreakTime: number = 0;

	public _startIsSaved: boolean = false;

	constructor(
		private _settings: SettingsProvider,
		private _api: APIProvider,
		private _employee: EmployeeDataProvider,
		private data: DataProvider
	) {}

	public async init() {
		await this.updateBookings();
	}

	public startTracking(save = true, createId = true) {
		if (this._breakInterval != null) {
			this.stopBreak();
		}
		if (this._trackingInterval != null) {
			this.stopTracking(false);
		}

		if (createId) {
			this._id = uuidv4();
		}
		if (!this._startHasBeenSet) {
			this._start = DateTime.now().setZone(this._settings.timeZone).setLocale('de');
		} else {
			this._startHasBeenSet = false;
		}
		this._end = null;
		this._isTracking = true;
		this._trackingInterval = setInterval(() => {
			this._duration = -this._start.diffNow('milliseconds', {
				zone: this._settings.timeZone,
				locale: 'de'
			}).milliseconds;
		}, 100);
		this.data.setStorage('breakStart', null);

		if (save) {
			this._api.saveTrackingStamp(new TrackingStamp(this._id, this._start, null, this._projectId, this._activityId));
			this._startIsSaved = true;
		}
	}

	public async stopTracking(saveTrackingStampDirectly = true) {
		clearInterval(this._trackingInterval);
		this._trackingInterval = null;
		this._end = DateTime.now().setZone(this._settings.timeZone).setLocale('de');
		this._isTracking = false;
		this._duration = 0;
		this.data.setStorage('breakStart', null);
		if (saveTrackingStampDirectly) {
			// USE THIS CODE IF THE START STAMP IS RECORDED TWICE IN EVINT
			if (this._startIsSaved) {
				this._bookings = await this._api.getBookings(new Date());
				let b = new Booking();
				for (let i in this._bookings) {
					if (!this._bookings[i].end) {
						b = this._bookings[i];
						this._api.deleteTimestamp(b);
						break;
					}
				}
				this._startIsSaved = false;
			}

			if (!this._start) {
				this._api.getTrackingStampFromStorage().then((stamps) => {
					if (stamps.length > 0) {
						let latestStamp = stamps[0];
						stamps.forEach((stamp) => {
							if (stamp.started > latestStamp.started && stamp.stopped == null) {
								latestStamp = stamp;
							}
						});
						this._start = latestStamp.started;
					}
				});
				/*
				if (this.start == null) {
					this._bookings = await this._api.getBookings(new Date());
					for (let i in this._bookings) {
						if (!this._bookings[i].end) {
							this._start = this._bookings[i].start;
							break;
						}
					}
				}
				*/
			}
			this._api.saveTrackingStamp(
				new TrackingStamp(this._id, this._start, this._end, this._projectId, this._activityId)
			);

			await this.updateBookings();
		}
	}

	public async startBreak(breakStart: DateTime = null) {
		if (breakStart == null) {
			await this.stopTracking();
		}
		this._isPause = true;
		this._isTracking = false;
		if (breakStart) {
			this._start = breakStart;
		} else {
			this._start = DateTime.now().setZone(this._settings.timeZone).setLocale('de');
		}
		this._isPause = true;
		this._breakInterval = setInterval(() => {
			this._duration = -this._start.diffNow('milliseconds', {
				zone: this._settings.timeZone,
				locale: 'de'
			}).milliseconds;
		}, 100);

		this.data.setStorage('breakStart', this._start.toISO());
	}

	public stopBreak(startTrackerAfterBreak = false) {
		clearInterval(this._breakInterval);
		this._breakInterval = null;
		this._isPause = false;

		this.data.setStorage('breakStart', null);
		if (startTrackerAfterBreak) {
			this.startTracking();
			this.updateBookings();
		} else {
			this._end = DateTime.fromObject(
				{},
				{
					zone: this._settings.timeZone,
					locale: 'de'
				}
			);
		}
	}

	/**
	 * Getter id
	 * @return {string }
	 */
	public get id(): string {
		return this._id;
	}

	/**
	 * Getter isPause
	 * @return {boolean }
	 */
	public get isPause(): boolean {
		return this._isPause;
	}

	/**
	 * Getter start
	 * @return {DateTime }
	 */
	public get start(): DateTime {
		return this._start;
	}

	/**
	 * Getter end
	 * @return {DateTime }
	 */
	public get end(): DateTime {
		return this._end;
	}

	/**
	 * Getter duration
	 * @return {number }
	 */
	public get duration(): number {
		return this._duration;
	}

	/**
	 * Getter projectId
	 * @return {number }
	 */
	public get projectId(): number {
		return this._projectId;
	}

	/**
	 * Getter activityId
	 * @return {number }
	 */
	public get activityId(): number {
		return this._activityId;
	}

	/**
	 * Getter isTracking
	 * @return {boolean }
	 */
	public get isTracking(): boolean {
		return this._isTracking;
	}

	/**
	 * Setter id
	 * @param {string } value
	 */
	public set id(value: string) {
		this._id = value;
	}

	/**
	 * Setter isPause
	 * @param {boolean } value
	 */
	public set isPause(value: boolean) {
		this._isPause = value;
	}

	/**
	 * Setter start
	 * @param {DateTime } value
	 */
	public set start(value: DateTime) {
		this._start = value;
		this._startHasBeenSet = true;
	}

	/**
	 * Setter end
	 * @param {DateTime } value
	 */
	public set end(value: DateTime) {
		this._end = value;
	}

	/**
	 * Setter duration
	 * @param {number } value
	 */
	public set duration(value: number) {
		this._duration = value;
	}

	/**
	 * Setter projectId
	 * @param {number } value
	 */
	public set projectId(value: number) {
		this._projectId = value;
	}

	/**
	 * Setter activityId
	 * @param {number } value
	 */
	public set activityId(value: number) {
		this._activityId = value;
	}

	/**
	 * Getter onlyQrIsUsed
	 * @return {boolean }
	 */
	public get onlyQrIsUsed(): boolean {
		if (this._onlyQrIsUsed == null) {
			this.onlyQrIsUsed = this._employee.hasRightForOnlyQR;
		}
		return this._onlyQrIsUsed;
	}

	/**
	 * Setter onlyQrIsUsed
	 * @param {boolean } value
	 */
	public set onlyQrIsUsed(value: boolean) {
		this._onlyQrIsUsed = value;
	}

	/**
	 * Getter projects
	 * @return {Project[] }
	 */
	public get projects(): Project[] {
		return this._projects;
	}

	/**
	 * Setter projects
	 * @param {Project[] } value
	 */
	public set projects(value: Project[]) {
		this._projects = value;
	}

	/**
	 * Getter activities
	 * @return {Activity[] }
	 */
	public get activities(): Activity[] {
		return this._activities;
	}

	/**
	 * Setter activities
	 * @param {Activity[] } value
	 */
	public set activities(value: Activity[]) {
		this._activities = value;
	}

	/**
	 * Getter totalWorkTime
	 * @return {number }
	 */
	public get totalWorkTime(): number {
		if (this.isTracking && !this.isPause) return this._bookingWorkTime + this._duration;
		else return this._bookingWorkTime;
	}

	public get totalBreakTime(): number {
		if (this.isTracking && this.isPause) return this._bookingBreakTime + this._duration;
		else return this._bookingBreakTime;
	}

	private async updateBookings() {
		this._bookings = await this._api.getBookings(new Date(), false);
		this._bookingWorkTime = 0;
		this._bookingBreakTime = 0;

		this._bookings.forEach((booking) => {
			if (!booking.isPauseEntry) {
				if (booking.end && booking.start) {
					this._bookingWorkTime += booking.duration;
				}
			} else this._bookingBreakTime += booking.duration;
		});
	}
}
