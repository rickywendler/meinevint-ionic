import { Injectable, Inject } from '@angular/core';
import { DataProvider } from './DataProvider';
import { LanguageProvider } from './LanguageProvider';
import { TrackingStamp } from '../models/TrackingStamp';
import { Platform } from '@ionic/angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

@Injectable()
export class SettingsProvider {
	private _isPlatformReady = false;
	private _offlineMode = false;
	public _isIOS = false;
	public _isAndroid = false;

	private _fingerprintIsAvailable: boolean = null;
	private _fingerType = 'biometric';
	private _useFingerprint = false;
	private _showTimeWhileTimeTracking = false;
	private _currentLanguage = 'german';
	private _hasAllowedNotifs = false;
	private _hasAllowedCamera = false;
	private _hasSeenPrivacyNotif = false;
	private _timeZone: string = 'Europe/Berlin';

	constructor(
		private _data: DataProvider,
		private _lang: LanguageProvider,
		private _platform: Platform,
		private _fingerPrint: FingerprintAIO
	) {}

	public async init() {
		this._platform.ready().then(() => {
			this._isPlatformReady = true;
		});

		this._data.getStorage('currentLanguage').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('currentLanguage', this._currentLanguage);
			} else {
				this._currentLanguage = result;
				this._lang.setLanguage(result);
			}
		});

		this._data.getStorage('showTimeWhileTimeTracking').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('showTimeWhileTimeTracking', this._showTimeWhileTimeTracking);
			} else {
				this._showTimeWhileTimeTracking = result;
			}
		});

		this._data.getStorage('useFingerprint').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('useFingerprint', this._useFingerprint);
			} else {
				this._useFingerprint = result;
			}
		});

		this._data.getStorage('hasAllowedCamera').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('hasAllowedCamera', this._hasAllowedCamera);
			} else {
				this._hasAllowedCamera = result;
			}
		});

		this._data.getStorage('hasAllowedNotifs').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('hasAllowedNotifs', this._hasAllowedNotifs);
			} else {
				this._hasAllowedNotifs = result;
			}
		});

		this._data.getStorage('hasSeenPrivacyNotif').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('hasSeenPrivacyNotif', this._hasSeenPrivacyNotif);
			} else {
				this._hasSeenPrivacyNotif = result;
			}
		});

		this._data.getStorage('fingerType').then((result) => {
			if (result === undefined || result == null) {
				this._data.setStorage('fingerType', this._fingerType);
			} else {
				this._fingerType = result;
			}
		});

		if (this._platform.is('ios')) {
			this._isIOS = true;
			this._isAndroid = false;
		} else {
			this._isIOS = false;
			this._isAndroid = true;
		}
	}

	public get hasAllowedCamera(): boolean {
		return this._hasAllowedCamera;
	}

	public set hasAllowedCamera(value: boolean) {
		this._hasAllowedCamera = value;
		this._data.setStorage('hasAllowedCamera', value);
	}

	public get hasAllowedNotifs(): boolean {
		return this._hasAllowedNotifs;
	}

	public set hasAllowedNotifs(value: boolean) {
		this._hasAllowedNotifs = value;
		this._data.setStorage('hasAllowedNotifs', value);
	}

	public get useFingerprint(): boolean {
		return this._useFingerprint;
	}

	public set useFingerprint(value: boolean) {
		this._useFingerprint = value;
		this._data.setStorage('useFingerprint', value);
	}

	public get showTimeWhileTimeTracking(): boolean {
		return this._showTimeWhileTimeTracking;
	}

	public set showTimeWhileTimeTracking(value: boolean) {
		this._showTimeWhileTimeTracking = value;
		this._data.setStorage('showTimeWhileTimeTracking', value);
	}

	public get currentLanguage(): string {
		return this._currentLanguage;
	}

	public set currentLanguage(value: string) {
		this._currentLanguage = value;
		this._data.setStorage('currentLanguage', value);
		this._lang.setLanguage(value);
	}

	public get hasSeenPrivacyNotif(): boolean {
		return this._hasSeenPrivacyNotif;
	}

	public set hasSeenPrivacyNotif(value: boolean) {
		this._hasSeenPrivacyNotif = value;
		this._data.setStorage('hasSeenPrivacyNotif', value);
	}

	public get timeZone(): string {
		return this._timeZone;
	}

	public set timeZone(value: string) {
		this._timeZone = value;
		TrackingStamp.timeZone = value;
		this._data.setStorage('timeZone', value);
	}

	public get fingerprintIsAvailable(): boolean {
		if (this._isPlatformReady) {
			if (this._fingerprintIsAvailable == null) {
				this._fingerPrint
					.isAvailable()
					.then((element) => {
						this._fingerprintIsAvailable = true;
						this.fingerType = element;
					})
					.catch(() => {
						this._fingerprintIsAvailable = false;
					});
			} else {
				return this._fingerprintIsAvailable;
			}
		} else {
			return false;
		}
	}

	public get fingerType(): string {
		return this._fingerType;
	}

	public set fingerType(value: string) {
		this._fingerType = value;
		this._data.setStorage('fingerType', value);
	}

	public resetSettings() {
		this._fingerprintIsAvailable = null;
		this._fingerType = 'biometric';
		this._useFingerprint = false;
		this._showTimeWhileTimeTracking = false;
		this._currentLanguage = 'german';
		this._hasAllowedNotifs = false;
		this._hasAllowedCamera = false;
		this._hasSeenPrivacyNotif = false;
		this._timeZone = 'Europe/Berlin';
	}

	public get offlineMode(): boolean {
		return this._offlineMode;
	}

	public set offlineMode(value: boolean) {
		this._offlineMode = value;
	}
}
