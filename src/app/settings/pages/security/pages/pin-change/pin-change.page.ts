import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../../../providers/LanguageProvider';
import { DataProvider } from '../../../../../../providers/DataProvider';
import { Router } from '@angular/router';
import { ToastController, Platform } from '@ionic/angular';

@Component({
	selector: 'app-pin-change',
	templateUrl: './pin-change.page.html',
	styleUrls: ['./pin-change.page.scss']
})
export class PinChangePage implements OnInit {
	private _oldPin: number = null;
	private _newPin: number = null;
	private _newPinRepeated: number = null;
	private _localPin: string = '';

	constructor(
		public _lang: LanguageProvider,
		private _data: DataProvider,
		private _nav: Router,
		private _toast: ToastController,
		private _platform: Platform
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('pinChange-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/
		this._localPin = await this._data.getStorage('localpw');
	}

	async saveNewPin() {
		let errorOccured = false;

		const old = (this._oldPin ?? '') + '';
		const newPin = (this._newPin ?? '') + '';
		const repeat = (this._newPinRepeated ?? '') + '';

		if (old.length != 4 || old != this._localPin) {
			errorOccured = true;
			const toast = await this._toast.create({
				message: this._lang._dict.Settings.SecurityPage.PinPage.ToastMessageOldNotCorrect,
				position: 'bottom',
				cssClass: 'fail-toast',
				duration: 2000
			});

			await toast.present();
		}

		if ((newPin.length != 4 || repeat.length != 4) && !errorOccured) {
			errorOccured = true;
			const toast = await this._toast.create({
				message: this._lang._dict.Settings.SecurityPage.PinPage.ToastMessagePinNotLongEnough,
				position: 'bottom',
				cssClass: 'fail-toast',
				duration: 2000
			});

			await toast.present();
		}
		if (newPin != repeat && !errorOccured) {
			errorOccured = true;
			const toast = await this._toast.create({
				message: this._lang._dict.Settings.SecurityPage.PinPage.ToastMessagePinsAreNotMatching,
				position: 'bottom',
				cssClass: 'fail-toast',
				duration: 2000
			});

			await toast.present();
		}

		if (!errorOccured) {
			this._data.setStorage('localpw', this._newPin + '');
			const toast = await this._toast.create({
				message: this._lang._dict.Settings.SecurityPage.PinPage.ToastMessagePinSuccessfullySet,
				position: 'bottom',
				cssClass: 'success-toast',
				duration: 2000
			});

			await toast.present();
			this._nav.navigate(['/app/tabs/settings/security']);
		}
	}

	async onInput(event) {
		const element = event.target;
		let val = element.value.toString();
		if (val.length > 3) {
			element.value = val.substring(0, 4);

			const name = element.name;
			switch (name) {
				case 'oldPin':
					(document.getElementsByName('newPin')[0] as HTMLIonInputElement).setFocus();
					break;
				case 'newPin':
					(document.getElementsByName('newPinRepeated')[0] as HTMLIonInputElement).setFocus();
					break;
				case 'newPinRepeated':
					this.saveNewPin();
					break;
			}
		}
	}

	/**
	 * Getter oldPin
	 * @return {number}
	 */
	public get oldPin(): number {
		return this._oldPin;
	}

	/**
	 * Getter newPin
	 * @return {number}
	 */
	public get newPin(): number {
		return this._newPin;
	}

	/**
	 * Getter newPinRepeated
	 * @return {number}
	 */
	public get newPinRepeated(): number {
		return this._newPinRepeated;
	}

	/**
	 * Setter oldPin
	 * @param {number} value
	 */
	public set oldPin(value: number) {
		this._oldPin = value;
	}

	/**
	 * Setter newPin
	 * @param {number} value
	 */
	public set newPin(value: number) {
		this._newPin = value;
	}

	/**
	 * Setter newPinRepeated
	 * @param {number} value
	 */
	public set newPinRepeated(value: number) {
		this._newPinRepeated = value;
	}
}
