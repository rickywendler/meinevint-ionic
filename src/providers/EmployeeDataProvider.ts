import { Injectable, Inject } from '@angular/core';
import { Activity } from 'src/models/Activity';
import { Project } from 'src/models/Project';
import { DataProvider } from './DataProvider';
import * as utf8 from 'utf8';

/**
 * This file will be used for data collection of the employee
 */
@Injectable()
export class EmployeeDataProvider {
	private _id: number;
	private _pnr: string;
	private _username: string;
	private _firstname: string = 'Vorname';
	private _lastname: string = 'Nachname';

	private _website: string;
	private _mail: string;
	private _adressOne: string;
	private _adressTwo: string;
	private _city;
	private _country;
	private _plz;
	private _state;

	private _landlineCountry: string;
	private _landlineNumber: number;
	private _mobilCountry: number;
	private _mobilNumber: number;

	private _projects: Project[];
	private _activities: Activity[];

	private _defaultActivityId: number;
	private _defaultProjectId: number;

	private _hasRightForTimeTracking: boolean;
	private _hasRightForTimeSheet: boolean;
	private _hasRightForHolidayAccount: boolean;
	private _hasRightForWorkTimeAccount: boolean;
	private _hasRightForEditTimeSheet: boolean;
	private _hasRightForDescriptionTimeSheet: boolean;
	private _hasRightForMonthOverview: boolean;
	private _hasRightForShiftPlan: boolean;
	private _hasRightForAllowancesOverview: boolean;
	private _hasRightForMessagesToSuperior: boolean;

	private _hasRightForOnlyQR: boolean;

	constructor(private _data: DataProvider) {}

	public get initials(): string {
		return this._firstname.charAt(0) + this._lastname.charAt(0);
	}

	public get id(): number {
		return this._id;
	}

	public set id(value: number) {
		this._id = value;

		this._data.setStorage('employeeId', value);
	}

	public get pnr(): string {
		return this._pnr;
	}

	public set pnr(value: string) {
		this._pnr = value;

		this._data.setStorage('employeePnr', value);
	}

	public get username(): string {
		return this._username;
	}

	public set username(value: string) {
		this._username = value;

		this._data.setStorage('employeeUsername', value);
	}

	public get firstname(): string {
		return this._firstname;
	}

	public set firstname(value: string) {
		this._firstname = utf8.decode(value);

		this._data.setStorage('employeeFirstname', value);
	}

	public get lastname(): string {
		return this._lastname;
	}

	public set lastname(value: string) {
		this._lastname = utf8.decode(value);

		this._data.setStorage('employeeLastname', value);
	}

	public get defaultActivityId(): number {
		return this._defaultActivityId;
	}

	public set defaultActivityId(value: number) {
		this._defaultActivityId = value;

		this._data.setStorage('employeeDefaultActivity', value);
	}

	public get defaultProjectId(): number {
		return this._defaultProjectId;
	}

	public set defaultProjectId(value: number) {
		this._defaultProjectId = value;

		this._data.setStorage('employeeDefaultProjectId', value);
	}

	public get website(): string {
		return this._website;
	}

	public set website(value: string) {
		this._website = value;

		this._data.setStorage('employeeWebsite', value);
	}

	public get mail(): string {
		return this._mail;
	}

	public set mail(value: string) {
		this._mail = value;

		this._data.setStorage('employeeMail', value);
	}

	public get adressOne(): string {
		return this._adressOne;
	}

	public set adressOne(value: string) {
		this._adressOne = value;

		this._data.setStorage('employeeAdressOne', value);
	}

	public get adressTwo(): string {
		return this._adressTwo;
	}

	public set adressTwo(value: string) {
		this._adressTwo = value;

		this._data.setStorage('employeeAdressTwo', value);
	}

	public get city() {
		return this._city;
	}

	public set city(value) {
		this._city = value;

		this._data.setStorage('employeeCity', value);
	}

	public get country() {
		return this._country;
	}

	public set country(value) {
		this._country = value;

		this._data.setStorage('employeeCountry', value);
	}

	public get plz() {
		return this._plz;
	}

	public set plz(value) {
		this._plz = value;

		this._data.setStorage('employeePlz', value);
	}

	public get state() {
		return this._state;
	}

	public set state(value) {
		this._state = value;

		this._data.setStorage('employeeState', value);
	}

	/**
	 * Getter hasRightForTimeTracking
	 * @return {boolean}
	 */
	public get hasRightForTimeTracking(): boolean {
		return this._hasRightForTimeTracking;
	}

	/**
	 * Getter hasRightForTimeSheet
	 * @return {boolean}
	 */
	public get hasRightForTimeSheet(): boolean {
		return this._hasRightForTimeSheet;
	}

	/**
	 * Getter hasRightForHolidayAccount
	 * @return {boolean}
	 */
	public get hasRightForHolidayAccount(): boolean {
		return this._hasRightForHolidayAccount;
	}

	/**
	 * Getter hasRightForWorkTimeAccount
	 * @return {boolean}
	 */
	public get hasRightForWorkTimeAccount(): boolean {
		return this._hasRightForWorkTimeAccount;
	}

	/**
	 * Setter hasRightForTimeTracking
	 * @param {boolean} value
	 */
	public set hasRightForTimeTracking(value: boolean) {
		this._hasRightForTimeTracking = value;

		this._data.setStorage('hasRightForTimeTracking', value);
	}

	/**
	 * Setter hasRightForTimeSheet
	 * @param {boolean} value
	 */
	public set hasRightForTimeSheet(value: boolean) {
		this._hasRightForTimeSheet = value;

		this._data.setStorage('hasRightForTimeSheet', value);
	}

	/**
	 * Setter hasRightForHolidayAccount
	 * @param {boolean} value
	 */
	public set hasRightForHolidayAccount(value: boolean) {
		this._hasRightForHolidayAccount = value;

		this._data.setStorage('hasRightForHolidayAccount', value);
	}

	/**
	 * Setter hasRightForWorkTimeAccount
	 * @param {boolean} value
	 */
	public set hasRightForWorkTimeAccount(value: boolean) {
		this._hasRightForWorkTimeAccount = value;

		this._data.setStorage('hasRightForWorkTimeAccount', value);
	}

	/**
	 * Getter hasRightForEditTimeSheet
	 * @return {boolean}
	 */
	public get hasRightForEditTimeSheet(): boolean {
		return this._hasRightForEditTimeSheet;
	}

	/**
	 * Getter hasRightForDescriptionTimeSheet
	 * @return {boolean}
	 */
	public get hasRightForDescriptionTimeSheet(): boolean {
		return this._hasRightForDescriptionTimeSheet;
	}

	/**
	 * Getter hasRightForMonthOverview
	 * @return {boolean}
	 */
	public get hasRightForMonthOverview(): boolean {
		return this._hasRightForMonthOverview;
	}

	/**
	 * Getter hasRightForShiftPlan
	 * @return {boolean}
	 */
	public get hasRightForShiftPlan(): boolean {
		return this._hasRightForShiftPlan;
	}

	/**
	 * Getter hasRightForAllowancesOverview
	 * @return {boolean}
	 */
	public get hasRightForAllowancesOverview(): boolean {
		return this._hasRightForAllowancesOverview;
	}

	/**
	 * Getter hasRightForMessagesToSuperior
	 * @return {boolean}
	 */
	public get hasRightForMessagesToSuperior(): boolean {
		return this._hasRightForMessagesToSuperior;
	}

	/**
	 * Setter hasRightForEditTimeSheet
	 * @param {boolean} value
	 */
	public set hasRightForEditTimeSheet(value: boolean) {
		this._hasRightForEditTimeSheet = value;

		this._data.setStorage('hasRightForEditTimeSheet', value);
	}

	/**
	 * Setter hasRightForDescriptionTimeSheet
	 * @param {boolean} value
	 */
	public set hasRightForDescriptionTimeSheet(value: boolean) {
		this._hasRightForDescriptionTimeSheet = value;

		this._data.setStorage('hasRightForDescriptionTimeSheet', value);
	}

	/**
	 * Setter hasRightForMonthOverview
	 * @param {boolean} value
	 */
	public set hasRightForMonthOverview(value: boolean) {
		this._hasRightForMonthOverview = value;

		this._data.setStorage('hasRightForMonthOverview', value);
	}

	/**
	 * Setter hasRightForShiftPlan
	 * @param {boolean} value
	 */
	public set hasRightForShiftPlan(value: boolean) {
		this._hasRightForShiftPlan = value;

		this._data.setStorage('hasRightForShiftPlan', value);
	}

	/**
	 * Setter hasRightForAllowancesOverview
	 * @param {boolean} value
	 */
	public set hasRightForAllowancesOverview(value: boolean) {
		this._hasRightForAllowancesOverview = value;

		this._data.setStorage('hasRightForAllowancesOverview', value);
	}

	/**
	 * Setter hasRightForMessagesToSuperior
	 * @param {boolean} value
	 */
	public set hasRightForMessagesToSuperior(value: boolean) {
		this._hasRightForMessagesToSuperior = value;

		this._data.setStorage('hasRightForMessagesToSuperior', value);
	}

	/**
	 * Getter hasRightForOnlyQR
	 * @return {boolean }
	 */
	public get hasRightForOnlyQR(): boolean {
		return this._hasRightForOnlyQR;
	}

	/**
	 * Setter hasRightForOnlyQR
	 * @param {boolean } value
	 */
	public set hasRightForOnlyQR(value: boolean) {
		this._hasRightForOnlyQR = value;

		this._data.setStorage('hasRightForOnlyQR', value);
	}

	/**
	 * Getter projects
	 * @return {Project[]}
	 */
	public get projects(): Project[] {
		return this._projects;
	}

	/**
	 * Setter projects
	 * @param {Project[]} value
	 */
	public set projects(value: Project[]) {
		this._projects = value;
	}

	/**
	 * Getter activities
	 * @return {Activity[]}
	 */
	public get activities(): Activity[] {
		return this._activities;
	}

	/**
	 * Setter activities
	 * @param {Activity[]} value
	 */
	public set activities(value: Activity[]) {
		this._activities = value;
	}

	/**
	 * Method to get a single project by its id
	 * @param id id of the project
	 * @returns the found project with the given id. otherwise undefined
	 */
	public getProjectById(id: number): Project | undefined {
		return this._projects.find((p) => p.id == id);
	}

	/**
	 * Method to get a single activity by its id
	 * @param id of the activity
	 * @returns the found activity with the given id. otherwise undefined
	 */
	public getActivityById(id: number): Activity | undefined {
		return this._activities.find((a) => a.id == id);
	}

	/**
	 * Method to get a list of activities for the given project id
	 * @param projectId the id of the project by which the activities should be filterd
	 * @returns Return an array of activities which are assigned to the given projectId
	 */
	public activitiesByProject(projectId: number = 0): Activity[] {
		return this._activities.filter((value) => {
			value.projectId == projectId;
		});
	}

	/**
	 * Getter landlineCountry
	 * @return {string}
	 */
	public get landlineCountry(): string {
		return this._landlineCountry;
	}

	/**
	 * Setter landlineCountry
	 * @param {string} value
	 */
	public set landlineCountry(value: string) {
		this._landlineCountry = value;
	}

	/**
	 * Setter landlineNumber
	 * @param {number} value
	 */
	public set landlineNumber(value: number) {
		this._landlineNumber = value;
	}

	/**
	 * Getter landlineNumber
	 * @return {number}
	 */
	public get landlineNumber(): number {
		return this._landlineNumber;
	}

	/**
	 * Getter mobilCountry
	 * @return {number}
	 */
	public get mobilCountry(): number {
		return this._mobilCountry;
	}

	/**
	 * Setter mobilCountry
	 * @param {number} value
	 */
	public set mobilCountry(value: number) {
		this._mobilCountry = value;
	}

	/**
	 * Getter mobilNumber
	 * @return {number}
	 */
	public get mobilNumber(): number {
		return this._mobilNumber;
	}

	/**
	 * Setter mobilNumber
	 * @param {number} value
	 */
	public set mobilNumber(value: number) {
		this._mobilNumber = value;
	}
}
