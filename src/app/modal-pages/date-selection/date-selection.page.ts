import { Component, Input, OnInit } from '@angular/core';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-date-selection',
	templateUrl: './date-selection.page.html',
	styleUrls: ['./date-selection.page.scss']
})
export class DateSelectionPage implements OnInit {
	@Input() _multi: boolean = false;
	@Input() _setStart: boolean = true;
	@Input() _selectedEnd: DateTime = null;
	@Input() _selectedStart: DateTime = null;

	public _multiSelection: DateTime[] = [];
	public _today = DateTime.now().setZone(this._settings.timeZone);
	public _nextYear = DateTime.now().plus({ years: 1 }).endOf('year').toISO();

	constructor(
		private _settings: SettingsProvider,
		public _lang: LanguageProvider,
		private _modalCon: ModalController
	) {}

	ngOnInit() {}

	public endChanged(ev) {
		if (ev.detail.value != null) {
			this._selectedEnd = DateTime.fromISO(ev.detail.value);
			//if (this.startAndEndAreSameDate) {
			//	this._lastDayComplete = this._firstDayComplete;
			//}
		}
	}

	public startChanged(ev) {
		if (ev.detail.value != null) {
			this._selectedStart = DateTime.fromISO(ev.detail.value);
			//if (this.startAndEndAreSameDate) {
			//	this._lastDayComplete = this._firstDayComplete;
			//}
		}
	}

	public multiChanged(ev) {
		if (ev.detail.value != null) {
			this._multiSelection = [];
			ev.detail.value.forEach((element) => {
				this._multiSelection.push(DateTime.fromISO(element));
			});
		}
	}

	closeModal() {
		this._modalCon.dismiss({ success: false });
	}

	saveDate() {
		if (this._multi) {
			this._modalCon.dismiss({
				success: true,
				_multiSelection: this._multiSelection
			});
		} else {
			this._modalCon.dismiss({
				success: true,
				_selectedEnd: this._selectedEnd,
				_selectedStart: this._selectedStart
			});
		}
	}
}
