import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PinChangePage } from './pin-change.page';

const routes: Routes = [
  {
    path: '',
    component: PinChangePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PinChangePageRoutingModule {}
