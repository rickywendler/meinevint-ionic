import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SettingsProvider } from 'src/providers/SettingsProvider';
import { LanguageProvider } from '../../../providers/LanguageProvider';

@Component({
	selector: 'app-select-language',
	templateUrl: './select-language.page.html',
	styleUrls: ['./select-language.page.scss']
})
export class SelectLanguagePage implements OnInit {
	private _selectedLanguage = '';

	constructor(private modalCon: ModalController, public lang: LanguageProvider, private settings: SettingsProvider) {}

	ngOnInit() {}

	// dismiss modal
	dismiss() {
		// using the injected ModalController this page
		// can "dismiss" itself and optionally pass back data
		this.modalCon.dismiss({
			dismissed: true
		});
	}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('languageChange-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
*/
		this._selectedLanguage = this.settings.currentLanguage;
	}

	public set selectedLanguage(value: string) {
		this._selectedLanguage = value;
	}

	public get selectedLanguage(): string {
		return this._selectedLanguage;
	}

	save(closeModal = false) {
		this.settings.currentLanguage = this._selectedLanguage;

		if (closeModal) {
			this.dismiss();
		}
		//this._nav.navigate(['/app/tabs/settings']);
	}
}
