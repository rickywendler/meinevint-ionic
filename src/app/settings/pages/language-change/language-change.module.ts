import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LanguageChangePageRoutingModule } from './language-change-routing.module';

import { LanguageChangePage } from './language-change.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LanguageChangePageRoutingModule
  ],
  declarations: [LanguageChangePage]
})
export class LanguageChangePageModule {}
