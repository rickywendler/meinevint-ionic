import { Component, Input, OnInit } from '@angular/core';
import { Booking } from '../../../models/Booking';
import { APIProvider } from '../../../providers/APIProvider';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { AnimationController, ModalController, PopoverController } from '@ionic/angular';
import { Activity } from '../../../models/Activity';
import { AlertProvider } from '../../../providers/AlertProvider';
import { Platform } from '@ionic/angular';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { TrackingStamp } from 'src/models/TrackingStamp';
import { v4 as uuidv4 } from 'uuid';
import { SelectTimePopoverComponent } from '../../components/select-time-popover/select-time-popover.component';
import { EmployeeDataProvider } from 'src/providers/EmployeeDataProvider';
import { Project } from 'src/models/Project';

@Component({
	selector: 'app-edit-stamp',
	templateUrl: './edit-stamp.page.html',
	styleUrls: ['./edit-stamp.page.scss']
})
export class EditStampPage implements OnInit {
	@Input() _initStamp: Booking = new Booking();
	@Input() _addNote: boolean = false;

	public _stamp: Booking;

	public _noteIsAdded = false;
	private _allProjects: Project[];
	private _projects = [];
	private _activities: Activity[];

	private _bookingChanged: boolean = false;
	private _noteChanged: boolean = false;

	public isNewStamp: boolean = false;

	public timeIsMissing = false;
	public startIsMissing = false;
	public endIsMissing = false;

	enterAnimationForSelections = (baseEl: HTMLElement) => {
		const root = baseEl.shadowRoot;

		const backdropAnimation = this.animationCtrl
			.create()
			.addElement(root.querySelector('ion-backdrop')!)
			.fromTo('opacity', '0.01', 'var(--backdrop-opacity)');

		const wrapperAnimation = this.animationCtrl
			.create()
			.addElement(root.querySelector('.modal-wrapper')!)
			.keyframes([
				{ offset: 0, opacity: '0', transform: 'translateX(100%)' },
				{ offset: 1, opacity: '1', transform: 'translateX(0%)' }
			]);

		return this.animationCtrl
			.create()
			.addElement(baseEl)
			.duration(300)
			.addAnimation([backdropAnimation, wrapperAnimation]);
	};

	leaveAnimationForSelections = (baseEl: HTMLElement) => {
		return this.enterAnimationForSelections(baseEl).direction('reverse');
	};

	public get isSaveButtonEnabled(): boolean {
		if (this._stamp != null) {
			if (this._stamp.projectId != null && this._stamp.activityId != null) {
				if (this.isNewStamp) {
					const endIsAfterStart = this._stamp.end.diff(this._stamp.start, 'milliseconds').toObject().milliseconds > 0;
					const startIsBevoreNow = this._stamp.start.diffNow('milliseconds').toObject().milliseconds < 0;
					const endIsBevoreNow = this._stamp.end.diffNow('milliseconds').toObject().milliseconds < 0;

					return endIsAfterStart && startIsBevoreNow && endIsBevoreNow;
				}
				return true;
			}
		}
		return false;
	}

	constructor(
		private _api: APIProvider,
		public _lang: LanguageProvider,
		private _modalCon: ModalController,
		private _alertPro: AlertProvider,
		private platform: Platform,
		private animationCtrl: AnimationController,
		private employee: EmployeeDataProvider,
		private _settings: SettingsProvider,
		private _employeeData: EmployeeDataProvider,
		private _popoverController: PopoverController
	) {}

	ngOnInit() {
		this._stamp = Object.assign<Booking, Booking>(new Booking(), this._initStamp);

		this.employee.projects.forEach((p) => {
			this._projects.push({ _name: p.name, _value: p.id });
		});
		this._activities = this.employee.activities;
		if (this.isNewStamp) {
			this.assignDefaultValues();
		}

		if (this._stamp.start || this._stamp.end) {
			if (this._addNote) this._noteIsAdded = this._addNote;
		} else {
			this.isNewStamp = true;
		}

		if (!this.isNewStamp && (this._stamp.start == null || this._stamp.end == null)) {
			this.assignValuesForMissingVariables();
		}
	}

	async ionViewWillEnter() {
		if (this.isNewStamp) {
			this._stamp.start = DateTime.fromObject(
				{
					day: this._stamp.dateAsDateTime.day,
					month: this._stamp.dateAsDateTime.month,
					year: this._stamp.dateAsDateTime.year,
					hour: 8,
					minute: 0
				},
				{
					zone: this._settings.timeZone,
					locale: this._lang.locale
				}
			);
			this._stamp.end = DateTime.fromObject(
				{
					day: this._stamp.dateAsDateTime.day,
					month: this._stamp.dateAsDateTime.month,
					year: this._stamp.dateAsDateTime.year,
					hour: 9,
					minute: 0
				},
				{
					zone: this._settings.timeZone,
					locale: this._lang.locale
				}
			);
		}

		if (this._initStamp.description != null) {
			this._noteIsAdded = true;
		}

		this._bookingChanged = false;
		this._noteChanged = false;
	}

	DismissModal() {
		this._modalCon.dismiss({ changed: false }, 'canceled');
	}

	async addnote(removeNode = false) {
		this._noteIsAdded = !removeNode;
	}

	async deleteStamp() {
		const success = await this._alertPro.ShowAreYouSureAlert(this._lang._dict.TimeSheet.EditStamp.DeleteStampAlert);

		if (success) {
			await this._api.deleteTimestamp(this._initStamp);
			this._modalCon.dismiss({ success: true }, 'delete');
		}
	}

	public async saveNote() {
		this._noteChanged = true;
	}

	async editProject() {
		const modalData = await this._alertPro.ShowListModal(
			this._projects,
			this._lang._dict.TimeTracking.ChooseProject,
			this._stamp.projectId,
			false,
			true,
			true,
			this.enterAnimationForSelections,
			this.leaveAnimationForSelections
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._stamp.projectName = modalData.data._name;
			this._stamp.projectId = modalData.data._value;
			this._bookingChanged = true;
		}
	}

	async editActivity() {
		let activityList = [];

		this._activities.forEach((a) => {
			if (a.projectId == this._stamp.projectId) {
				activityList.push({ _name: a.name, _value: a.id });
			}
		});

		if (activityList.length == 0) {
			this._activities.forEach((a) => {
				if (a.projectId == 0) {
					activityList.push({ _name: a.name, _value: a.id });
				}
			});
		}

		const modalData = await this._alertPro.ShowListModal(
			activityList,
			this._lang._dict.TimeTracking.ChooseActivity,
			this._stamp.activityId,
			false,
			true,
			true,
			this.enterAnimationForSelections,
			this.leaveAnimationForSelections
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._stamp.activityName = modalData.data._name;
			this._stamp.activityId = modalData.data._value;
			this._bookingChanged = true;
		}
	}

	noteChanged(e) {
		this._noteChanged = true;
		this._stamp.description = e.detail.value;
	}

	async saveChanges() {
		if (this.isNewStamp) {
			this._api.saveTrackingStamp(
				new TrackingStamp(uuidv4(), this._stamp.start, this._stamp.end, this._stamp.projectId, this._stamp.activityId)
			);
		} else {
			this._initStamp.projectId = this._stamp.projectId;
			this._initStamp.projectName = this._stamp.projectName;
			this._initStamp.activityId = this._stamp.activityId;
			this._initStamp.activityName = this._stamp.activityName;
			this._api.saveBookingChanges(this._stamp);
		}
		if (this._noteChanged) {
			this._stamp.description = this._stamp.description.trim();
			if (this._stamp.description.length == 0) {
				this._stamp.description = null;
			}
			this._initStamp.description = this._stamp.description;
			this._api.assignDescriptionToBooking(this._stamp);
		}
		this._modalCon.dismiss({ changed: this._bookingChanged }, 'success');
	}

	async assignDefaultValues() {
		if (this._employeeData.defaultProjectId > 0) {
			this._stamp.projectId = this._employeeData.defaultProjectId;

			//The api does not return the name of the project - so we need to look for it
			const proj = this._projects.find((p) => p._value == this._stamp.projectId);
			if (proj != null && proj != undefined) {
				this._stamp.projectName = proj._name;
			}

			if (this._employeeData.defaultActivityId > 0) {
				this._stamp.activityId = this._employeeData.defaultActivityId;

				//Now we also need to look for the detault activities name

				const activ = this._activities.find((a) => a.id == this._stamp.activityId);
				if (activ != null && activ != undefined) {
					this._stamp.activityName = activ.name;
				}
			} else {
				this._stamp.activityName = this._lang._dict.TimeTracking.ChooseActivity;
			}
		} else {
			this._stamp.projectName = this._lang._dict.TimeTracking.ChooseProject;
			this._stamp.activityName = this._lang._dict.TimeTracking.ChooseActivity;
		}
	}

	assignValuesForMissingVariables() {
		this.timeIsMissing = true;
		if (this._stamp.start == null && this._stamp.end != null) {
			this.startIsMissing = true;
			this._stamp.start = DateTime.fromObject(
				{
					day: this._stamp.dateAsDateTime.day,
					month: this._stamp.dateAsDateTime.month,
					year: this._stamp.dateAsDateTime.year,
					hour: this._stamp.end.hour - 1,
					minute: this._stamp.end.minute
				},
				{
					zone: this._settings.timeZone,
					locale: this._lang.locale
				}
			);
		} else if (this._stamp.start != null && this._stamp.end == null) {
			this.endIsMissing = true;
			this._stamp.end = DateTime.fromObject(
				{
					day: this._stamp.dateAsDateTime.day,
					month: this._stamp.dateAsDateTime.month,
					year: this._stamp.dateAsDateTime.year,
					hour: this._stamp.start.hour + 1,
					minute: this._stamp.start.minute
				},
				{
					zone: this._settings.timeZone,
					locale: this._lang.locale
				}
			);
		}
	}

	public async editTimestamp(e, isStart) {
		console.log('stamp start: ' + this._stamp.start.toISO());
		const popover = await this._popoverController.create({
			component: SelectTimePopoverComponent,
			event: e,
			reference: e.target,
			alignment: 'center',
			side: 'top',
			size: 'cover',
			arrow: true,
			componentProps: { _time: isStart ? this._stamp.start : this._stamp.end }
		});

		await popover.present();

		const { data, role } = await popover.onDidDismiss();
		if (role == 'success') {
			if (isStart) {
				this._stamp.start = data.time;
			} else {
				this._stamp.end = data.time;
			}
		}
	}

	/*
	public set startTime(value) {
		this._stamp.start = DateTime.fromISO(value, {
			zone: this._settings.timeZone,
			locale: this._lang.locale,
		});
	}

	public get startTime() {
		return this._stamp.start.toISO();
	}

	public set endTime(value) {
		this._stamp.end = DateTime.fromISO(value, {
			zone: this._settings.timeZone,
			locale: this._lang.locale,
		});
	}

	public get endTime() {
		return this._stamp.end.toISO();
	}
	*/
}
