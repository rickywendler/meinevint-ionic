import { Time } from '@angular/common';
import { DateTime } from 'luxon';

export class Booking {
	private _isPauseEntry: boolean = false;

	private _date: Date;
	private _start: DateTime;
	private _end: DateTime;
	private _duration: number;
	private _startStamp: number | null;
	private _endStamp: number | null;

	private _projectId: number;
	private _projectName: string;
	private _activityId: number;
	private _activityName: string;
	// Tätigkeitsbeschreibung
	private _description: string = null;

	private _zone: string;

	constructor(
		isPauseEntry?: boolean,
		zone?: string,
		date?: Date,
		start?: DateTime,
		end?: DateTime,
		projectId?: number,
		activityId?: number,
		duration?: number,
		startStamp?: number,
		endStamp?: number,
		projectName?: string,
		activityName?: string
	) {
		this._isPauseEntry = isPauseEntry ?? false;
		this._date = date ?? null;
		this._start = start ?? null;
		this._end = end ?? null;
		this._projectId = projectId ?? null;
		this._activityId = activityId ?? null;
		this._duration = duration ?? null;
		this._startStamp = startStamp ?? null;
		this._endStamp = endStamp ?? null;
		this._zone = zone ?? 'Europe/Berlin';
		this._projectName = projectName ?? null;
		this._activityName = activityName ?? null;
	}

	/**
	 * Getter date
	 * @return {Date}
	 */
	public get date(): Date {
		return this._date;
	}

	/**
	 * Setter date
	 * @param {Date } value
	 */
	public set date(value: Date) {
		this._date = value;
	}

	/**
	 * Getter dateAsDateTime, which give the date back as DateTime
	 * @return {DateTime}
	 */
	public get dateAsDateTime(): DateTime {
		return DateTime.fromJSDate(this._date, { zone: this._zone });
	}

	/**
	 * Setter dateAsDateTime
	 * @param {DateTime } value
	 */
	public set dateAsDateTime(value: DateTime) {
		this._date = value.toJSDate();
	}

	/**
	 * Getter start
	 * @return {DateTime}
	 */
	public get start(): DateTime {
		return this._start;
	}

	/**
	 * Setter start
	 * @param {DateTime } value
	 */
	public set start(value: DateTime) {
		this._start = value;
	}

	/**
	 * Getter end
	 * @return {DateTime}
	 */
	public get end(): DateTime {
		return this._end;
	}

	/**
	 * Setter end
	 * @param {DateTime } value
	 */
	public set end(value: DateTime) {
		this._end = value;
	}

	/**
	 * Getter projectId
	 * @return {number}
	 */
	public get projectId(): number {
		return this._projectId;
	}

	/**
	 * Setter projectId
	 * @param {number } value
	 */
	public set projectId(value: number) {
		this._projectId = value;
	}

	/**
	 * Getter activityId
	 * @return {number}
	 */
	public get activityId(): number {
		return this._activityId;
	}

	/**
	 * Setter activityId
	 * @param {number } value
	 */
	public set activityId(value: number) {
		this._activityId = value;
	}

	/**
	 * Setter duration
	 * @param {number } value
	 */
	public set duration(value: number) {
		this._duration = value;
	}

	/**
	 * Getter duration
	 * @return {number}
	 */
	public get duration(): number {
		return this._duration;
	}

	/**
	 * Getter startStamp
	 * @return {number | null}
	 */
	public get startStamp(): number | null {
		return this._startStamp;
	}

	/**
	 * Setter startStamp
	 * @param {number | null } value
	 */
	public set startStamp(value: number | null) {
		this._startStamp = value;
	}

	/**
	 * Getter endStamp
	 * @return {number}
	 */
	public get endStamp(): number | null {
		return this._endStamp;
	}

	/**
	 * Setter endStamp
	 * @param {number | null } value
	 */
	public set endStamp(value: number | null) {
		this._endStamp = value;
	}

	/**
	 * Setter zone
	 * @param {string } value
	 */
	public set zone(value: string) {
		this._zone = value;
	}

	/**
	 * Getter workingTime
	 * @return {Time}
	 */
	public get workingTime(): Time {
		let value: Time = {
			hours: 0,
			minutes: 0
		};

		if (this._end != null && this._start != null) {
			const diff = this._end.diff(this._start, ['hours', 'minutes'], { zone: this._zone });
			value.hours = diff.hours;
			value.minutes = diff.minutes;
		} else if (this._start != null) {
			const now = DateTime.fromObject({}, { zone: this._zone });
			if (this._start.toFormat('yyyy-MM-dd') != now.toFormat('yyyy-MM-dd')) {
				const diff = now.diff(this._start, ['hours', 'minutes'], { zone: this._zone });
				value.hours = diff.hours;
				value.minutes = diff.minutes;
			}
		}

		return value;
	}

	/**
	 * Getter projectName
	 * @return {string}
	 */
	public get projectName(): string {
		return this._projectName;
	}

	/**
	 * Getter activityName
	 * @return {string}
	 */
	public get activityName(): string {
		return this._activityName;
	}

	/**
	 * Getter zone
	 * @return {string}
	 */
	public get zone(): string {
		return this._zone;
	}

	/**
	 * Getter description
	 * @return {string}
	 */
	public get description(): string {
		return this._description;
	}

	/**
	 * Setter projectName
	 * @param {string} value
	 */
	public set projectName(value: string) {
		this._projectName = value;
	}

	/**
	 * Setter activityName
	 * @param {string} value
	 */
	public set activityName(value: string) {
		this._activityName = value;
	}

	/**
	 * Setter description
	 * @param {string} value
	 */
	public set description(value: string) {
		this._description = value;
	}

	/**
	 * Getter isPauseEntry
	 * @return {boolean }
	 */
	public get isPauseEntry(): boolean {
		return this._isPauseEntry;
	}

	/**
	 * Setter isPauseEntry
	 * @param {boolean } value
	 */
	public set isPauseEntry(value: boolean) {
		this._isPauseEntry = value;
	}
}
