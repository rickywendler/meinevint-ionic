import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { SettingsProvider } from '../../providers/SettingsProvider';
import { DataProvider } from '../../providers/DataProvider';
import { APIProvider } from '../../providers/APIProvider';
import { Project } from '../../models/Project';
import { Activity } from '../../models/Activity';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Booking } from '../../models/Booking';
import { EmployeeDataProvider } from '../../providers/EmployeeDataProvider';
import { IonSelect, ModalController, NavController, GestureController, Gesture } from '@ionic/angular';
import { DateTime } from 'luxon';

import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { AlertProvider } from '../../providers/AlertProvider';
import { TrackerRunningPage } from './tracker-running/tracker-running.page';
import { TimeTrackingProvider } from '../../providers/TimeTrackingProvider';

@Component({
	selector: 'app-time-tracking',
	templateUrl: './time-tracking.page.html',
	styleUrls: ['./time-tracking.page.scss']
})
export class TimeTrackingPage implements OnInit {
	private initialized = false;
	public useOnlyQr: boolean = false;
	public today: DateTime = DateTime.now();

	public sliderButtonLeft: string = '0px';
	private sliderCalledOnced = false;
	private slideButton;

	public _availableProjects: Project[] = null;
	public _selectedProject = null;

	private _allActivities: Activity[] = null;
	public _availableActivities: Activity[] = null;
	public _selectedActivity = null;

	public _bookings: Booking[];

	constructor(
		public _lang: LanguageProvider,
		public _settings: SettingsProvider,
		private _data: DataProvider,
		private _employeeData: EmployeeDataProvider,
		private _api: APIProvider,
		private _modalController: ModalController,
		public _alertProvider: AlertProvider,
		private _nav: NavController,
		private _router: Router,
		private _gestureCtrl: GestureController,
		private route: ActivatedRoute,
		public _tracker: TimeTrackingProvider
	) {}

	async ngOnInit() {
		this.route.queryParams.subscribe(async (params) => {
			let startTracking = false;
			if (Object.keys(params).length > 0) {
				startTracking = (params.startTracking as unknown as boolean) ?? false;
			}
		});

		this.today = DateTime.now().setLocale(this._lang.locale);
		this._availableProjects = this._employeeData.projects;
		this._allActivities = this._employeeData.activities;

		this._bookings = await this._api.getBookings(DateTime.now().setZone(this._settings.timeZone));

		if (this._availableProjects != null) {
			if (this._availableProjects.length > 0) {
				await this.assignDefaultValues();
				if (this._allActivities.length > 0) {
					this.assignActivitiesToSelect();
				}
			}
		}

		if (this._tracker.isTracking || this._tracker.isPause) {
			this.timerIsRunning();
		}
	}

	ionViewDidEnter() {
		this.slideButton = document.querySelector('#sliderButton') as any;

		const gesture = this._gestureCtrl.create(
			{
				el: document.querySelector('#startSlider'),
				gestureName: 'test',
				onMove: (detail) => this.checkForSlider(detail),
				onEnd: (detail) => this.resetSlider()
			},
			true
		);

		gesture.enable(true);
	}

	async ionViewWillEnter() {
		if (this._employeeData.hasRightForOnlyQR) {
			this.useOnlyQr = true;
			this.scanQr();
		}
		this.sliderCalledOnced = false;

		if (this._tracker.isTracking || this._tracker.isPause) {
			this.timerIsRunning();
		}

		this._bookings = await this._api.getBookings(DateTime.fromObject({}, { zone: this._settings.timeZone }).toJSDate());

		// We need to ensure that the preious timestamp was correctly and completely handled by the serrver
		// Thus: wait a second
		setTimeout(() => {
			this.checkForRunningTracking();
		}, 500);
	}

	async assignDefaultValues() {
		if (!this._tracker.isTracking && !this.initialized) {
			this._tracker.projectId = this._employeeData.defaultProjectId;

			//The api does not return the name of the project - so we need to look for it
			const proj = this._availableProjects.find((p) => p.id == this._tracker.projectId);
			if (proj != null && proj != undefined) {
				this._selectedProject = proj.name;
				this._availableActivities = this._allActivities.filter((a) => {
					a.projectId == proj.id;
				});
			} else {
				this._availableActivities = this._allActivities.filter((a) => {
					a.projectId == 0;
				});
				this._selectedProject = null;
			}

			this._tracker.activityId = this._employeeData.defaultActivityId;
			if (this._tracker.activityId != null && this._tracker.activityId.toString() != '') {
				this._selectedActivity = this._employeeData.getActivityById(this._tracker.activityId).name;
			}
			this.initialized = true;
		}
	}

	public async timerIsRunning() {
		this._router.navigate(['/app/tabs/time-tracking/tracker-running']);
	}

	public selectedActivityChanged(data) {
		const activSelect = data.target as HTMLIonSelectElement;
		this._tracker.activityId = (activSelect.value as number) ?? null;
	}

	public async changeSelectedProject() {
		let projectList = [];

		this._availableProjects.forEach((p) => {
			projectList.push({ _name: p.name, _value: p.id });
		});

		const modalData = await this._alertProvider.ShowListModal(
			projectList,
			this._lang._dict.TimeTracking.ChooseProject,
			this._tracker.projectId
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._selectedProject = modalData.data._name ?? null;
			this._tracker.projectId = modalData.data._value ?? null;
			this._tracker.activityId = null;
			this._selectedActivity = null;
			this.assignActivitiesToSelect();
		}
	}

	public async assignActivity() {
		if (this._availableActivities.length > 0) {
			let activityList = [];

			this._availableActivities.forEach((a) => {
				activityList.push({ _name: a.name, _value: a.id });
			});

			const modalData = await this._alertProvider.ShowListModal(
				activityList,
				this._lang._dict.TimeTracking.ChooseActivity,
				this._tracker.activityId
			);

			if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
				this._selectedActivity = modalData.data._name ?? null;
				this._tracker.activityId = modalData.data._value ?? null;
			}
		}
	}

	public async scanQr() {
		const status = await BarcodeScanner.checkPermission({ force: true });
		if (status.granted) {
			this._tracker.projects = this._availableProjects;
			this._router.navigate(['/app/tabs/time-tracking/qr-scan']);
		} else {
			this._alertProvider.ShowInfo(
				this._lang._dict.TimeTracking.QRCode.NoPermissionsHeader,
				this._lang._dict.TimeTracking.QRCode.NoPermissionsMessage
			);
		}
	}

	// private methods

	private checkForRunningTracking() {
		this._bookings.forEach((booking: Booking) => {
			if (booking.end == null) {
				console.log('CHECKFORRUNNINGTRACKING: FOUND RUNNING TRACKING');
				console.log(booking, this._bookings);
				this._tracker.start = booking.start;
				this._tracker.projectId = booking.projectId;
				this._tracker.activityId = booking.activityId;
				this._tracker.startTracking(false);

				this.timerIsRunning();
			}
		});
	}

	private assignActivitiesToSelect() {
		this._availableActivities = [];
		let value = 0;

		if (this._tracker.projectId != null) {
			value = this._tracker.projectId;
		}

		this._allActivities.forEach((activity) => {
			if (activity.projectId == value) {
				this._availableActivities.push(activity);
			}
		});

		if (this._availableActivities.length == 0) {
			this._allActivities.forEach((activity) => {
				if (activity.projectId == 0) {
					this._availableActivities.push(activity);
				}
			});
		}
	}

	private assignActivityText(activityID) {
		this._selectedActivity = this._employeeData.getActivityById(activityID).name;
	}

	checkForSlider(detail) {
		if (detail.startX < 100 && this.isSliderActive) {
			this.slideButton.style.left = detail.currentX - 40 + 'px';
			if (
				detail.currentX > window.innerWidth * 0.9 &&
				!this.sliderCalledOnced
			) {
				this.sliderCalledOnced = true;
				//The slider was dragged to the right
				// this._trackingStamp.stopBreak();
				this.timerIsRunning();
			}

			//We now check whether the slider was actually dragged to the left
			console.log(detail.currentX);
			if (detail.currentX < 40) {
				this.slideButton.style.left = '5px';
			}
		}
	}

	resetSlider() {
		this.slideButton.style.left = '5px';
	}

	//#region getter

	public get projectsAreAvailable(): boolean {
		if (this._availableProjects == null) return false;
		return this._availableProjects.length > 0;
	}

	public get activitiesAreAvailable(): boolean {
		if (this._availableActivities == null) return false;
		return this._availableActivities.length > 0;
	}

	public get isSliderActive(): boolean {

		//Demo, so just return true and be done with it
		return true;

		if (this.projectsAreAvailable) {
			if (
				this._tracker.projectId != null &&
				this._tracker.projectId.toString() != '' &&
				!(this._selectedProject == null || this._selectedProject == '')
			) {
				return true;
				/*
				code for checking if activities is set
				if (this.activitiesAreAvailable) {
					if (this._tracker.activityId != null && this._tracker.activityId.toString() != '') {
						return true;
					}
				} else {
					return true;
				}
				*/
			}
		} else {
			return true;
		}
		return false;
	}

	//#endregion
}
