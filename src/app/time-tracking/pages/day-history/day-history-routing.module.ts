import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DayHistoryPage } from './day-history.page';

const routes: Routes = [
  {
    path: '',
    component: DayHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DayHistoryPageRoutingModule {}
