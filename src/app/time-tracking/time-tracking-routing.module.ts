import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeTrackingPage } from './time-tracking.page';

const routes: Routes = [
	{
		path: '',
		component: TimeTrackingPage,
	},
	{
		path: 'tracker-running',
		loadChildren: () =>
			import('./tracker-running/tracker-running.module').then((m) => m.TrackerRunningPageModule),
	},
	{
		path: 'day-history',
		loadChildren: () =>
			import('./pages/day-history/day-history.module').then((m) => m.DayHistoryPageModule),
	},
	{
		path: 'noauths',
		loadChildren: () => import('../no-auths/no-auths.module').then((m) => m.NoAuthsPageModule),
	},
	{
		path: 'finished-day',
		loadChildren: () =>
			import('./pages/finished-day/finished-day.module').then((m) => m.FinishedDayPageModule),
	},  {
    path: 'qr-scan',
    loadChildren: () => import('./pages/qr-scan/qr-scan.module').then( m => m.QrScanPageModule)
  },

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TimeTrackingPageRoutingModule {}
