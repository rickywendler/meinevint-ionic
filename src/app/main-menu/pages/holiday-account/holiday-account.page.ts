import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { APIProvider } from '../../../../providers/APIProvider';
import { HolidayAccount } from '../../../../models/HolidayAccount';
import { HolidayRequest } from 'src/models/HolidayRequest';
import { ModalController, Platform } from '@ionic/angular';
import { HolidayRequestPage } from '../../../modal-pages/holiday-request/holiday-request.page';
import { AbscenceType } from '../../../../models/AbscenceType';
import { DateTime } from 'luxon';
import { AlertProvider } from '../../../../providers/AlertProvider';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { HolidayDetailPage } from './modals/holiday-detail/holiday-detail.page';

@Component({
	selector: 'app-holiday-account',
	templateUrl: './holiday-account.page.html',
	styleUrls: ['./holiday-account.page.scss']
})
export class HolidayAccountPage implements OnInit {
	public _currentYear = +DateTime.now().year;
	public _previousYear = +DateTime.now().year - 1;

	public _holiday: HolidayAccount = new HolidayAccount();
	public _previousHoliday: HolidayAccount = new HolidayAccount();
	public _allHolidays: HolidayAccount[] = [];

	public _allHolidayRequests: HolidayRequest[] = [];
	public _previousRequests: HolidayRequest[] = [];
	public _upcomingRequests: HolidayRequest[] = [];

	constructor(
		private _api: APIProvider,
		public _lang: LanguageProvider,
		private _modalCon: ModalController,
		private _platform: Platform,
		public _alertProvider: AlertProvider,
		public _settings: SettingsProvider
	) {}

	private async Init() {
		this._holiday = this._allHolidays.find((h) => h.year == this._currentYear);
		this._previousHoliday = this._allHolidays.find((h) => h.year == this._previousYear) ?? new HolidayAccount();

		this._previousRequests = [];
		this._upcomingRequests = [];
		if (!this._settings.offlineMode) {
			this._allHolidayRequests = await this._api.getHolidayRequests(this._currentYear);

			if (this._allHolidayRequests.length > 0) {
				// Sorting of the requests into upcoming and previous
				const now = DateTime.now().setZone(this._settings.timeZone);
				this._allHolidayRequests.forEach((h) => {
					// Determine what is the start and end of the holidayrequest
					let start = h.period[0].start;
					let end = h.period[0].end;
					h.period.forEach((p) => {
						if (start != p.start && end != p.end) {
							if (p.start.valueOf() < start.valueOf()) {
								start = p.start;
							}
							if (p.end.valueOf() > end.valueOf()) {
								end = p.end;
							}
						}
					});

					if (end.valueOf() < now.valueOf()) {
						this._previousRequests.push(h);
					} else {
						this._upcomingRequests.push(h);
					}
				});
			}
		}
	}

	async ngOnInit() {
		this._allHolidays = await this._api.getHolidays(true);
		this.Init();
	}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('holidayAccount-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/
	}

	private async createHolidayRequest() {
		const modal = await this._modalCon.create({
			component: HolidayRequestPage,
			componentProps: {
				_holidayRequests: this._allHolidayRequests,
				_holidayAccount: this._holiday
			},
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();

		const modalData = await modal.onWillDismiss();
		if (modalData.data != null) {
			if (modalData.data.success) {
				const intervalVar = setInterval(
					(async () => {
						let holidays = await this._api.getHolidays();
						const currentYear = new Date().getFullYear();
						let holi: HolidayAccount;

						holidays.forEach((holiday) => {
							if (holiday.year == currentYear) {
								holi = holiday;
							}
						});

						if (holi.requestedDays != this._holiday.requestedDays) {
							this._holiday = holi;
							clearInterval(intervalVar);
						}
					}).bind(this),
					100
				);
			}
		}
	}

	public async selectPeriod() {
		let yearSelection = [];

		this._allHolidays.forEach((h) => {
			yearSelection.push({ _name: h.year, _value: h.year });
		});

		const modalData = await this._alertProvider.ShowListModal(
			yearSelection,
			this._lang._dict.HolidayAccount.SelectYear,
			this._currentYear
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._allHolidays = await this._api.getHolidays(true);
			this._currentYear = modalData.data._value;
			this._previousYear = modalData.data._value - 1;
			this.Init();
		}
	}

	public async newRequest() {
		if (!this._settings.offlineMode) {
			this.createHolidayRequest();
		}
	}

	public async openRequest(request: HolidayRequest) {
		const modal = await this._modalCon.create({
			component: HolidayDetailPage,
			componentProps: {
				_request: request
			},
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		await modal.present();
		const modalData = await modal.onWillDismiss();
		if (modalData.role == 'deleted') {
			this._allHolidays = await this._api.getHolidays(true);
			this.Init();
		}
	}
}
