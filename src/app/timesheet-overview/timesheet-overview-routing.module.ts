import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimesheetOverviewPage } from './timesheet-overview.page';

const routes: Routes = [
	{
		path: '',
		component: TimesheetOverviewPage,
	},
	{
		path: 'timesheet',
		loadChildren: () =>
			import('./pages/time-sheet/time-sheet.module').then((m) => m.TimeSheetPageModule),
	},
	{
		path: 'select-week',
		loadChildren: () =>
			import('./modals/select-week/select-week.module').then((m) => m.SelectWeekPageModule),
	},
	{
		path: 'noauths',
		loadChildren: () =>
			import('../no-auths/no-auths.module').then((m => m.NoAuthsPageModule))
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TimesheetOverviewPageRoutingModule {}
