import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from 'src/providers/LanguageProvider';
import { APIProvider } from '../../../../providers/APIProvider';
import { DateTime } from 'luxon';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from '../../../../models/Booking';
import { DayHistoryPage } from '../day-history/day-history.page';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { TimeTrackingProvider } from 'src/providers/TimeTrackingProvider';

@Component({
	selector: 'app-finished-day',
	templateUrl: './finished-day.page.html',
	styleUrls: ['./finished-day.page.scss']
})
export class FinishedDayPage implements OnInit {
	public _isOnline = false;
	public _today = DateTime.now().setLocale(this._lang.locale);
	public _projectName = 'Name of project';
	public _activityName = 'Name of activity';

	public _totalWorkTime = 0;
	public _totalPauseTime = 0;

	public _dayStarted = DateTime.now();
	public _dayEnded = DateTime.now();

	private _bookings: Booking[] = [];
	private initialise = false;

	constructor(
		public _lang: LanguageProvider,
		private _api: APIProvider,
		private _route: ActivatedRoute,
		private _modal: ModalController,
		private _router: Router,
		private _routerOutlet: IonRouterOutlet,
		public tracker: TimeTrackingProvider
	) {
		this._route.queryParams.subscribe((params) => {
			if (Object.keys(params).length > 0) {
				//this.bookings = params.bookings;
				this._projectName = params.projectName;
				this._activityName = params.activityName;
				this.initialise = params.init ?? false;
			}
		});
	}

	async ngOnInit() {}
	ionViewDidEnter() {
		this._routerOutlet.swipeGesture = false;
	}
	ionViewDidLeave() {
		this._routerOutlet.swipeGesture = true;
	}

	async ionViewWillEnter() {
		if (this.initialise) {
			this.bookings = await this._api.getBookings(new Date());
			this.initialise = false;
		}
	}

	private set bookings(value: Booking[]) {
		this._totalWorkTime = 0;
		this._totalPauseTime = 0;

		this._bookings = value;
		this._dayStarted = this._bookings[0].start;
		this._dayEnded = this.tracker.end;

		let startValue = this._dayStarted.valueOf();

		this._bookings.forEach((b) => {
			if (!b.isPauseEntry) {
				this._totalWorkTime += b.duration;
				if (b.start.valueOf() < startValue) {
					this._dayStarted = b.start;
					startValue = b.start.valueOf();
				}
				if (!b.end) {
					this._totalWorkTime += this.tracker.duration;
				}
			} else {
				this._totalPauseTime += b.duration;
			}
		});
	}

	public async showDayHistory() {
		const modal = await this._modal.create({
			component: DayHistoryPage,
			canDismiss: true,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();
	}

	public finish() {
		this._router.navigateByUrl('/app/tabs/timesheet-overview', { replaceUrl: true });
	}
}
