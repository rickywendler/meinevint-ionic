import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsfeedPage } from './newsfeed.page';

const routes: Routes = [
  {
    path: '',
    component: NewsfeedPage
  },
  {
    path: 'news-detail',
    loadChildren: () => import('./modals/news-detail/news-detail.module').then( m => m.NewsDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsfeedPageRoutingModule {}
