import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorktimeAccountPageRoutingModule } from './worktime-account-routing.module';

import { WorktimeAccountPage } from './worktime-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorktimeAccountPageRoutingModule
  ],
  declarations: [WorktimeAccountPage]
})
export class WorktimeAccountPageModule {}
