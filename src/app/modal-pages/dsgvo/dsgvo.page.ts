import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LanguageProvider } from 'src/providers/LanguageProvider';
import { DataProvider } from '../../../providers/DataProvider';

@Component({
	selector: 'app-dsgvo',
	templateUrl: './dsgvo.page.html',
	styleUrls: ['./dsgvo.page.scss']
})
export class DsgvoPage implements OnInit {
	//#region properties

	@Input() _userNeedToAccept: boolean = false;
	private accepted = false;
	public scrollDepthTriggered = false;

	//#endregion

	//#region system methods + constructor

	constructor(public _lang: LanguageProvider, private _modalCon: ModalController, private _data: DataProvider) {}

	ngOnInit() {}

	//#endregion

	//#region public methods

	public acceptedPolicy() {
		this.accepted = true;
		this._data.setStorage('seenDSGVO', this.accepted);
		this._modalCon.dismiss({ accepted: this.accepted });
	}

	public dismiss() {
		this._modalCon.dismiss({ accepted: this.accepted });
	}

	public async scrolling($event) {
		// only send the event once
		if (this.scrollDepthTriggered) {
			return;
		}

		if ($event.target.localName != 'ion-content') {
			// not sure if this is required, just playing it safe
			return;
		}

		const scrollElement = await $event.target.getScrollElement();

		// minus clientHeight because trigger is scrollTop
		// otherwise you hit the bottom of the page before
		// the top screen can get to 80% total document height
		const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;

		const currentScrollDepth = scrollElement.scrollTop;

		const targetPercent = 99;

		let triggerDepth = (scrollHeight / 100) * targetPercent;

		if (currentScrollDepth >= triggerDepth) {
			console.log(`Scrolled to ${targetPercent}%`);
			// this ensures that the event only triggers once
			this.scrollDepthTriggered = true;
			// do your analytics tracking here
		}
	}

	//#endregion
}
