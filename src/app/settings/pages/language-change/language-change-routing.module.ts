import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LanguageChangePage } from './language-change.page';

const routes: Routes = [
  {
    path: '',
    component: LanguageChangePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LanguageChangePageRoutingModule {}
