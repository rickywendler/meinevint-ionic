import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { APIProvider } from '../../../../providers/APIProvider';
import { Gesture, GestureController, NavController, ModalController } from '@ionic/angular';
import { BookingDetailsPage } from '../../../modal-pages/booking-details/booking-details.page';
import { TrackingStamp } from '../../../../models/TrackingStamp';
import { Booking } from '../../../../models/Booking';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { WTALeave } from '../../../../models/WTALeave';
import { AlertProvider } from '../../../../providers/AlertProvider';
import { EditStampPage } from '../../../modal-pages/edit-stamp/edit-stamp.page';
import { HolidayRequest } from '../../../../models/HolidayRequest';
import { HolidayAccount } from '../../../../models/HolidayAccount';
import { DateSelectionPage } from 'src/app/modal-pages/date-selection/date-selection.page';
import { pipe, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
	selector: 'app-time-sheet',
	templateUrl: './time-sheet.page.html',
	styleUrls: ['./time-sheet.page.scss']
})
export class TimeSheetPage implements OnInit {
	@ViewChild('#contentSheet') content: ElementRef;

	public _timeRecords: Booking[];
	public _totalWorkTime = 0;
	public _totalPauseTime = 0;
	public _dayOffset = 0;
	public _trackingInfo: TrackingStamp;
	public _runningTime: number = 0;
	public _bookingDate: Date = new Date();
	public _isModal = false;

	public leaves: WTALeave[] = [];
	public holidayRequests: HolidayRequest[] = [];
	public HolidayRequestForToday: HolidayRequest = null;
	public leaveTheCompleteDay = false;

	constructor(
		public _api: APIProvider,
		public _lang: LanguageProvider,
		private gestureCtrl: GestureController,
		private navCtrl: NavController,
		private modalCon: ModalController,
		private router: Router,
		private route: ActivatedRoute,
		public _settings: SettingsProvider,
		private _alertProv: AlertProvider
	) {
		this.route.queryParams.subscribe((params) => {
			if (Object.keys(params).length > 0) {
				this._bookingDate = new Date(params.bookingDate);
				this._isModal = false;
				this.getLeaves();
				//this.getHolidayLeaves();
			} else {
				this._bookingDate = new Date();
				this._isModal = true;
			}
		});
	}

	ngOnInit() {
		/*
		let content = document.getElementsByClassName('contentSheet');

		const gesture: Gesture = this.gestureCtrl.create({
			el: document.querySelector('.contentSheet'),
			gestureName: 'test',
			onEnd: (detail) => this.SwitchDays(detail),
		});

		gesture.enable();
		*/
	}

	SwitchDays(detail) {
		this._timeRecords = [];
		//Check whether a swiping criteria was met
		if (detail.startX - 100 > detail.currentX || detail.startX + 100 < detail.currentX) {
			let newdate = DateTime.now().setZone(this._settings.timeZone).setLocale(this._lang.locale);

			//The cursor was swiped to the left
			if (detail.startX - 100 > detail.currentX) {
				this._dayOffset += 1;
				newdate.setDate(newdate.getDate() + this._dayOffset);
			}

			//The cursor war swiped to the right
			if (detail.startX + 100 < detail.currentX) {
				this._dayOffset -= 1;
				newdate.setDate(newdate.getDate() + this._dayOffset);
			}

			this.getBookings(newdate);
		}
	}

	private async getLeaves() {
		this._api
			.getLeaves(DateTime.fromJSDate(this._bookingDate), DateTime.fromJSDate(this._bookingDate))
			.then((leaves) => {
				this.leaves = leaves;

				if (this.leaves.length > 0) {
					let hoursForToday = 0;
					this.leaves.forEach((l) => {
						hoursForToday += l.hours;
					});
					if (hoursForToday >= 8) {
						this.leaveTheCompleteDay = true;
					}
					let message = this._lang._dict.TimeSheet.LeavesAvailable + '';
					message = message.replace('XX', hoursForToday + '');
					setTimeout(async () => this._alertProv.ShowAlertPrompt('timeSheetAlert', message, 'warning'), 100);
				}
			});
	}

	private async getHolidayLeaves() {
		this._api.getHolidayRequests(this._bookingDate.getFullYear()).then((response) => {
			this.holidayRequests = response;

			if (this.holidayRequests.length > 0) {
				const holidayRequest = this.holidayRequests.find((h) => {
					return (
						h.period.find(
							(p) =>
								p.start.getMilliseconds() < this._bookingDate.getMilliseconds() &&
								p.end.getMilliseconds() > this._bookingDate.getMilliseconds()
						) != undefined
					);
				});

				if (holidayRequest != undefined) {
					let hoursForToday = 0;

					let message = this._lang._dict.TimeSheet.LeavesAvailable + '';
					message = message.replace('XX', hoursForToday + '');
					setTimeout(() => this._alertProv.ShowAlertPrompt('timeSheetAlert', message, 'warning'), 100);
				}
			}
		});
	}

	//Holt die Buchungen von der API und berechnet die Gesamtzeit
	async getBookings(param: DateTime = DateTime.now().setZone(this._settings.timeZone)) {
		this._timeRecords = await this._api.getBookings(param, true);
		this.CalculateTotalTime();
	}

	async ionViewWillEnter() {
		await this.getBookings(this._bookingDate);

		//this.CalculateTotalTime();
		this.calculateRunningTime();
	}

	checkLastItem() {
		const lastPoint = document.getElementById('lastPoint');
		if (lastPoint != null) {
			const parentChildren = Array.from(lastPoint.parentElement.children);
			parentChildren.forEach((el) => {
				if (el.classList.contains('visualline')) {
					el.setAttribute('style', 'height: 40% !important;');
				}
			});
		}
	}

	async CalculateTotalTime() {
		this._totalWorkTime = 0;
		this._totalPauseTime = 0;
		console.log('Calculating total time');
		console.log(this._timeRecords);
		this._timeRecords.forEach((i) => {
			if (!i.isPauseEntry) {
				this._totalWorkTime += i.duration;
			} else {
				if (i.end != null && i.start != null) this._totalPauseTime += i.duration;
			}
		});
	}

	private calculateRunningTime() {
		setInterval(
			(() => {
				this._runningTime = this._totalWorkTime + TrackingStamp.staticTrackedTime / 3600000;
				this._runningTime = Number(this._runningTime.toFixed(2));
			}).bind(this),

			1000
		);
	}

	async GoToDetails(booking, addNote = false) {
		let newStamp = false;
		if (booking != null) {
			await this._api.getDescriptionForBooking(booking);
		} else {
			newStamp = true;
			booking = new Booking(false, this._settings.timeZone, this._bookingDate);
		}

		//Hand over the model to the new page and create the modal
		const modal = await this.modalCon.create({
			component: EditStampPage,
			componentProps: {
				_initStamp: booking,
				_addNote: addNote
			},
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();

		const { role, data } = await modal.onDidDismiss();

		if ((newStamp && role == 'success') || role == 'delete') {
			this.ionViewWillEnter();
		}
	}

	async createNewStamp() {
		let navExtras: NavigationExtras = {
			queryParams: {
				date: DateTime.fromJSDate(this._bookingDate)
			}
		};

		this.router.navigate(['/app/tabs/timesheet-overview/timesheet/create-timestamp'], navExtras);
	}

	async createPDF() {
		let options = [
			{ _name: this._lang._dict.TimeSheet.PDFCreation.Week, _value: 'week' },
			{ _name: this._lang._dict.TimeSheet.PDFCreation.Month, _value: 'month' }
			//{ _name: this._lang._dict.TimeSheet.PDFCreation.Date, _value: 'date' }
		];

		const modalData = await this._alertProv.ShowListModal(
			options,
			this._lang._dict.TimeSheet.PDFCreation.Header,
			'week',
			true,
			false
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			const selection = modalData.data;
			switch (selection._value) {
				case 'week':
					this._api.getTimeSheetPDF(1, this._bookingDate);
					break;
				case 'month':
					this._api.getTimeSheetPDF(2, this._bookingDate);
					break;
				/*
				case 'date':
					const modal = await this.modalCon.create({
						component: DateSelectionPage,
						componentProps: {
							_setStart: false,
							_multiSelect: true
						},
						breakpoints: [0, 0.75],
						initialBreakpoint: 0.75
					});

					modal.present();

					const modalData = await modal.onWillDismiss();
					break;
					*/
			}
		}
	}
}
