export class Period {
	private _id: number;
	private _start: Date;
	private _end: Date;
	private _daysCount: number;
	private _viewHelper: string;

	constructor(id?: number, start?: Date, end?: Date, daysCount?: number, viewHelper?: string) {
		this._start = start ?? null;
		this._end = end ?? null;
		this._id = id ?? null;
		this._daysCount = daysCount ?? null;
		this._viewHelper = viewHelper ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Getter start
	 * @return {Date}
	 */
	public get start(): Date {
		return this._start;
	}

	/**
	 * Setter start
	 * @param {Date } value
	 */
	public set start(value: Date) {
		this._start = value;
	}

	/**
	 * Getter end
	 * @return {Date}
	 */
	public get end(): Date {
		return this._end;
	}

	/**
	 * Setter end
	 * @param {Date } value
	 */
	public set end(value: Date) {
		this._end = value;
	}

	/**
	 * Getter daysCount
	 * @return {number}
	 */
	public get daysCount(): number {
		return this._daysCount;
	}

	/**
	 * Setter daysCount
	 * @param {number } value
	 */
	public set daysCount(value: number) {
		this._daysCount = value;
	}

	/**
	 * Getter viewHelper
	 * @return {string}
	 */
	public get viewHelper(): string {
		return this._viewHelper;
	}

	/**
	 * Setter viewHelper
	 * @param {string } value
	 */
	public set viewHelper(value: string) {
		this._viewHelper = value;
	}
}
