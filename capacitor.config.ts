import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
	appId: 'mea.compleet.Freemium',
	appName: 'mycompleet free',
	webDir: 'www',
	bundledWebRuntime: false,
	ios: {
		cordovaLinkerFlags: ['-ObjC']
	},
	plugins: {
		SplashScreen: {
			launchShowDuration: 0
		},
		PushNotifications: {
			presentationOptions: ['badge', 'sound', 'alert']
		}
	}
};

export default config;
