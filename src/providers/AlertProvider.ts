import { Injectable } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ListModalPage } from '../app/modal-pages/list-modal/list-modal.page';

@Injectable()
export class AlertProvider {
	constructor(private _modalController: ModalController, private _alert: AlertController) {}

	/**
	 *
	 * @param elementList - Contains an Array of Elements of any type.
	 * The elements HAVE to contain a "_name" property to be displayed and a "_value" for Preselected!
	 * @param header is a String which displays the titel of the modal
	 * @param preselected is a variable which uses for radioGroup to selected the preselected element
	 */
	public async ShowListModal(
		elementList,
		header: string,
		preselected: any = '',
		showSaveButton: boolean = false,
		showOverFullHeight = false,
		showReturnButton = false,
		enterAnimation = null,
		leaveAnimation = null
	) {
		let modal = null;
		if (showOverFullHeight) {
			modal = await this._modalController.create({
				component: ListModalPage,
				componentProps: {
					_header: header,
					_inputList: elementList,
					_inputPreselected: preselected,
					_showSaveButton: showSaveButton,
					_showReturnButton: showReturnButton
				},
				swipeToClose: false,

				enterAnimation: enterAnimation,
				leaveAnimation: leaveAnimation
			});
		} else {
			modal = await this._modalController.create({
				component: ListModalPage,
				componentProps: {
					_header: header,
					_inputList: elementList,
					_inputPreselected: preselected,
					_showSaveButton: showSaveButton,
					_showReturnButton: showReturnButton
				},
				cssClass: 'auto-height',
				breakpoints: [0, 1],
				initialBreakpoint: 1,

				enterAnimation: enterAnimation,
				leaveAnimation: leaveAnimation
			});
		}

		await modal.present();

		return await modal.onDidDismiss();
	}

	/**
	 * Shows an Alert which will be prompted in a div with the class "alertPrompt"
	 * @param Message - String that shall be displayed
	 * @param Type - Which type the alert prompt should be
	 * success: Success message
	 * warning: Warning
	 * alert: Alert
	 * none: Remove alert
	 */
	public ShowAlertPrompt(cssClass: string, message: string, type: string, iconName: string = null) {
		const alertPanel = document.getElementsByClassName(cssClass);
		console.log(alertPanel);
		const alert = alertPanel.item(0) as HTMLElement;
		alert.setAttribute('class', cssClass + ' alertPrompt-' + type);
		if (iconName != null) {
			let color = '';

			switch (type) {
				case 'warning':
					color = 'yellow';
					break;
				case 'success':
					color = 'green';
					break;
				case 'danger':
					color = 'red';
					break;
			}

			const icon = document.createElement('ion-icon');
			icon.setAttribute('src', 'assets/icon/' + iconName + '.svg');
			icon.setAttribute('class', color);
			alert.appendChild(icon);
		}
		const test = document.createElement('span');
		test.setAttribute('style', 'vertical-align: super; margin-left: 0.5rem');
		test.innerText = message;
		alert.appendChild(test);
		//alert.innerText = message;
		if (type == 'none') {
			alert.innerText = '';
		}
	}

	public async ShowInfo(header: string, message: string, awaitResult = false) {
		const alert = await this._alert.create({
			header: header,
			message: message,
			buttons: ['Ok']
		});

		await alert.present();

		if (awaitResult) {
			await alert.onDidDismiss();
		}
	}

	public async ShowAreYouSureAlert(AlertLang: any): Promise<boolean> {
		const alert = await this._alert.create({
			header: AlertLang.Header,
			message: AlertLang.Message,
			buttons: [
				{
					text: AlertLang.Cancel,
					role: 'cancel'
				},
				{
					text: AlertLang.Confirm,
					role: 'confirm'
				}
			]
		});

		await alert.present();

		const { role } = await alert.onDidDismiss();

		return role == 'confirm';
	}
}
