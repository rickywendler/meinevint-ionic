import { Component, Input, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../../../providers/LanguageProvider';
import { HolidayRequest } from '../../../../../../models/HolidayRequest';
import { ModalController } from '@ionic/angular';
import { DateTime } from 'luxon';
import { Period } from 'src/models/Period';
import { AlertProvider } from '../../../../../../providers/AlertProvider';
import { APIProvider } from '../../../../../../providers/APIProvider';

@Component({
	selector: 'app-holiday-detail',
	templateUrl: './holiday-detail.page.html',
	styleUrls: ['./holiday-detail.page.scss'],
})
export class HolidayDetailPage implements OnInit {
	@Input() public _request: HolidayRequest;

	public periodStart: number;
	public periodEnd: number;

	constructor(
		public _lang: LanguageProvider,
		private _modalCon: ModalController,
		private alertProv: AlertProvider,
		private api: APIProvider
	) {}

	ngOnInit() {}

	IonViewWillEnter() {}

	DismissModal() {
		this._modalCon.dismiss({ changed: false }, 'canceled');
	}

	async withdrawRequest() {
		const success = await this.alertProv.ShowAreYouSureAlert(
			this._lang._dict.HolidayAccount.Details.DeleteRequestAlert
		);

		if (success) {
			await this.api.deleteHolidayRequest(this._request);
			this._modalCon.dismiss({ success: true }, 'deleted');
		}
	}
}
