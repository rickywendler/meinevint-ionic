/* eslint-disable quote-props */
import { Injectable, Inject } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { DataProvider } from './DataProvider';
import { TrackingStamp } from '../models/TrackingStamp';
import { Project } from '../models/Project';
import { Activity } from '../models/Activity';
import { Shift } from '../models/Shift';
import { EmployeeDataProvider } from './EmployeeDataProvider';
import { Booking } from '../models/Booking';
import { HolidayAccount } from '../models/HolidayAccount';
import { HolidayRequest } from '../models/HolidayRequest';
import { HolidayDay } from '../models/HolidayDay';
import { Period } from '../models/Period';
import { AbscenceType } from '../models/AbscenceType';
import { WorkTimeAccount } from '../models/WorkTimeAccount';
import { Overtime } from '../models/Overtime';
import { FTARequest } from '../models/FTARequest';
import { DateTime } from 'luxon';
import { SettingsProvider } from './SettingsProvider';
import { WTALeave } from '../models/WTALeave';
import { WTA } from 'src/models/WTA';
import { element } from 'protractor';
import { News } from '../models/News';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { Filesystem, Directory } from '@capacitor/filesystem';
import write_blob from 'capacitor-blob-writer';
import * as utf8 from 'utf8';
import { Buffer } from 'buffer';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders } from '@angular/common/http';
import { async, pipe, from, Observable, throwError, of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { symlinkSync } from 'fs';
import { HttpParams, PermissionState } from '@capacitor/core';
import { Plugins } from '@capacitor/core';

const { Browser } = Plugins;

@Injectable()
export class APIProvider {
	//#region variables
	//Initializing the variables that are globally used.

	//TEST URL
	//_url = "http://devweb.gbg-ag.com/rw220/ws_mein_evint/index.php";

	//PROD TEST URL
	//_url = 'https://datatransfer.gbg-ag.com/ws_my_compleet_app/index.php';

	//PROD URL
	_url = 'https://datatransfer.gbg-ag.com/ws_mein_evint_test/index.php';

	_timeout = 4.0;
	private _isOffline: boolean = true;

	private standardHeader = {
		'Content-Type': 'application/x-www-form-urlencoded; charset="utf-8"'
	};

	private postBody = {
		method: 'POST',
		mode: 'cors',
		credentials: 'same-origin',
		headers: this.standardHeader,
		body: ''
	};

	//#endregion

	//#region constructor

	constructor(
		private http: HTTP,
		private _data: DataProvider,
		private _employeeData: EmployeeDataProvider,
		private _settings: SettingsProvider,
		private AngularHttp: HttpClient
	) {}

	//#endregion

	//#region Properties

	public get isOffline(): boolean {
		return this._isOffline;
	}

	private set isOffline(value: boolean) {
		this._isOffline = value;
		this._settings.offlineMode = value;
		if (!value) {
			this.checkForOfflineDataToPush();
		}
	}

	//#endrefion

	//#region public methods

	//#region Login & Token

	/**
	 * The function which will be called to login to evint or compleet with
	 * username: the email or shorthand identifier
	 * password: the password of the user
	 */
	public async loginFunction(username, password) {
		console.log(password);
		password = CryptoJS.MD5(password).toString(CryptoJS.enc.Hex);
		//this.addLogLine(password);
		//alert('MD5: ' + password);
		password = CryptoJS.SHA512(password).toString(CryptoJS.enc.Hex);

		let body = {
			action: 'login',
			login: username,
			password: password
		};

		let tempToken = '';

		await this.http
			.post(this._url, body, this.standardHeader)
			.then(async (data) => {
				console.log(await data);
				tempToken = await data.data;

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
			});

		return tempToken;
	}

	/**
	 * 	Check whether the token is expired - if we get the first name of the customer, the token is valid
	 */

	public async tokenExpired(token = '') {
		let response = null;

		if (token == '') {
			token = await this._data.getToken();
		}
		this.http.setRequestTimeout(10.0);

		await this.http
			.post(
				this._url,
				{
					action: 'getEmployeeInformation',
					webservice_token: token
				},
				this.standardHeader
			)
			.then(async (data) => {
				if (data.data != '') {
					let fullname;

					try {
						fullname = JSON.parse(data.data);
						response = 'success';
					} catch (e) {
						console.error('Problem mit JSON Parse in fullname');
						response = 'failed';
					}
				} else {
					response = 'failed';
				}

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (data) => {
				console.error(data);
				if (data.status == -3 || data.status == -4 || data.status == -6) {
					response = 'OFFLINE';
				} else {
					response = 'FAILED';
				}
				this.isOffline = true;
			});

		return response;
	}

	//#endregion

	//#region Employee

	public async getEmployeeInfo() {
		let response;
		const token = await this._data.getToken();
		this.http.setRequestTimeout(4.0);
		await this.http
			.post(
				this._url,
				{
					action: 'getEmployeeInformation',
					webservice_token: token
				},
				this.standardHeader
			)
			.then((data) => {
				response = JSON.parse(data.data);
				this._data.setStorage('QRCode', response.qr_code_content);

				this._employeeData.id = response.employee_id;
				this._employeeData.pnr = response.employee_pnr;
				this._employeeData.firstname = response.firstname;
				this._employeeData.lastname = response.lastname;
				this._employeeData.username = response.username;
				this._employeeData.defaultProjectId = response.default_project_id as number;
				this._employeeData.defaultActivityId = response.default_activity_id as number;

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.assignEmployeeDataFromStorage();
				console.error(error.error);
				this.isOffline = true;
			});
	}

	public async editEmployeeData() {
		const token = await this._data.getToken();

		let body = {
			action: 'editEmployeeData',
			webservice_token: token,
			CountryCode: +this._employeeData.landlineCountry,
			PhoneNumber: this._employeeData.landlineNumber,

			MobilePrefix: +this._employeeData.mobilCountry,
			MobilePostfix: this._employeeData.mobilNumber,

			AddressAddition: this._employeeData.adressTwo,
			AddressStreet: this._employeeData.adressOne,
			AddressZIP: this._employeeData.plz,
			AddressLocation: this._employeeData.city,
			selStaat: +this._employeeData.country
		};

		const header = {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Accept-Encoding': 'gzip, deflate, br'
		};

		this.http.setRequestTimeout(4.0);
		await this.http
			.post(this._url, body, this.standardHeader)
			.then((newData) => {
				console.log(newData);
			})
			.catch(async (error) => {
				console.log(error);
			});
	}

	public async getEmployeeData(update = true) {
		const token = await this._data.getToken();

		let body = {
			action: 'getEmployeeData',
			webservice_token: token
		};

		let response = false;
		if (update) {
			this.http.setRequestTimeout(4.0);
			await this.http
				.post(this._url, body, this.standardHeader)
				.then((newData) => {
					const tmp = JSON.parse(newData.data);
					//this._employeeData.firstname = tmp.employeeInfo.Vorname;
					//this._employeeData.lastname = tmp.employeeInfo.Nachname;
					this._employeeData.mail = tmp.employeeInfo.EMail;
					this._employeeData.website = tmp.employeeInfo.Webseite;
					this._employeeData.adressOne = tmp.employeeInfo.Adresszeile1;
					this._employeeData.adressTwo = tmp.employeeInfo.Adresszeile2;
					this._employeeData.plz = tmp.employeeInfo.Postleitzahl;
					this._employeeData.city = tmp.employeeInfo.Ort;
					this._employeeData.country = tmp.employeeInfo.Staat;

					this._employeeData.landlineCountry = tmp.employeeInfo.festnetz_land;
					this._employeeData.landlineNumber = tmp.employeeInfo.festnetz_nummer;

					this._employeeData.mobilCountry = tmp.employeeInfo.mobil_land;
					this._employeeData.mobilNumber = tmp.employeeInfo.mobil_nummer;
					response = true;

					if (this.isOffline) {
						this.isOffline = false;
					}
				})
				.catch(async (error) => {
					this.assignEmployeeDataFromStorage();

					this.isOffline = true;
				});
		}
		return response;
	}

	public async getCountries() {
		const token = await this._data.getToken();
		let body = {
			action: 'getCountries',
			webservice_token: token
		};

		let response = null;
		this.http.setRequestTimeout(4.0);
		await this.http
			.post(this._url, body, this.standardHeader)
			.then((newData) => {
				this._data.setStorage('countriesList', newData);
				response = newData.data;
			})
			.catch(async (error) => {
				response = this._data.getStorage('countriesList');
			});
		response = JSON.parse(response);
		return response;
	}

	//#endregion

	//#region Activities
	/**
	 * Method to get all activities from API and save them into employeeDataProvider
	 * @returns Array of all activities
	 */
	public async getActivities() {
		//let token = null;
		const token = await this._data.getToken();
		let activityList: Activity[];

		await this.http
			.post(
				this._url,
				{
					action: 'getActivities',
					webservice_token: token
				},
				this.standardHeader
			)
			.then(async (newData) => {
				const data = newData.data;
				if (data != '' && data != null) {
					this._data.setStorage('ActivityList', data);
					activityList = await this.convertActivityResponseToActivityArray(data);
				}

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				console.error(error);
				const data = await this._data.getStorage('ActivityList');
				activityList = await this.convertActivityResponseToActivityArray(data);
				this.isOffline = true;
			});

		this._employeeData.activities = await activityList;
		return await activityList;
	}
	//#endregion

	//#region Projects

	/**
	 * Method to get all projects from API and save them into employeeDataProvider
	 * @returns Array of all projects
	 */
	public async getProjects() {
		const token = await this._data.getToken();
		let response = null;
		let returnValue: Project[];

		//this.addLogLine('Getting Projects');

		await this.http
			.post(
				this._url,
				{
					action: 'getProjectList',
					webservice_token: token
				},
				this.standardHeader
			)
			.then(async (newData) => {
				const data = newData.data;
				if (data != '' && data != null) {
					this._data.setStorage('ProjectList', data);
					returnValue = await this.convertProjectsResponseToProjectList(data);
				}

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
				console.error(error);
				await this._data
					.getStorage('ProjectList')
					.then(async (projectsResponse) => {
						returnValue = await this.convertProjectsResponseToProjectList(projectsResponse);
					})
					.catch(async () => {
						//this.addLogLine('Failed to get offline projects due to ' + error);
					});
			});
		this._employeeData.projects = await returnValue;
		return await returnValue;
	}

	//#endregion

	//#region Bookings

	public async saveTrackingStamp(trackingStamp: TrackingStamp, sendingStampsFromStorage = false): Promise<boolean> {
		console.trace('saveTrackingStamp was called!', trackingStamp);
		const token = await this._data.getToken();
		let success = false;

		console.log(trackingStamp.started);

		let body = {
			action: 'saveTimestamp',
			webservice_token: token,
			timestamp_from: trackingStamp.started ? trackingStamp.started.toFormat('yyyy-MM-dd HH:mm:ss') : -1,
			timestamp_to: trackingStamp.stopped?.toFormat('yyyy-MM-dd HH:mm:ss'),
			project_id: trackingStamp.projectId,
			activity_id: trackingStamp.activityId
		};
		console.log(body);
		console.log('Actually in the correct dir');

		this.http.setRequestTimeout(4.0);
		await this.http
			.post(this._url, body, this.standardHeader)
			.then(async (newData) => {
				console.log(newData);
				if (newData.data == '') {
					alert("Your Clock is ahead of time, can't book the token");
				}

				success = true;

				if (sendingStampsFromStorage) {
					this.deleteTrackingStampFromStorage(trackingStamp);
				} else {
					if (this.isOffline) {
						this.isOffline = false;
					}
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
				console.error(error);
				if (!sendingStampsFromStorage) {
					await this.saveTrackingStampInStorage(trackingStamp);
				}
			});

		return await success;
	}

	public async deleteTimestamp(booking: Booking) {
		const token = await this._data.getToken();
		let body = {
			action: 'deleteTimestamp',
			webservice_token: token,
			stamps: booking.startStamp
		};

		this.http.setRequestTimeout(4.0);

		await this.http
			.post(this._url, body, this.standardHeader)
			.then(async (data) => {
				console.log(data);

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (data) => {
				console.error(data);
				this.isOffline = true;
			});
		if (!this.isOffline && booking.endStamp != null) {
			body = {
				action: 'deleteTimestamp',
				webservice_token: token,
				stamps: booking.endStamp
			};

			this.http.setRequestTimeout(4.0);

			await this.http
				.post(this._url, body, this.standardHeader)
				.then(async (data) => {
					console.log(data);

					if (this.isOffline) {
						this.isOffline = false;
					}
				})
				.catch(async (data) => {
					console.error(data);
					this.isOffline = true;
				});
		}
	}

	public async getBookings(date: Date | DateTime, getDescription = false) {
		const token = await this._data.getToken();
		let response = null;
		let returnValue: Booking[] = [];

		this.http.setRequestTimeout(4.0);
		await this.http
			.post(
				this._url,
				{
					action: 'getBookings',
					webservice_token: token,
					period_start: date,
					period_end: date
				},
				this.standardHeader
			)
			.then(async (newData) => {
				response = await JSON.parse(newData.data);

				let tmpArray = Object.values(response);
				let bookingsArray = (tmpArray[0] as any).bookings;

				const timeZone = this._settings.timeZone;

				//bookingsArray.forEach((booking: any) =>
				for (const { index, value } of bookingsArray.map((value, index) => ({ index, value }))) {
					console.log('START TIME');
					console.log(value.start_time);
					let start = null;
					if (value.start_time != '') {
						const startHours = (value.start_time as string).split(':')[0] as unknown as number;
						const startMinutes = (value.start_time as string).split(':')[1] as unknown as number;

						start = DateTime.fromObject(
							{
								day: date instanceof DateTime ? date.day : date.getDate(),
								month: date instanceof DateTime ? date.month : date.getMonth() + 1,
								year: date instanceof DateTime ? date.year : date.getFullYear(),
								hour: startHours,
								minute: startMinutes
							},
							{ zone: timeZone }
						);
					}

					let endTime = null;
					if (value.end_time != '') {
						const endHours = (value.end_time as string).split(':')[0] as unknown as number;
						const endMinutes = (value.end_time as string).split(':')[1] as unknown as number;
						endTime = DateTime.fromObject(
							{
								day: date instanceof DateTime ? date.day : date.getDate(),
								month: date instanceof DateTime ? date.month : date.getMonth() + 1,
								year: date instanceof DateTime ? date.year : date.getFullYear(),
								hour: endHours,
								minute: endMinutes
							},
							{ zone: timeZone }
						);
					}

					// insert Pause Element
					if (index != 0) {
						if (bookingsArray[index - 1].end_time != '' && start != null) {
							const endHours = (bookingsArray[index - 1].end_time as string).split(':')[0] as unknown as number;
							const endMinutes = (bookingsArray[index - 1].end_time as string).split(':')[1] as unknown as number;
							const previousEndTime = DateTime.fromObject(
								{
									day: date instanceof DateTime ? date.day : date.getDate(),
									month: date instanceof DateTime ? date.month : date.getMonth() + 1,
									year: date instanceof DateTime ? date.year : date.getFullYear(),
									hour: endHours,
									minute: endMinutes
								},
								{ zone: timeZone }
							);

							const diff = start.diff(previousEndTime, ['hours', 'minutes'], {
								zone: timeZone
							});

							if (diff.hours > 0 || diff.minutes > 0) {
								returnValue.push(
									new Booking(
										true,
										this._settings.timeZone,
										date instanceof DateTime ? date.toJSDate() : date,
										previousEndTime,
										start,
										null,
										null,
										diff.hours * 3600000 + diff.minutes * 60000
									)
								);
							}
						}
					}

					let diff = null;
					if (endTime != null && start != null) {
						diff = endTime.diff(start, ['hours', 'minutes'], {
							zone: timeZone
						});
					}

					const booking = new Booking(
						false,
						this._settings.timeZone,
						date instanceof DateTime ? date.toJSDate() : date,
						start,
						endTime,
						value.project_id,
						value.activity_id,
						diff != null ? diff.hours * 3600000 + diff.minutes * 60000 : value.project_time_sum * 60,
						value.event_id_start == 0 ? null : value.event_id_start,
						value.event_id_end == 0 ? null : value.event_id_end,
						utf8.decode(value.project_name),
						utf8.decode(value.activity_name)
					);

					if (getDescription) {
						this.getDescriptionForBooking(booking);
					}
					returnValue.push(booking);
				}

				this.saveBookingsInStorage(returnValue);

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				console.log(error);
				returnValue = await this.getBookingsFromStorage(date, getDescription);
				this.isOffline = true;
			});

		return await returnValue;
	}

	public async saveBookingChanges(booking: Booking) {
		console.log('saveBookingChanges called');
		console.log(booking);
		await this.deleteTimestamp(booking);

		await this.saveTrackingStamp(
			new TrackingStamp(null, booking.start, booking.end, booking.projectId, booking.activityId)
		);

		console.log(booking);
		//this.updateBookingsInStorage(booking);
	}

	public async getDescriptionForBooking(booking: Booking) {
		const token = await this._data.getToken();

		this.http.setRequestTimeout(4.0);
		await this.http
			.post(
				this._url,
				{
					action: 'getActivityText',
					webservice_token: token,
					date: booking.date
				},
				this.standardHeader
			)
			.then((data) => {
				this.saveBookingDescription(data.data, booking);
				if (data.data != '') {
					const parsedData = JSON.parse(data.data);

					for (const key in parsedData) {
						let tmpObj = parsedData[key];
						if (tmpObj.project_id == booking.projectId) {
							if (booking.activityId == tmpObj.activity_id) {
								booking.description = atob(tmpObj.text);
								break;
							}
						}
					}
				}
			})
			.catch((error) => {
				this.getBookingDescription(booking);
			});
	}

	public async assignDescriptionToBooking(booking: Booking) {
		const token = await this._data.getToken();
		let text = null;
		let body: any = {
			action: 'setActivityText',
			webservice_token: token,
			date: booking.date,
			project_id: booking.projectId,
			activity_id: booking.activityId
		};
		if (booking.description != null) {
			if (booking.description.length > 0) {
				body.text = btoa(booking.description);
			}
		}

		this.http.setRequestTimeout(4.0);

		await this.http
			.post(this._url, body, this.standardHeader)
			.then(async (newData) => {
				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				console.error(error);
				this.isOffline = true;
			});
	}

	public async getTimeSheetPDF(mode: number, date: DateTime | Date) {
		const token = this._data.temptoken;
		const dateString = date instanceof DateTime ? date.toFormat('yyyy-MM-dd') : date.toISOString().split('T')[0];

		/*
		const body = {
			action: 'getTimeSheetPDF',
			webservice_token: token,
			viewing_period: mode,
			date: dateString
		};
		*/

		await Browser.open({
			url:
				this._url +
				'?action=getTimeSheetPDF&webservice_token=' +
				token +
				'&viewing_period=' +
				mode +
				'&date=' +
				dateString
		});

		/*
		return from(this.http.post(this._url, body, { responseType: 'blob' })).pipe(
			tap((res) => {
				console.log('Received response from API.');
				console.log(res);
			}),
			switchMap((res: any) => this.saveFile(this.textToBlob(res.data), 'test.pdf')),
			catchError(this.handleError)
		);
		*/
	}

	/*
	textToBlob(text: string): Blob {
		// Convert string to Buffer
		const buffer = Buffer.from(text, 'binary');

		// Convert Buffer to base64
		const base64 = buffer.toString('base64');

		// Convert base64 to blob and return
		const byteCharacters = atob(base64);
		const byteNumbers = new Array(byteCharacters.length);
		for (let i = 0; i < byteCharacters.length; i++) {
			byteNumbers[i] = byteCharacters.charCodeAt(i);
		}
		const byteArray = new Uint8Array(byteNumbers);
		return new Blob([byteArray], { type: 'application/pdf' });
	}

	async saveFile(blob: Blob, filename: string) {
		console.log('Saving file...');

		return from(this.blobToBase64(blob)).pipe(
			tap((base64Data: string) => {
				console.log('base64Data:', base64Data.substring(0, 100)); // Output first 100 characters of base64Data
			}),
			switchMap((base64Data: string) => {
				console.log('Converted Blob to base64.');

				return from(
					Filesystem.writeFile({
						path: filename,
						data: base64Data,
						directory: Directory.External
					})
				).pipe(
					tap(() => console.log('File saved successfully.')),
					catchError((err) => {
						console.error('An error occurred when saving the file:', err);
						return of(err);
					})
				);
			}),
			catchError((err) => {
				console.error('An error occurred when converting blob to base64:', err);
				return of(err);
			})
		);
	}

	private blobToBase64 = (blob: Blob) =>
		new Promise((resolve, reject) => {
			const reader = this.getFileReader();
			reader.onerror = reject;
			reader.onload = () => {
				// Remove the prefix such as "data:application/pdf;base64," from the result.
				const base64Data = (reader.result as string).split(',')[1];
				resolve(base64Data);
			};
			reader.readAsDataURL(blob);
		});

	getFileReader(): FileReader {
		const fileReader = new FileReader();
		const zoneOriginalInstance = (fileReader as any)['__zone_symbol__originalInstance'];
		return zoneOriginalInstance || fileReader;
	}

	private handleError(error) {
		console.error('An error occurred', error);
		return throwError('Something bad happened; please try again later.');
	}

	private base64ToBlob(base64: string, contentType: string = ''): Blob {
		const sliceSize = 1024;
		const byteCharacters = atob(base64);
		const bytesLength = byteCharacters.length;
		const slicesCount = Math.ceil(bytesLength / sliceSize);
		const byteArrays = new Array(slicesCount);

		for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
			const begin = sliceIndex * sliceSize;
			const end = Math.min(begin + sliceSize, bytesLength);

			const bytes = new Array(end - begin);
			for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
				bytes[i] = byteCharacters[offset].charCodeAt(0);
			}
			byteArrays[sliceIndex] = new Uint8Array(bytes);
		}

		return new Blob(byteArrays, { type: contentType });
	}
	*/

	//#endregion

	//#region Holiday

	public async getHolidays(getPastYears = false): Promise<HolidayAccount[]> | null {
		let response: HolidayAccount[] = null;
		const token = await this._data.getToken();

		this.http.setRequestTimeout(8.0);
		let bufferData = null;

		await this.http
			.post(
				this._url,
				{
					action: 'getHolidayAccount',
					webservice_token: token,
					getPastYears: getPastYears
				},
				this.standardHeader
			)
			.then((data) => {
				bufferData = data;
				this._data.setStorage('BufferHoliday', data);
				if (this.isOffline) {
					this.isOffline = false;
				}

				//this.storage.set('leaveAccount', response);
			})
			.catch(async (data) => {
				console.error(data);
				bufferData = await this._data.getStorage('BufferHoliday');
				this.isOffline = true;
			});

		if (bufferData == null) {
			return null;
		}
		const parsedJson = JSON.parse(bufferData.data);
		response = [];

		for (const key in parsedJson) {
			let tmpObj = parsedJson[key];
			response.push(
				new HolidayAccount(
					key as unknown as number,
					+(tmpObj.annual_contingent + '').replace('-', ''),
					+(tmpObj.scheduled_leave + '').replace('-', ''),
					+(tmpObj.approved_leave + '').replace('-', ''),
					+(tmpObj.available_leave + '').replace('-', ''),
					+(tmpObj.requested_leave + '').replace('-', ''),
					+(tmpObj.special_entries_amount + '').replace('-', ''),
					+(tmpObj.taken_leave + '').replace('-', ''),
					+(tmpObj.remaining_leave + '').replace('-', '')
				)
			);
		}

		return response;
	}

	public async getHolidayRequests(year?: number) {
		let returnValue: HolidayRequest[] = [];
		const token = await this._data.getToken();
		const actualYear = year ?? new Date().getFullYear();

		this.http.setRequestTimeout(20.0);

		let body = {
			action: 'getHolidayRequests',
			webservice_token: token,
			year: actualYear,
			encoding: 'ISO-8859-1'
		};
		let response;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				this._data.setStorage('getHolidayRequests', data.data);
				response = data.data;

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				response = await this._data.getStorage('getHolidayRequests');
				this.isOffline = true;
			});

		if (response == null) {
			return null;
		}

		response = JSON.parse(response);

		Object.entries(response).forEach(([key, value]) => {
			if (value != null) {
				(value as Array<object>).forEach((responseValue) => {
					const info = responseValue['info'];
					const gen = responseValue['general'];

					let days: HolidayDay[] = [];
					let periods: any[] = [];
					let periodId = 0;
					let requestID = null;

					info.forEach((inf) => {
						let daysCount = 0;

						for (const key in inf.days) {
							days.push(
								new HolidayDay(
									new Date(inf.days[key].date),
									inf.days[key].request_day_type,
									inf.days[key].request_day_type_id,
									inf.days[key].amount,
									periodId
								)
							);
							daysCount++;
						}

						const endDate = new Date(inf.period.end_date).setHours(23, 59, 59, 999);
						periods.push(new Period(periodId, new Date(inf.period.start_date), new Date(endDate), daysCount));

						periodId++;
					});

					let requestComment = null;
					if (gen.request_comment != '' && gen.request_comment != 'null') {
						requestComment = gen.request_comment;
					}

					returnValue.push(
						new HolidayRequest(
							gen.requestID,
							gen.cost_center,
							gen.request_owner,
							gen.pnr,
							gen.request_status as number,
							gen.request_status_type,
							periods,
							days,
							gen.requested_by,
							new Date(gen.requested_on),
							gen.requested_location_info,
							requestComment,
							gen.response_comment
						)
					);
				});
			}
		});

		return await returnValue;
	}

	public async saveHolidayReq(start: Date | DateTime, end: Date | DateTime, absence: AbscenceType, comment: string) {
		let response;
		const token = await this._data.getToken();

		const startIso = start instanceof Date ? start.toISOString() : start.toISO();
		const endIso = end instanceof Date ? end.toISOString() : end.toISO();

		let body = {
			action: 'saveHolidayRequest',
			webservice_token: token,
			request_start_date: startIso,
			request_end_date: endIso,
			request_absence_id: absence.id,
			request_comment: comment,
			encoding: 'ISO-8859-1'
		};

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				response = JSON.parse(data.data);
				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				//this.setOfflineRequest(body);
				console.error(error.error);
				this.isOffline = true;
			});

		return response;
	}

	public async deleteHolidayRequest(request: HolidayRequest) {
		let response;
		const token = await this._data.getToken();

		await this.http
			.post(
				this._url,
				{
					action: 'deleteHolidayRequest',
					webservice_token: token,
					request_id: request.id
				},
				this.standardHeader
			)
			.then((data) => {
				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				//this.setOfflineRequest(body);
				console.error(error.error);
				this.isOffline = true;
			});
	}

	public async getHolidayTypes(): Promise<AbscenceType[]> {
		let response;
		const token = await this._data.getToken();

		await this.http
			.post(
				this._url,
				{
					action: 'getHolidayRequestAbsenceTypes',
					webservice_token: token,
					encoding: 'ISO-8859-1'
				},
				this.standardHeader
			)
			.then((data) => {
				//this.addLogLine("%cAbscence Data", "color: red");
				response = JSON.parse(data.data);
				let val = [];
				let prop: keyof typeof response;

				for (prop in response) {
					const tmp = response[prop];
					val.push(new AbscenceType(tmp.id, tmp.bezeichnung, prop));
				}

				response = val;
				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
			});

		return response;
	}

	//#endregion

	//#region WTA & FTA

	public async getLeaves(start: DateTime, end: DateTime): Promise<WTALeave[]> {
		let response: WTALeave[] = [];
		const token = await this._data.getToken();

		let body = {
			action: 'getLeaves',
			webservice_token: token,
			startDate: start,
			endDate: end
		};

		this.http.setRequestTimeout(20.0);
		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				const tmp = JSON.parse(data.data).Message;

				tmp.forEach((e) => {
					response.push(
						new WTALeave(
							DateTime.fromISO(e.abw_datum_iso, { zone: this._settings.timeZone, locale: 'de' }),
							e.abwstunden,
							e.bezeichnung,
							e.schluessel_id
						)
					);
				});

				this.saveLeavesInStorage(response);

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
				const dateDiff = end.diff(start, 'days');

				for (let i = 0; i <= dateDiff.days; i++) {
					const date = start.plus({ days: i });
					const leaves = await this.getLeavesFromStorage(date);
					response.push(...leaves);
				}
			});

		return response;
	}

	public async getWTA(datetime: DateTime): Promise<WTA | null> {
		let bufferData = null;
		let response: WTA = null;
		const token = await this._data.getToken();

		let body = {
			action: 'getWTA',
			webservice_token: token,
			datetime: datetime.toFormat('dd-MM-yyyy')
		};
		this.http.setRequestTimeout(20.0);
		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				bufferData = data as any;
				this._data.setStorage('BufferWTA', bufferData);

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
				bufferData = (await this._data.getStorage('BufferWTA')) as any;
			});

		if (bufferData != null) {
			const tmp = JSON.parse(bufferData.data).Message;

			let periodStart = tmp.period.split('-')[0].trim();
			let periodEnd = tmp.period.split('-')[1].trim();

			periodStart = DateTime.fromFormat(periodStart, 'dd.MM.yyyy', {
				zone: this._settings.timeZone
			});
			periodEnd = DateTime.fromFormat(periodEnd, 'dd.MM.yyyy', { zone: this._settings.timeZone });
			response = new WTA(
				periodStart,
				periodEnd,
				tmp.target_working_time,
				tmp.monthly_target_working_time,
				tmp.payed_absence_times,
				tmp.current_working_time,
				tmp.left_over_target_working_time,
				tmp.scheduled_target_working_time,
				tmp.predicted_overtime
			);
		}

		return response;
	}

	public async getWorkTimeAccount(update = true): Promise<WorkTimeAccount | null> {
		let response: WorkTimeAccount;
		const token = await this._data.getToken();

		let body = {
			action: 'getWorktimeAccounts',
			webservice_token: token
		};

		if (update) {
			this.http.setRequestTimeout(20.0);
			await this.http
				.post(this._url, body, this.standardHeader)
				.then((data) => {
					const tmp = JSON.parse(data.data)[0];
					response = new WorkTimeAccount(
						tmp.worktime_account_id,
						tmp.worktime_account_name,
						new Date(tmp.period_end),
						tmp.balance_old,
						tmp.balance_new,
						tmp.overtime,
						tmp.account_available_for_ftc,
						tmp.available_ftc_hours
					);

					if (this.isOffline) {
						this.isOffline = false;
					}
				})
				.catch(async (error) => {
					console.error(error);
					this.isOffline = true;
				});
		} else {
			/*
			await this.storage.get('WorktimeAccount').then((data) => {
				if (data != undefined && data != '' && data != null) {
					response = JSON.parse(data);
				}
			});
			*/
		}

		return response;
	}

	public async getOvertime(): Promise<Overtime | null> {
		const token = await this._data.getToken();
		let body = {
			action: 'calculateOvertime',
			webservice_token: token
		};

		let response = null;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				const tmp = JSON.parse(data.data);
				response = new Overtime(tmp.Old_Value, tmp.overtime, tmp.Projection);

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
			});

		return response;
	}

	public async getFTARequests(update = true): Promise<FTARequest[] | null> {
		let response: FTARequest[] = null;
		const token = await this._data.getToken();

		let body = {
			action: 'getFTCRequests',
			webservice_token: token
		};

		if (update) {
			await this.http
				.post(this._url, body, this.standardHeader)
				.then((data) => {
					if (data.data != '' || data.data != null) {
						let tmp = JSON.parse(data.data);

						response = [];

						let property: keyof typeof tmp;
						for (property in tmp) {
							const req = tmp[property];

							req.forEach((period) => {
								const temp = period[0];
								response.push(
									new FTARequest(
										temp.mitarbeiter_id,
										temp.id,
										temp.kostenstellen_bezeichnung,
										temp.azk_id,
										temp.gestellt_von_name,
										new Date(temp.gestellt_am + ' +0100'),
										temp.gestellt_von,
										temp.bearbeitet_von_name,
										temp.beantragte_stunden,
										new Date(temp.zeitraum_start + ' +0100'),
										new Date(temp.zeitraum_ende + ' +0100'),
										temp.status_typ_id,
										temp.status_autom_angepasst,
										new Date(temp.status_bearbeitet_am + ' +0100'),
										temp.status_bearbeitet_von,
										temp.antrag_kommentar,
										temp.antwort_kommentar,
										temp.is_burda_special_wtc_request,
										temp.zur_kenntnis_genommen,
										temp.zur_kenntnis_genommen_von,
										temp.zur_kenntnis_genommen_von_name,
										temp.zur_kenntnis_genommen_am
									)
								);
							});
						}
					}

					if (this.isOffline) {
						this.isOffline = false;
					}
				})
				.catch(async (error) => {
					console.error(error);
					this.isOffline = true;
					/*
								await this.storage.get('FTCRequests').then((data) => {
					if (data != undefined && data != '' && data != null) {
						response = JSON.parse(data);
					}
				});
				*/
				});
		} else {
			/*
			await this.storage.get('FTCRequests').then((data) => {
				if (data != undefined && data != '' && data != null) {
					response = JSON.parse(data);
				}
			});
			*/
		}
		return response;
	}

	//#endregion

	//#region Settings

	public async getAuths() {
		const token = await this._data.getToken();

		let body = {
			action: 'getAuths',
			webservice_token: token
		};

		let response = null;

		this.http
			.post(this._url, body, this.standardHeader)
			.then(async (data) => {
				response = JSON.parse(data.data).data;
				this._employeeData.hasRightForTimeTracking = response.zeiterfassung as boolean;
				this._employeeData.hasRightForTimeSheet = response.stundenzettel_anzeigen as boolean;
				this._employeeData.hasRightForHolidayAccount = response.urlaubskonto as boolean;
				this._employeeData.hasRightForWorkTimeAccount = response.arbeitszeitkonto as boolean;

				this._employeeData.hasRightForAllowancesOverview = response.zulagenuebersicht as boolean;
				this._employeeData.hasRightForDescriptionTimeSheet = response.stundenzettel_taetigkeitsbeschreibung as boolean;
				this._employeeData.hasRightForEditTimeSheet = response.stundenzettel_bearbeiten as boolean;
				this._employeeData.hasRightForMessagesToSuperior = response.email_an_vorgesetzter as boolean;
				this._employeeData.hasRightForMonthOverview = response.monatsbericht as boolean;
				this._employeeData.hasRightForShiftPlan = response.schichtplan as boolean;

				// TODO: replace the response to correct response
				this._employeeData.hasRightForOnlyQR = response.QR_tracking_only as boolean;

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
				this.assignEmployeeRightsFromStorage();
			});
	}

	public async getShifts(startDate: string, endDate: string) {
		const token = await this._data.getToken();

		let body = {
			action: 'getShiftPlanning',
			webservice_token: token,
			period_start: startDate,
			period_end: endDate
		};

		let response = null;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				this._data.setStorage('ShiftData', data);
				response = data;
			})
			.catch(() => {
				response = this._data.getStorage('ShiftData');
			});

		if (response == null) {
			return;
		}

		let tmp = JSON.parse(response.data);

		console.log(tmp);

		let responseArray = [];

		let tmpArray = [];
		//Object.entries(tmp).forEach(([key, value]) => console.log(`${value}`));
		tmpArray = Object.values(tmp);

		tmpArray.forEach((data) => {
			responseArray.push(
				new Shift(
					data.shift_id,
					data.date,
					data.shift_name,
					data.shift_symbol,
					data.customer_location_name,
					data.customer_location_id,
					data.project_id,
					data.project_name
				)
			);
		});
		return responseArray;
	}

	public registration(salutation, firstname, lastname, email, password, passwordconfirm) {
		let result = null;
		password = utf8.encode(password);
		passwordconfirm = utf8.encode(passwordconfirm);
		password = btoa(password);
		passwordconfirm = btoa(passwordconfirm);
		let registerBody = {
			action: 'registerDemoAccount',
			salutation: salutation,
			first_name: firstname,
			last_name: lastname,
			email: email,
			password: password,
			password_confirm: passwordconfirm
		};
		const registerData$ = from(this.http.post(this._url, registerBody, this.standardHeader));
		const response$ = registerData$.pipe(map((res) => JSON.parse(res.data)));
		return response$;
	}

	public confirmAccount(confirmationCode, email) {
		let result = null;
		let registerBody = {
			action: 'activateAccount',
			code: confirmationCode,
			email: email
		};
		const activatedAccount$ = from(this.http.post(this._url, registerBody, this.standardHeader));
		const response$ = activatedAccount$.pipe(map((res) => JSON.parse(res.data)));
		return response$;
	}

	public async getTimeZone() {
		const token = await this._data.getToken();

		let body = {
			action: 'getTimezone',
			webservice_token: token
		};

		let response = null;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				response = JSON.parse(data.data).Message;

				if (this.isOffline) {
					this.isOffline = false;
				}
			})
			.catch(async (error) => {
				this.isOffline = true;
			});

		return response;
	}

	public async registerToken(FCMToken) {
		const token = await this._data.getToken();

		let body = {
			action: 'addPushToken',
			webservice_token: token,
			push_token: FCMToken
		};

		let response = null;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				console.log('addpush: ' + data);
				response = data;
			})
			.catch((err) => (response = err));

		return response;
	}

	public async sendTestPush(FCMToken) {
		const token = await this._data.getToken();

		let body = {
			action: 'sendPushMessage',
			webservice_token: token,
			push_token: FCMToken
		};

		let response = null;

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				console.log('testpush: ' + data);
				response = data;
			})
			.catch((err) => (response = err));
		return response;
	}

	//#endregion

	async getMessages() {
		let result: News[] = [];
		let apiResult;

		let body = {
			action: 'getMessages',
			webservice_token: await this._data.getToken()
		};

		await this.http
			.post(this._url, body, this.standardHeader)
			.then((data) => {
				this._data.setStorage('MessagesData', data.data);
				apiResult = JSON.parse(data.data) as any;
				this.isOffline = false;
			})
			.catch(async (err) => {
				this.isOffline = true;
				apiResult = await this._data.getStorage('MessagesData');
				apiResult = JSON.parse(apiResult) as any;
			});

		if (apiResult.Messages) {
			apiResult.Messages.forEach(async (mes) => {
				const created = DateTime.fromFormat(mes.created_on, 'yyyy-MM-dd HH:mm:ss', {
					zone: this._settings.timeZone
				});
				let marked = await this._data.getStorage('NewsMarked-' + mes.id);
				marked = marked ? marked : false;

				let message: string = mes.message.replace(/<br\/>/gi, '\n');
				if (message.includes('&')) {
					message = message.replace(/\&Uuml\;/g, 'Ü');
					message = message.replace(/\&uuml\;/g, 'ü');
					message = message.replace(/\&Auml\;/g, 'Ä');
					message = message.replace(/\&auml\;/g, 'ä');
					message = message.replace(/\&Ouml\;/g, 'Ö');
					message = message.replace(/\&ouml\;/g, 'ö');
					message = message.replace(/\&amp\;/g, '&');
					message = message.replace(/\&szlig\;/g, 'ß');
					message = message.replace(/\&euro\;/g, '€');
					message = message.replace(/\&pound\;/g, '£');
					message = message.replace(/\&nbsp\;/g, ' ');
				}

				result.push(
					new News(mes.id, created, message, mes.pers_vorname + ' ' + mes.pers_name, marked, mes.read_on ?? false)
				);
			});

			result.sort((a, b) => a.date.toMillis() - b.date.toMillis());
		}

		return result;
	}

	async readMessage(id) {
		let success = false;
		let body = {
			action: 'readMessage',
			webservice_token: await this._data.getToken(),
			messageID: id
		};

		await this.http.post(this._url, body, this.standardHeader).then((data) => {
			success = true;
		});

		return success;
	}

	//#endregion

	//#region private Methods

	private async convertActivityResponseToActivityArray(data): Promise<Activity[]> {
		const parsedJson = await JSON.parse(data);
		let keys = Object.keys(parsedJson);

		let activityList = [];
		keys.forEach((key) => {
			let val = Object.values(parsedJson[key]);

			val.forEach((value: any) => {
				activityList.push(
					new Activity(
						value.activity_id,

						utf8.decode(value.activity_name),
						key as unknown as number
					)
				);
			});
		});

		return activityList;
	}

	private async convertProjectsResponseToProjectList(json: string): Promise<Project[]> {
		let parsedJson = await JSON.parse(json);
		let response = Object.values(parsedJson);
		let projectList = [];

		response.forEach((proj: Project) => {
			projectList.push(new Project(proj.id, utf8.decode(proj.name)));
		});

		return projectList;
	}

	public async checkForOfflineDataToPush() {
		const dataToPush = (await this._data.getStorage('existOfflineDataToPush')) as boolean;
		if (dataToPush) {
			// Tracking Stamps
			const trackingStamps = await this.getTrackingStampFromStorage();
			if (trackingStamps.length > 0) {
				let success = true;
				trackingStamps.forEach(async (ts) => {
					if (success) success = await this.saveTrackingStamp(ts, true);
				});
			}
		}
	}

	//#region EmployeeData

	private assignEmployeeDataFromStorage() {
		this._data.getStorage('employeeId').then(async (result) => {
			this._employeeData.id = result as number;
		});

		this._data.getStorage('employeePnr').then(async (result) => {
			this._employeeData.pnr = result as string;
		});

		this._data.getStorage('employeeUsername').then(async (result) => {
			this._employeeData.username = result as string;
		});

		this._data.getStorage('employeeFirstname').then(async (result) => {
			this._employeeData.firstname = result as string;
		});

		this._data.getStorage('employeeLastname').then(async (result) => {
			this._employeeData.lastname = result as string;
		});

		this._data.getStorage('employeeDefaultActivity').then(async (result) => {
			this._employeeData.defaultActivityId = result as number;
		});

		this._data.getStorage('employeeDefaultProjectId').then(async (result) => {
			this._employeeData.defaultProjectId = result as number;
		});

		this._data.getStorage('employeeWebsite').then(async (result) => {
			this._employeeData.website = result as string;
		});

		this._data.getStorage('employeeMail').then(async (result) => {
			this._employeeData.mail = result as string;
		});

		this._data.getStorage('employeeAdressOne').then(async (result) => {
			this._employeeData.adressOne = result as string;
		});

		this._data.getStorage('employeeAdressTwo').then(async (result) => {
			this._employeeData.adressTwo = result as string;
		});

		this._data.getStorage('employeeCity').then(async (result) => {
			this._employeeData.city = result;
		});

		this._data.getStorage('employeeState').then(async (result) => {
			this._employeeData.state = result;
		});

		this._data.getStorage('employeeCountry').then(async (result) => {
			this._employeeData.country = result;
		});

		this._data.getStorage('employeePlz').then(async (result) => {
			this._employeeData.plz = result;
		});
	}

	private assignEmployeeRightsFromStorage() {
		this._data.getStorage('hasRightForTimeTracking').then(async (result: boolean) => {
			this._employeeData.hasRightForTimeTracking = result ?? false;
		});

		this._data.getStorage('hasRightForTimeSheet').then(async (result: boolean) => {
			this._employeeData.hasRightForTimeSheet = result ?? false;
		});

		this._data.getStorage('hasRightForHolidayAccount').then(async (result: boolean) => {
			this._employeeData.hasRightForHolidayAccount = result ?? false;
		});

		this._data.getStorage('hasRightForWorkTimeAccount').then(async (result: boolean) => {
			this._employeeData.hasRightForWorkTimeAccount = result ?? false;
		});

		this._data.getStorage('hasRightForEditTimeSheet').then(async (result: boolean) => {
			this._employeeData.hasRightForEditTimeSheet = result ?? false;
		});

		this._data.getStorage('hasRightForDescriptionTimeSheet').then(async (result: boolean) => {
			this._employeeData.hasRightForDescriptionTimeSheet = result ?? false;
		});

		this._data.getStorage('hasRightForMonthOverview').then(async (result: boolean) => {
			this._employeeData.hasRightForMonthOverview = result ?? false;
		});

		this._data.getStorage('hasRightForShiftPlan').then(async (result: boolean) => {
			this._employeeData.hasRightForShiftPlan = result ?? false;
		});

		this._data.getStorage('hasRightForAllowancesOverview').then(async (result: boolean) => {
			this._employeeData.hasRightForAllowancesOverview = result ?? false;
		});

		this._data.getStorage('hasRightForMessagesToSuperior').then(async (result: boolean) => {
			this._employeeData.hasRightForMessagesToSuperior = result ?? false;
		});

		this._data.getStorage('hasRightForOnlyQR').then(async (result: boolean) => {
			this._employeeData.hasRightForOnlyQR = result ?? false;
		});
	}

	//#endregion

	//#region Trackingstamp

	private async saveTrackingStampInStorage(trackingStamp: TrackingStamp): Promise<void> {
		let offlineRequests = await this.getTrackingStampFromStorage();

		this._data.setStorage('existOfflineDataToPush', true);

		for (let i = 0; i < offlineRequests.length; i++) {
			if (offlineRequests[i].id == trackingStamp.id) {
				offlineRequests.splice(i);
			}
		}

		offlineRequests.push(trackingStamp);

		const offReq = JSON.stringify(offlineRequests);

		await this._data.setStorage('OfflineRequests', offReq);
	}

	public async getTrackingStampFromStorage(): Promise<TrackingStamp[]> {
		let offlineRequests = [];

		const tmp = (await this._data.getStorage('OfflineRequests')) as unknown as string;

		if (tmp != null && tmp != undefined && tmp != '[]') {
			const storageContent = JSON.parse(tmp);

			storageContent.forEach((content) => {
				let end = null;
				if (content._stopped != null) {
					end = DateTime.fromISO(content._stopped, {
						zone: this._settings.timeZone,
						locale: 'de'
					});
				}

				let start = null;
				if (content._started != null) {
					start = DateTime.fromISO(content._started, {
						zone: this._settings.timeZone,
						locale: 'de'
					});
				}

				offlineRequests.push(new TrackingStamp(content._id, start, end, content._projectId, content._activityId));
			});
		}

		console.log('offlineRequests:', offlineRequests);
		return offlineRequests;
	}

	private async deleteTrackingStampFromStorage(trackingStamp: TrackingStamp): Promise<void> {
		const dataToPush = (await this._data.getStorage('existOfflineDataToPush')) as boolean;
		if (dataToPush) {
			let trackingStamps = await this.getTrackingStampFromStorage();

			if (trackingStamps.length > 0) {
				for (let i = 0; i < trackingStamps.length; i++) {
					if (trackingStamps[i].id == trackingStamp.id) {
						trackingStamps.splice(i, 1);
					}
				}

				if (trackingStamps.length == 0) {
					this._data.setStorage('existOfflineDataToPush', false);
				}
			}

			const offReq = JSON.stringify(trackingStamps);

			await this._data.setStorage('OfflineRequests', offReq);
		}
	}

	//#endregion

	//#region WTA

	private async saveLeavesInStorage(leaves: WTALeave[]) {
		if (leaves.length > 0) {
			let datesArray = [];
			leaves.forEach((l) => {
				const lFormat = l.date.toFormat('dd.LL.yyyy');
				if (!datesArray.includes((o) => o == lFormat)) {
					datesArray.push(lFormat);
				}
			});

			datesArray.forEach(async (d) => {
				const el = leaves.filter((l) => l.date.toFormat('dd.LL.yyyy') == d);
				const offReq = JSON.stringify(el);
				await this._data.setStorage('OfflineLeaves-' + d, offReq);
			});
		}
	}

	private async saveBookingsInStorage(bookings: Booking[]) {
		if (bookings.length > 0) {
			const offReq = JSON.stringify(bookings);

			await this._data.setStorage('OfflineBookings-' + bookings[0].dateAsDateTime.toFormat('dd.LL.yyyy'), offReq);
		}
	}

	private async getLeavesFromStorage(date: DateTime): Promise<WTALeave[]> {
		let offlineRequests = [];

		const tmp = (await this._data.getStorage('OfflineLeaves-' + date.toFormat('dd.LL.yyyy'))) + '';

		if (tmp != '' && tmp != '[]' && tmp != 'null') {
			const storageContent = JSON.parse(tmp);

			storageContent.forEach((content) => {
				offlineRequests.push(
					new WTALeave(
						DateTime.fromISO(content._date, {
							zone: this._settings.timeZone,
							locale: 'de'
						}),
						content._hours,
						content._describtion,
						content._keyId
					)
				);
			});
		}
		return offlineRequests;
	}

	private async getBookingsFromStorage(date: DateTime | Date, getDescription = false): Promise<Booking[]> {
		let offlineRequests = [];

		const dateString =
			date instanceof Date ? DateTime.fromJSDate(date).toFormat('dd.LL.yyyy') : date.toFormat('dd.LL.yyyy');

		const tmp = (await this._data.getStorage('OfflineBookings-' + dateString)) + '';

		if (tmp != '' && tmp != '[]' && tmp != 'null') {
			const storageContent = JSON.parse(tmp);

			storageContent.forEach((content) => {
				let end;
				if (content._end != null) {
					end = DateTime.fromISO(content._end, {
						zone: this._settings.timeZone,
						locale: 'de'
					});
				}

				const booking = new Booking(
					content._isPauseEntry,
					this._settings.timeZone,
					new Date(content._date),
					DateTime.fromISO(content._start, { zone: this._settings.timeZone, locale: 'de' }),
					end,
					content._projectId,
					content._activityId,
					content._duration,
					content._startStamp,
					content._endStamp,
					content._projectName,
					content._activityName
				);

				if (getDescription) {
					this.getDescriptionForBooking(booking);
				}

				offlineRequests.push(booking);
			});
		}

		return offlineRequests;
	}

	private async saveBookingDescription(jsonString: string, booking: Booking) {
		const dateString = booking.dateAsDateTime.toFormat('dd.LL.yyyy');

		await this._data.setStorage('OfflineBookingsDescription-' + dateString, jsonString);
	}

	private async getBookingDescription(booking: Booking) {
		const dateString = booking.dateAsDateTime.toFormat('dd.LL.yyyy');

		const tmp = (await this._data.getStorage('OfflineBookingsDescription-' + dateString)) + '';

		if (tmp != '' && tmp != '[]' && tmp != 'null') {
			const parsedData = JSON.parse(tmp);

			for (const key in parsedData) {
				let tmpObj = parsedData[key];
				if (tmpObj.project_id == booking.projectId) {
					if (booking.activityId == tmpObj.activity_id) {
						booking.description = atob(tmpObj.text);
						break;
					}
				}
			}
		}
	}

	//#endregion

	//#endregion
}
