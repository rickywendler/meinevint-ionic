import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrackerRunningPageRoutingModule } from './tracker-running-routing.module';

import { TrackerRunningPage } from './tracker-running.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrackerRunningPageRoutingModule
  ],
  declarations: [TrackerRunningPage]
})
export class TrackerRunningPageModule {}
