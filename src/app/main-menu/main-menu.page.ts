import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { EmployeeDataProvider } from '../../providers/EmployeeDataProvider';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../providers/SettingsProvider';
import { TrackingStamp } from '../../models/TrackingStamp';
import { APIProvider } from '../../providers/APIProvider';
import { Booking } from '../../models/Booking';
import { TimeTrackingProvider } from '../../providers/TimeTrackingProvider';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { Shift } from '../../models/Shift';
import { HolidayAccount } from '../../models/HolidayAccount';
import { DataProvider } from 'src/providers/DataProvider';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.page.html',
  styleUrls: ['./main-menu.page.scss'],
})
export class MainMenuPage implements OnInit {
  public isTracking: boolean = false;
  public isPaused: boolean = false;
  public trackedTime: number;
  public trackedPause: number;
  public totalPauseTime: number = 0;
  public shifts: Shift[];
  public nextShift: Shift;
  public holiday: HolidayAccount;

  private trackerInterval;
  private allBookings: Booking[];
  private initialized = false;

  public _weekDates: DateTime[] = [];
  public _weekWorkTimeHours = 0;
  public _weekWorkTimeMinutes = 0;
  public _weekPauseTimeHours = 0;
  public _weekPauseTimeMinutes = 0;
  private _weekWorkTotalTime = 0;
  private _weekPauseTotalTime = 0;
  public _weekBookings: Booking[] = [];
  public _dayTimes: number[] = [];
  public _everythingCalculated = false;

  public _weekStart = DateTime.now()
    .setZone(this.settings.timeZone)
    .setLocale(this.lang.locale);
  public _weekEnd = DateTime.now()
    .setZone(this.settings.timeZone)
    .setLocale(this.lang.locale);
  public _displayedWeek = DateTime.now()
    .setZone(this.settings.timeZone)
    .setLocale(this.lang.locale);

  constructor(
    public lang: LanguageProvider,
    public employeeData: EmployeeDataProvider,
    private nav: NavController,
    private platform: Platform,
    private settings: SettingsProvider,
    private api: APIProvider,
    public tracker: TimeTrackingProvider,
    private data: DataProvider
  ) {}

  async ngOnInit() {
    this.platform.pause.subscribe(() => {
      this.nav.navigateRoot('/login', { replaceUrl: true });
    });

    this.api.getBookings(DateTime.now()).then((result) => this.init(result));
    const allHolidays = this.api.getHolidays(true);
    const now = DateTime.now()
      .setZone(this.settings.timeZone)
      .setLocale(this.lang.locale);

    let nowISO = now.toISO();
    nowISO = nowISO.split('T')[0];
    let endOfMonth = now.endOf('month').setLocale(this.lang.locale).toISO();
    endOfMonth = endOfMonth.split('T')[0];

    this.shifts = await this.api.getShifts(nowISO, endOfMonth);

    let shift = this.shifts[0];

    for (let index = 0; index < this.shifts.length; index++) {
      const element = this.shifts[index];
      if (
        element.shiftDate < shift.shiftDate &&
        element.shiftDate > now.toMillis()
      ) {
        shift = element;
      }
    }

    this.nextShift = shift;

    const currentYear = +DateTime.now().year;
    this.holiday = (await allHolidays).find((h) => h.year == currentYear);
  }

  /**
   * Method to initialize the main base, specialy the tracker on top
   * @param bookings
   */
  private init(bookings: Booking[]) {
    this.allBookings = bookings;
    let foundBooking = false;

    for (const i in bookings) {
      if (bookings[i].end == null) {
        this.tracker.start = bookings[i].start;
        this.tracker.projectId = bookings[i].projectId;
        this.tracker.activityId = bookings[i].activityId;
        foundBooking = true;
        // let _trackingTmp = new TrackingStamp();
        // _trackingTmp.started = bookings[i].start;
        // _trackingTmp.projectId = bookings[i].projectId;
        // _trackingTmp.activityId = bookings[i].activityId;
        if (bookings[i].isPauseEntry) {
          console.log('isPauseEntry');
          this.tracker.isPause = true;
          this.tracker.startBreak();
          //alert("I just failed hard");
          // _trackingTmp.startBreak();
          // this._isPaused = true;
        } else {
          console.log('isNOPauseEntry');
          this.tracker.startTracking(false);
          // _trackingTmp.startTracker();
          // this._isTracking = true;
        }
        // this.startIntervals();
        break;
      }
    }

    if (!foundBooking) {
      this.data.getStorage('breakStart').then((result) => {
        if (result) {
          let resultDate = DateTime.fromISO(result).setLocale(this.lang.locale);
          let now = DateTime.now();
          if (
            resultDate.day == now.day &&
            resultDate.month == now.month &&
            resultDate.year == now.year
          ) {
            this.tracker.isPause = true;
            this.tracker.startBreak(resultDate);
          }
        }
      });
    }

    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      console.log('Push registration success, token:');
      console.log(token);

      this.api.registerToken(token.value).then((data) => {
        console.log(data);
        //alert("success");
      });
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error on registration:');
      console.log(JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        console.log('Push received:');
        console.log(JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        console.log('Push action performed:');
        console.log(JSON.stringify(notification));
      }
    );

    this.calculateBreakTime();

    this.initialized = true;
  }

  /**
   * Method to start all Intervals in this page
   */
  private async startIntervals() {
    if (this.isTracking) {
      this.trackedTime = TrackingStamp.staticTrackedTime;
      this.trackerInterval = setInterval(() => {
        this.trackedTime = TrackingStamp.staticTrackedTime;
      }, 1000); // once per secound
    } else if (this.isPaused) {
      this.trackedPause = TrackingStamp.staticTrackedPause;
      this.trackerInterval = setInterval(() => {
        this.trackedPause = TrackingStamp.staticTrackedPause;
      }, 1000); // once per secound
    }
  }

  async ionViewWillEnter() {
    if (this.initialized) {
      // this._isTracking = TrackingStamp._isTracking;
      // this._isPaused = TrackingStamp._isPaused;
      // this.startIntervals();
      this.api.getBookings(DateTime.now()).then((result) => {
        this.allBookings = result;
        this.calculateBreakTime();
      });
    }

    console.log('bullshit');
    if (!this._everythingCalculated) {
      this.createWeeksArrayForDetails().then((weekDates) => {
        this._weekDates = weekDates;
        this.calculateDurationForBookings();
      });

      //this.calculateDurationForBookings();
      console.log('Worktime hours');
      console.log(this._weekWorkTimeHours);

      this._everythingCalculated = true;
    }

    /*
		const toolbar = document.getElementById('Menu-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/
  }

  ionViewDidLeave() {
    clearInterval(this.trackerInterval);
  }

  /**
   * MEthod for changedTab event
   * @param event event
   */
  changeTab(tabName) {
    switch (tabName) {
      case 'TimeTracking':
        if (this.tracker.isTracking || this.tracker.isPause) {
          this.nav.navigateForward(['/app/tabs/time-tracking/tracker-running']);
        } else {
          this.nav.navigateForward(['/app/tabs/time-tracking']);
        }
        break;
      case 'Timesheet':
        this.nav.navigateForward(['/app/tabs/timesheet-overview']);
        break;
      case 'WTA':
        this.nav.navigateForward(['/app/tabs/main-menu/worktime-account']);
        break;
      case 'HolidayAccount':
        this.nav.navigateForward(['/app/tabs/main-menu/holiday-account']);
        break;
      case 'News':
        this.nav.navigateForward('/app/tabs/newsfeed');
        break;
      case 'Settings':
        this.nav.navigateForward(['/app/tabs/settings']);
        break;
      case 'Shift':
        this.nav.navigateForward(['/app/tabs/shift-planning']);
        break;
    }
  }

  public async addBooking(value: Booking, weekDay: DateTime) {
    console.log('addBooking');
    this._weekBookings.push(value);
    if (value.isPauseEntry) {
      this._weekPauseTotalTime += value.duration / 1000;

      let hours = (this._weekPauseTotalTime / 3600).toString().split('.')[0];
      let minutes = ((this._weekPauseTotalTime % 3600) / 60)
        .toString()
        .split('.')[0];

      this._weekPauseTimeHours = +hours;
      this._weekPauseTimeMinutes = +minutes;
    } else {
      this._weekWorkTotalTime += value.duration / 1000;

      let hours = (this._weekWorkTotalTime / 3600).toString().split('.')[0];
      let minutes = ((this._weekWorkTotalTime % 3600) / 60)
        .toString()
        .split('.')[0];

      this._weekWorkTimeHours = +hours;
      this._weekWorkTimeMinutes = +minutes;
    }

    if (
      this._dayTimes[weekDay.toISO()] != null &&
      this._dayTimes[weekDay.toISO()] != undefined
    ) {
      this._dayTimes[weekDay.toISO()] += value.duration;
    } else {
      this._dayTimes[weekDay.toISO()] = value.duration;
    }
    console.log('In addbooking weektimehours');
    console.log(this._weekWorkTimeHours);
    console.log(this._weekWorkTimeMinutes);
  }

  private async createWeeksArrayForDetails(): Promise<DateTime[]> {
    let i = 0;
    let newDate: DateTime = null;
    let returnArray = [];
    let isoDate;
    const now = DateTime.now();
    if (this._weekEnd.valueOf() > now.valueOf()) {
      isoDate = now.toISO();
    } else {
      isoDate = this._weekEnd.toISO();
    }

    do {
      newDate = DateTime.fromISO(isoDate, {
        locale: this.lang.locale,
      });

      newDate = newDate.minus({ days: i });

      returnArray.push(newDate);
      i++;
    } while (newDate.weekday > 1);
    return returnArray;
  }

  private async calculateDurationForBookings() {
    console.log('calculateDurationForBooking');
    console.log(this._weekDates);
    this._weekDates.forEach((weekDay: DateTime) => {
      this.api.getBookings(weekDay).then((bookingArray) => {
        bookingArray.forEach((element) => {
          this.addBooking(element, weekDay);
        });
      });
    });
  }

  /**
   * method to calculate the break time
   */

  private async calculateBreakTime() {
    let tmpTime = 0;

    this.allBookings.forEach((data) => {
      if (data.isPauseEntry) {
        tmpTime += data.duration;
      }
    });
    this.totalPauseTime = tmpTime / 60000; // divided by 60.000 to get the minutes
  }

  startTracking() {
    this.nav.navigateForward(['/app/tabs/time-tracking'], {
      queryParams: { stampIn: true },
    });
  }
}
