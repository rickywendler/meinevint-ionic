import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditStampPageRoutingModule } from './edit-stamp-routing.module';

import { EditStampPage } from './edit-stamp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditStampPageRoutingModule
  ],
  declarations: [EditStampPage]
})
export class EditStampPageModule {}
