import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DateSelectionPageRoutingModule } from './date-selection-routing.module';

import { DateSelectionPage } from './date-selection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DateSelectionPageRoutingModule
  ],
  declarations: [DateSelectionPage]
})
export class DateSelectionPageModule {}
