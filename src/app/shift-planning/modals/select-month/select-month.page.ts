import { Component, OnInit, Input } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { DateTime } from 'luxon';
import { ModalController } from '@ionic/angular';
import { SettingsProvider } from '../../../../providers/SettingsProvider';

@Component({
	selector: 'app-select-month',
	templateUrl: './select-month.page.html',
	styleUrls: ['./select-month.page.scss']
})
export class SelectMonthPage implements OnInit {
	@Input() _initalMonth = '';
	public _months: monthSelection[] = [];
	private _selectedMonth: string = '';

	constructor(public _lang: LanguageProvider, private modal: ModalController, private _settings: SettingsProvider) {}

	async ngOnInit() {
		let i = DateTime.now().setLocale(this._lang.locale);
		let j = DateTime.local(i.year, i.month + 1, i.day);
		console.log(j.monthLong);

		let iter = 1;
		while (iter <= 12) {
			let sampleDate = DateTime.local(i.year, iter, 1).setLocale(this._lang.locale);
			this._months.push({
				display: sampleDate.monthLong,
				value: iter + ''
			});
			iter++;
		}

		this.selectedMonth = this._initalMonth + '';
	}

	/**
	 * Getter selectedMonth
	 * @return {string }
	 */
	public get selectedMonth(): string {
		return this._selectedMonth;
	}

	/**
	 * Setter selectedMonth
	 * @param {string } value
	 */
	public set selectedMonth(value: string) {
		this._selectedMonth = value;
		console.log('Selected month: ' + this._selectedMonth);
		this.modal.dismiss({ monthNumber: this.selectedMonth });
	}
}
interface monthSelection {
	display: string;
	value: string;
}
