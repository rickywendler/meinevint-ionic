import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { DateTime } from 'luxon';

@Component({
	selector: 'app-select-time-popover',
	templateUrl: './select-time-popover.component.html',
	styleUrls: ['./select-time-popover.component.scss'],
})
export class SelectTimePopoverComponent implements OnInit {
	constructor(
		public _lang: LanguageProvider,
		private _settings: SettingsProvider,
		private popover: PopoverController
	) {}

	ngOnInit() {}

	@Input() private _time: DateTime;

	public set time(value: string) {
		this._time = DateTime.fromISO(value, {
			locale: this._lang.locale,
		});
	}

	public get time(): string {
		return this._time.toISO();
	}

	cancel() {
		this.popover.dismiss(null, 'cancel');
	}

	confirm(e) {
		this.time = e.target.value;
		this.popover.dismiss({ time: this.time }, 'success');
	}
}
