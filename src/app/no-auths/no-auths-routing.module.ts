import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoAuthsPage } from './no-auths.page';

const routes: Routes = [
  {
    path: '',
    component: NoAuthsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoAuthsPageRoutingModule {}
