import { Period } from './Period';
export class FTARequest {
	//#region variables

	private _employeeId: number;
	private _id: number;
	private _costcenter: string;
	private _azkId: number;

	private _createdByName: string;
	private _createdOn: Date;
	private _createdBy: number;

	private _editedByName: string;

	private _hours: number;
	private _start: Date;
	private _end: Date;

	private _statusId: number;
	private _statusAutomaticEdited: string;
	private _statusEditedOn: Date;
	private _statusEditedBy: string;

	private _requestComment: string;
	private _answerComment: string;

	private _isBurdaWtcRequest: string;

	private _recognized: any;
	private _recognizedBy: number;
	private _recognizedByName: number;
	private _recognizedOn: Date;

	private _calculatedHours: number;

	//#endregion

	//#region constructor

	constructor(
		employeeId?: number,
		id?: number,
		costcenter?: string,
		azkId?: number,
		createdByName?: string,
		createdOn?: Date,
		createdBy?: number,
		editedByName?: string,
		hours?: number,
		start?: Date,
		end?: Date,
		statusId?: number,
		statusAutomaticEdited?: string,
		statusEditedOn?: Date,
		statusEditedBy?: string,
		requestComment?: string,
		answerComment?: string,
		isBurdaWtcRequest?: string,
		recognized?: any,
		recognizedBy?: number,
		recognizedByName?: number,
		recognizedOn?: Date,
		calculatedHours?: number
	) {
		this._employeeId = employeeId ?? null;
		this._id = id ?? null;
		this._costcenter = costcenter ?? null;
		this._azkId = azkId ?? null;
		this._createdByName = createdByName ?? null;
		this._createdOn = createdOn ?? null;
		this._createdBy = createdBy ?? null;
		this._editedByName = editedByName ?? null;
		this._hours = hours ?? null;
		this._start = start ?? null;
		this._end = end ?? null;
		this._statusId = statusId ?? null;
		this._statusAutomaticEdited = statusAutomaticEdited ?? null;
		this._statusEditedOn = statusEditedOn ?? null;
		this._statusEditedBy = statusEditedBy ?? null;
		this._requestComment = requestComment ?? null;
		this._answerComment = answerComment ?? null;
		this._isBurdaWtcRequest = isBurdaWtcRequest ?? null;
		this._recognized = recognized ?? null;
		this._recognizedBy = recognizedBy ?? null;
		this._recognizedByName = recognizedByName ?? null;
		this._recognizedOn = recognizedOn ?? null;
		this._calculatedHours = calculatedHours ?? null;
	}

	//#endregion

	//#region getter and setter

	/**
	 * Getter employeeId
	 * @return {number}
	 */
	public get employeeId(): number {
		return this._employeeId;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Getter costcenter
	 * @return {string}
	 */
	public get costcenter(): string {
		return this._costcenter;
	}

	/**
	 * Getter azkId
	 * @return {number}
	 */
	public get azkId(): number {
		return this._azkId;
	}

	/**
	 * Getter createdByName
	 * @return {string}
	 */
	public get createdByName(): string {
		return this._createdByName;
	}

	/**
	 * Getter createdOn
	 * @return {Date}
	 */
	public get createdOn(): Date {
		return this._createdOn;
	}

	/**
	 * Getter createdBy
	 * @return {number}
	 */
	public get createdBy(): number {
		return this._createdBy;
	}

	/**
	 * Getter editedByName
	 * @return {string}
	 */
	public get editedByName(): string {
		return this._editedByName;
	}

	/**
	 * Getter hours
	 * @return {number}
	 */
	public get hours(): number {
		return this._hours;
	}

	/**
	 * Getter start
	 * @return {Date}
	 */
	public get start(): Date {
		return this._start;
	}

	/**
	 * Getter end
	 * @return {Date}
	 */
	public get end(): Date {
		return this._end;
	}

	/**
	 * Getter statusId
	 * @return {number}
	 */
	public get statusId(): number {
		return this._statusId;
	}

	/**
	 * Getter statusAutomaticEdited
	 * @return {string}
	 */
	public get statusAutomaticEdited(): string {
		return this._statusAutomaticEdited;
	}

	/**
	 * Getter statusEditedOn
	 * @return {Date}
	 */
	public get statusEditedOn(): Date {
		return this._statusEditedOn;
	}

	/**
	 * Getter statusEditedBy
	 * @return {string}
	 */
	public get statusEditedBy(): string {
		return this._statusEditedBy;
	}

	/**
	 * Getter requestComment
	 * @return {string}
	 */
	public get requestComment(): string {
		return this._requestComment;
	}

	/**
	 * Getter answerComment
	 * @return {string}
	 */
	public get answerComment(): string {
		return this._answerComment;
	}

	/**
	 * Getter isBurdaWtcRequest
	 * @return {string}
	 */
	public get isBurdaWtcRequest(): string {
		return this._isBurdaWtcRequest;
	}

	/**
	 * Getter recognized
	 * @return {any}
	 */
	public get recognized(): any {
		return this._recognized;
	}

	/**
	 * Getter recognizedBy
	 * @return {number}
	 */
	public get recognizedBy(): number {
		return this._recognizedBy;
	}

	/**
	 * Getter recognizedByName
	 * @return {number}
	 */
	public get recognizedByName(): number {
		return this._recognizedByName;
	}

	/**
	 * Getter recognizedOn
	 * @return {Date}
	 */
	public get recognizedOn(): Date {
		return this._recognizedOn;
	}

	/**
	 * Setter employeeId
	 * @param {number} value
	 */
	public set employeeId(value: number) {
		this._employeeId = value;
	}

	/**
	 * Setter id
	 * @param {number} value
	 */
	public set id(value: number) {
		this._id = value;
	}

	/**
	 * Setter costcenter
	 * @param {string} value
	 */
	public set costcenter(value: string) {
		this._costcenter = value;
	}

	/**
	 * Setter azkId
	 * @param {number} value
	 */
	public set azkId(value: number) {
		this._azkId = value;
	}

	/**
	 * Setter createdByName
	 * @param {string} value
	 */
	public set createdByName(value: string) {
		this._createdByName = value;
	}

	/**
	 * Setter createdOn
	 * @param {Date} value
	 */
	public set createdOn(value: Date) {
		this._createdOn = value;
	}

	/**
	 * Setter createdBy
	 * @param {number} value
	 */
	public set createdBy(value: number) {
		this._createdBy = value;
	}

	/**
	 * Setter editedByName
	 * @param {string} value
	 */
	public set editedByName(value: string) {
		this._editedByName = value;
	}

	/**
	 * Setter hours
	 * @param {number} value
	 */
	public set hours(value: number) {
		this._hours = value;
	}

	/**
	 * Setter start
	 * @param {Date} value
	 */
	public set start(value: Date) {
		this._start = value;
	}

	/**
	 * Setter end
	 * @param {Date} value
	 */
	public set end(value: Date) {
		this._end = value;
	}

	/**
	 * Setter statusId
	 * @param {number} value
	 */
	public set statusId(value: number) {
		this._statusId = value;
	}

	/**
	 * Setter statusAutomaticEdited
	 * @param {string} value
	 */
	public set statusAutomaticEdited(value: string) {
		this._statusAutomaticEdited = value;
	}

	/**
	 * Setter statusEditedOn
	 * @param {Date} value
	 */
	public set statusEditedOn(value: Date) {
		this._statusEditedOn = value;
	}

	/**
	 * Setter statusEditedBy
	 * @param {string} value
	 */
	public set statusEditedBy(value: string) {
		this._statusEditedBy = value;
	}

	/**
	 * Setter requestComment
	 * @param {string} value
	 */
	public set requestComment(value: string) {
		this._requestComment = value;
	}

	/**
	 * Setter answerComment
	 * @param {string} value
	 */
	public set answerComment(value: string) {
		this._answerComment = value;
	}

	/**
	 * Setter isBurdaWtcRequest
	 * @param {string} value
	 */
	public set isBurdaWtcRequest(value: string) {
		this._isBurdaWtcRequest = value;
	}

	/**
	 * Setter recognized
	 * @param {any} value
	 */
	public set recognized(value: any) {
		this._recognized = value;
	}

	/**
	 * Setter recognizedBy
	 * @param {number} value
	 */
	public set recognizedBy(value: number) {
		this._recognizedBy = value;
	}

	/**
	 * Setter recognizedByName
	 * @param {number} value
	 */
	public set recognizedByName(value: number) {
		this._recognizedByName = value;
	}

	/**
	 * Setter recognizedOn
	 * @param {Date} value
	 */
	public set recognizedOn(value: Date) {
		this._recognizedOn = value;
	}

	/**
	 * Getter calculatedHours
	 * @return {number}
	 */
	public get calculatedHours(): number {
		if (this._calculatedHours == null) {
			const startTime = this._start.getTime() / 86400000;
			const totalDays = this._end.getTime() / 86400000 - startTime;

			let curDate = new Date(this._start.getTime());
			this._calculatedHours = 0;

			for (let i = 0; i <= totalDays; i++) {
				if (curDate.getDay() > 0 && curDate.getDay() < 6) {
					this._calculatedHours += 8;
				}
			}
		}
		return this._calculatedHours;
	}

	/**
	 * Setter calculatedHours
	 * @param {number} value
	 */
	public set calculatedHours(value: number) {
		this._calculatedHours = value;
	}

	//#endregion
}
