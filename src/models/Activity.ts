export class Activity {
	private _id: number;
	private _name: string;
	private _projectId: number;

	constructor(id?: number, name?: string, projectId?: number) {
		this._id = id ?? null;
		this._name = name ?? null;
		this._projectId = projectId ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Setter id
	 * @param {number} value
	 */
	public set id(value: number) {
		this._id = value;
	}

	/**
	 * Getter name
	 * @return {string}
	 */
	public get name(): string {
		return this._name;
	}

	/**
	 * Setter name
	 * @param {string} value
	 */
	public set name(value: string) {
		this._name = value;
	}

	/**
	 * Getter projectId
	 * @return {number}
	 */
	public get projectId(): number {
		return this._projectId;
	}

	/**
	 * Setter projectId
	 * @param {number} value
	 */
	public set projectId(value: number) {
		this._projectId = value;
	}
}
