import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditStampPage } from './edit-stamp.page';

const routes: Routes = [
  {
    path: '',
    component: EditStampPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditStampPageRoutingModule {}
