import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { Platform } from '@ionic/angular';

@Component({
	selector: 'app-analysis',
	templateUrl: './analysis.page.html',
	styleUrls: ['./analysis.page.scss'],
})
export class AnalysisPage implements OnInit {
	constructor(public _lang: LanguageProvider, private _platform: Platform) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		/*
		const toolbar = document.getElementById('analysis-Toolbar');
		if (this._platform.is('ios')) {
			toolbar.classList.add('iosToolbar');
		} else {
			toolbar.classList.add('androidToolbar');
		}
		*/
	}
}
