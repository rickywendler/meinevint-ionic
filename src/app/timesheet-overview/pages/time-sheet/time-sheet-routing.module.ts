import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeSheetPage } from './time-sheet.page';

const routes: Routes = [
  {
    path: '',
    component: TimeSheetPage
  },  {
    path: 'create-timestamp',
    loadChildren: () => import('./pages/create-timestamp/create-timestamp.module').then( m => m.CreateTimestampPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeSheetPageRoutingModule {}
