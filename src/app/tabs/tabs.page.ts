import { Component } from '@angular/core';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { EmployeeDataProvider } from '../../providers/EmployeeDataProvider';
import { Event, NavigationStart, Router, UrlSegment } from '@angular/router';
import { StatusBar, Style } from '@capacitor/status-bar';
import { Platform } from '@ionic/angular';
import { TimeTrackingProvider } from '../../providers/TimeTrackingProvider';

@Component({
	selector: 'app-tabs',
	templateUrl: 'tabs.page.html',
	styleUrls: ['tabs.page.scss']
})
export class TabsPage {
	public timeTrackingSelected = false;

	constructor(
		public _lang: LanguageProvider,
		public _employeeData: EmployeeDataProvider,
		private _router: Router,
		private _tracker: TimeTrackingProvider,
		private _platform: Platform
	) {
		this._router.events.subscribe((event: Event) => {
			if (event instanceof NavigationStart) {
				this.tabsWillChange(event);
			}
		});

		if (this._platform.is('ios')) {
			StatusBar.setStyle({ style: Style.Light });
		}
	}

	openTimeTracking() {
		this._router.navigate(['/app/tabs/time-tracking']);
	}

	tabsWillChange(e) {
		/*let cleanURL;
		cleanURL = e.url.replace('/noauths', '');
		console.log(cleanURL);
		console.log(e.url);*/

		if (e.url.includes('time-tracking')) {
			this.timeTrackingSelected = true;
		} else {
			this.timeTrackingSelected = false;
		}

		switch (e.url.split('?')[0]) {
			case '/app/tabs/time-tracking':
				if (!this._employeeData.hasRightForTimeTracking && !this._employeeData.hasRightForOnlyQR) {
					let nav = this._router.getCurrentNavigation();
					if (
						nav.extractedUrl.root.children.primary.segments[nav.extractedUrl.root.children.primary.segments.length - 1]
							.path !== 'noauths'
					) {
						nav.extractedUrl.root.children.primary.segments.push(new UrlSegment('noauths', {}));
					}
				} else if (this._tracker.isTracking || this._tracker.isPause) {
					let nav = this._router.getCurrentNavigation();
					if (
						nav.extractedUrl.root.children.primary.segments[nav.extractedUrl.root.children.primary.segments.length - 1]
							.path !== 'tracker-running'
					) {
						nav.extractedUrl.root.children.primary.segments.push(new UrlSegment('tracker-running', {}));
					}
				}
				break;

			case '/app/tabs/timesheet-overview':
				if (!this._employeeData.hasRightForTimeSheet) {
					let nav = this._router.getCurrentNavigation();
					if (
						nav.extractedUrl.root.children.primary.segments[nav.extractedUrl.root.children.primary.segments.length - 1]
							.path !== 'noauths'
					) {
						nav.extractedUrl.root.children.primary.segments.push(new UrlSegment('noauths', {}));
					}
				}
				break;

			case '/app/tabs/main-menu/worktime-account':
				if (!this._employeeData.hasRightForWorkTimeAccount) {
					let nav = this._router.getCurrentNavigation();
					if (
						nav.extractedUrl.root.children.primary.segments[nav.extractedUrl.root.children.primary.segments.length - 1]
							.path !== 'noauths'
					) {
						nav.extractedUrl.root.children.primary.segments.push(new UrlSegment('noauths', {}));
					}
				}
				break;

			case '/app/tabs/main-menu/holiday-account':
				if (!this._employeeData.hasRightForHolidayAccount) {
					let nav = this._router.getCurrentNavigation();
					if (
						nav.extractedUrl.root.children.primary.segments[nav.extractedUrl.root.children.primary.segments.length - 1]
							.path !== 'noauths'
					) {
						nav.extractedUrl.root.children.primary.segments.push(new UrlSegment('noauths', {}));
					}
				}
				break;
		}
	}
}
