import { DateTime } from 'luxon';

export class WTA {
	private _periodStart: DateTime = null;
	private _periodEnd: DateTime = null;
	private _target: number = null;
	private _isMonthlyTarget: boolean = null;
	private _payedAbsenceTimes: number = null;
	private _currentWorkingTime: number = null;
	private _leftOver: number = null;
	private _scheduled: number = null;
	private _predicted: number = null;

	constructor(
		periodStart?: DateTime,
		periodEnd?: DateTime,
		target?: number,
		isMonthlyTarget?: boolean,
		payedAbsenceTimes?: number,
		currentWorkingTime?: number,
		leftOver?: number,
		scheduled?: number,
		predicted?: number
	) {
		this._periodStart = periodStart ?? null;
		this._periodEnd = periodEnd ?? null;
		this._target = target ?? null;
		this._isMonthlyTarget = isMonthlyTarget ?? null;
		this._payedAbsenceTimes = payedAbsenceTimes ?? null;
		this._currentWorkingTime = currentWorkingTime ?? null;
		this._leftOver = leftOver ?? null;
		this._scheduled = scheduled ?? null;
		this._predicted = predicted ?? null;
	}

	/**
	 * Getter periodStart
	 * @return {DateTime }
	 */
	public get periodStart(): DateTime {
		return this._periodStart;
	}

	/**
	 * Getter periodEnd
	 * @return {DateTime }
	 */
	public get periodEnd(): DateTime {
		return this._periodEnd;
	}

	/**
	 * Getter target
	 * @return {number }
	 */
	public get target(): number {
		return this._target;
	}

	/**
	 * Getter isMonthlyTarget
	 * @return {boolean }
	 */
	public get isMonthlyTarget(): boolean {
		return this._isMonthlyTarget;
	}

	/**
	 * Getter payedAbsenceTimes
	 * @return {number }
	 */
	public get payedAbsenceTimes(): number {
		return this._payedAbsenceTimes;
	}

	/**
	 * Getter currentWorkingTime
	 * @return {number }
	 */
	public get currentWorkingTime(): number {
		return this._currentWorkingTime;
	}

	/**
	 * Getter leftOver
	 * @return {number }
	 */
	public get leftOver(): number {
		return this._leftOver;
	}

	/**
	 * Getter scheduled
	 * @return {number }
	 */
	public get scheduled(): number {
		return this._scheduled;
	}

	/**
	 * Getter predicted
	 * @return {number }
	 */
	public get predicted(): number {
		return this._predicted;
	}

	/**
	 * Setter periodStart
	 * @param {DateTime } value
	 */
	public set periodStart(value: DateTime) {
		this._periodStart = value;
	}

	/**
	 * Setter periodEnd
	 * @param {DateTime } value
	 */
	public set periodEnd(value: DateTime) {
		this._periodEnd = value;
	}

	/**
	 * Setter target
	 * @param {number } value
	 */
	public set target(value: number) {
		this._target = value;
	}

	/**
	 * Setter isMonthlyTarget
	 * @param {boolean } value
	 */
	public set isMonthlyTarget(value: boolean) {
		this._isMonthlyTarget = value;
	}

	/**
	 * Setter payedAbsenceTimes
	 * @param {number } value
	 */
	public set payedAbsenceTimes(value: number) {
		this._payedAbsenceTimes = value;
	}

	/**
	 * Setter currentWorkingTime
	 * @param {number } value
	 */
	public set currentWorkingTime(value: number) {
		this._currentWorkingTime = value;
	}

	/**
	 * Setter leftOver
	 * @param {number } value
	 */
	public set leftOver(value: number) {
		this._leftOver = value;
	}

	/**
	 * Setter scheduled
	 * @param {number } value
	 */
	public set scheduled(value: number) {
		this._scheduled = value;
	}

	/**
	 * Setter predicted
	 * @param {number } value
	 */
	public set predicted(value: number) {
		this._predicted = value;
	}
}
