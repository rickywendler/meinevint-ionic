import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecurityPage } from './security.page';

const routes: Routes = [
	{
		path: '',
		component: SecurityPage,
	},
	{
		path: 'pin-change',
		loadChildren: () =>
			import('./pages/pin-change/pin-change.module').then((m) => m.PinChangePageModule),
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SecurityPageRoutingModule {}
