import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
	selector: 'app-no-auths',
	templateUrl: './no-auths.page.html',
	styleUrls: ['./no-auths.page.scss'],
})
export class NoAuthsPage implements OnInit {
	public page;

	constructor(private route: ActivatedRoute, private router: Router) {
		this.route.queryParams.subscribe((params) => {
			const asd = this.router.getCurrentNavigation();
			let image = 'timerecord';

			switch (asd.extractedUrl.root.children.primary.segments[2].toString()) {
				case 'timesheet-overview':
					image = 'timesheet';
					break;
				default:
					switch (asd.extractedUrl.root.children.primary.segments[3].toString()) {
						case 'worktime-account':
							image = 'wta';
							break;
						case 'holiday-account':
							image = 'vacation';
							break;
					}
					break;
			}
			this.page = '../../assets/Images/lockscreens/' + image + '.png';
			console.log('IN NO AUTHS: ' + this.page);
		});
	}

	ngOnInit() {}
}
