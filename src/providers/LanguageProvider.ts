import { Injectable, Inject } from '@angular/core';
import * as EnglishFile from '../assets/language/english.json';
import * as GermanFile from '../assets/language/german.json';
import * as SpanishFile from '../assets/language/spanish.json';
import * as ItalianFile from '../assets/language/italian.json';
import * as PolishFile from '../assets/language/polish.json';
import * as FrenchFile from '../assets/language/french.json';
import { dispatch } from 'rxjs/internal/observable/pairs';
import { DataProvider } from './DataProvider';

@Injectable()
export class LanguageProvider {
	//This file will be used in order to grab words in all sorts of languages.

	public _dict: any;
	private _currentLanguage = 'german';
	private _locale = 'de';

	/**
	 * Constructor for languageProvider
	 */
	constructor() {
		this.assignLanguageDictToCurrentLanguage();
	}

	/**
	 * Method to set the Language for the app
	 * @param newLang the new language
	 */
	async setLanguage(newLang: string) {
		this._currentLanguage = newLang;
		this.assignLanguageDictToCurrentLanguage();
	}

	/**
	 * Function to get the current language for the App
	 * @returns the current language as string
	 */
	getLanguage() {
		return this._currentLanguage;
	}

	public get locale(): string {
		return this._locale;
	}

	/**
	 * Function to assign the current Language Dictionary
	 */
	private async assignLanguageDictToCurrentLanguage() {
		await fetch('../assets/language/' + this._currentLanguage + '.json', {})
			.then(async (data) => {
				console.log(data);
				const langObject = await data.json();
				this._dict = langObject.Dictionary;
				this._locale = langObject.locale;
			})
			.catch((error) => {
				console.log(error);
				console.log('WELL --- NOT TODAY');
			});
	}
}
