import { Injectable, Inject } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable()
export class DataProvider {
	//This file will be used for data collection

	private _storage: Storage | null = null;
	private tempToken = null;

	public token = null;

	constructor(private storage: Storage) {
		this.init();
	}

	async init() {
		// If using a custom driver:
		// await this.storage.defineDriver(MyCustomDriver)
		const storage = await this.storage.create();
		this._storage = storage;
	}

	/**
	 * Will return the token if there is one saved on the device.
	 * If there is no token, it will return null
	 */
	public async getToken() {
		await this._storage?.get('token').then((data) => {
			this.tempToken = data;
			//this.tempToken = "WEBSERVICE-TOKEN-64089520cc6d83.32070186";
		});

		return this.tempToken;
		//return 'WEBSERVICE-TOKEN-62e2512003e322.99808801';
	}

	/**
	 * Sets the token in the application and saves it in the storage
	 * @param {string} value the token which should be savedand used
	 */
	public setToken(value: string) {
		this._storage.set('token', value);
	}

	/**
	 *
	 * @param value the index under which values has been saved
	 * @returns
	 */
	public async getStorage(value) {
		let response = null;
		await this._storage?.get(value).then((data) => {
			response = data;
		});

		return response;
	}

	public setStorage(key, value) {
		this._storage.set(key, value);
	}

	public async clearStorage() {
		const language = await this.getStorage('currentLanguage');
		await this._storage.clear();
		this.setStorage('seenDSGVO', true);
		this.setStorage('languageHasBeenSetAtStart', true);
		this.setStorage('currentLanguage', language);
		this.tempToken = null;
		return true;
	}

	public get temptoken(): string {
		return this.tempToken;
	}
}
