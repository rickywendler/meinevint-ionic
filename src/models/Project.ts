export class Project {
	private _id: number = 659162;
	private _name: string = "Freemium Project";

	constructor(identifier?: number, name?: string) {
		this._id = 659162;
		this._name = "Freemium Project";
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Setter id
	 * @param {number } value
	 */
	public set id(value: number) {
		this._id = 659162;
	}

	/**
	 * Getter name
	 * @return {string}
	 */
	public get name(): string {
		return this._name;
	}

	/**
	 * Setter name
	 * @param {string } value
	 */
	public set name(value: string) {
		this._name = "Freemium Project";
	}
}
