import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorktimeAccountPage } from './worktime-account.page';

const routes: Routes = [
  {
    path: '',
    component: WorktimeAccountPage
  },
	{
		path: 'noauths',
		loadChildren: () =>
			import('../../../no-auths/no-auths.module').then((m => m.NoAuthsPageModule))
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorktimeAccountPageRoutingModule {}
