import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
	{
		path: 'tabs',
		component: TabsPage,
		children: [
			{
				path: 'main-menu',
				loadChildren: () => import('../main-menu/main-menu.module').then((m) => m.MainMenuPageModule)
			},
			{
				path: 'settings',
				loadChildren: () => import('../settings/settings.module').then((m) => m.SettingsPageModule)
			},
			{
				path: 'time-tracking',
				loadChildren: () => import('../time-tracking/time-tracking.module').then((m) => m.TimeTrackingPageModule)
			},
			{
				path: 'time-tracking/:startTracking',
				loadChildren: () => import('../time-tracking/time-tracking.module').then((m) => m.TimeTrackingPageModule)
			},
			{
				path: 'timesheet-overview',
				loadChildren: () =>
					import('../timesheet-overview/timesheet-overview.module').then((m) => m.TimesheetOverviewPageModule)
			},
			{
				path: 'holiday-request',
				loadChildren: () =>
					import('../modal-pages/holiday-request/holiday-request.module').then((m) => m.HolidayRequestPageModule)
			},
			{
				path: 'booking-details',
				loadChildren: () =>
					import('../modal-pages/booking-details/booking-details.module').then((m) => m.BookingDetailsPageModule)
			},
			{
				path: 'dsgvo',
				loadChildren: () => import('../modal-pages/dsgvo/dsgvo.module').then((m) => m.DsgvoPageModule)
			},
			{
				path: 'terms-of-use',
				loadChildren: () =>
					import('../modal-pages/terms-of-use/terms-of-use.module').then((m) => m.TermsOfUsePageModule)
			},
			{
				path: 'list-modal',
				loadChildren: () => import('../modal-pages/list-modal/list-modal.module').then((m) => m.ListModalPageModule)
			},
			{
				path: 'edit-stamp',
				loadChildren: () => import('../modal-pages/edit-stamp/edit-stamp.module').then((m) => m.EditStampPageModule)
			},
			{
				path: 'date-selection',
				loadChildren: () =>
					import('../modal-pages/date-selection/date-selection.module').then((m) => m.DateSelectionPageModule)
			},
			{
				path: 'no-auths',
				loadChildren: () => import('../no-auths/no-auths.module').then((m) => m.NoAuthsPageModule)
			},
			{
				path: 'shift-detail',
				loadChildren: () =>
					import('../modal-pages/shift-detail/shift-detail.module').then((m) => m.ShiftDetailPageModule)
			},
			{
				path: 'select-language',
				loadChildren: () =>
					import('../modal-pages/select-language/select-language.module').then((m) => m.SelectLanguagePageModule)
			},
			{
				path: 'shift-planning',
				loadChildren: () => import('../shift-planning/shift-planning.module').then((m) => m.ShiftPlanningPageModule)
			},
			{
				path: 'newsfeed',
				loadChildren: () => import('../newsfeed/newsfeed.module').then((m) => m.NewsfeedPageModule)
			}
		]
	},
	{
		path: '',
		redirectTo: 'app/tabs/main-menu'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)]
})
export class TabsPageRoutingModule {}
