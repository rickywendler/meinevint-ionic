export class WorkTimeAccount {
	private _id: number;
	private _name: string;
	private _periodEnd: Date;

	private _balanceOld: number;
	private _balanceNew: number;
	private _overtime: number;

	private _ftcAvailable: boolean;
	private _ftcHours: number;

	constructor(
		id?: number,
		name?: string,
		periodEnd?: Date,
		balanceOld?: number,
		balanceNew?: number,
		overtime?: number,
		ftcAvailable?: boolean,
		ftcHours?: number
	) {
		this._id = id ?? null;
		this._name = name ?? null;
		this._periodEnd = periodEnd ?? null;
		this._balanceOld = balanceOld ?? null;
		this._balanceNew = balanceNew ?? null;
		this._overtime = overtime ?? null;
		this._ftcAvailable = ftcAvailable ?? null;
		this._ftcHours = ftcHours ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Setter id
	 * @param {number } value
	 */
	public set id(value: number) {
		this._id = value;
	}

	/**
	 * Getter name
	 * @return {string}
	 */
	public get name(): string {
		return this._name;
	}

	/**
	 * Setter name
	 * @param {string } value
	 */
	public set name(value: string) {
		this._name = value;
	}

	/**
	 * Getter name
	 * @return {Date}
	 */
	public get periodEnd(): Date {
		return this._periodEnd;
	}

	/**
	 * Setter periodEnd
	 * @param {Date } value
	 */
	public set periodEnd(value: Date) {
		this._periodEnd = value;
	}

	/**
	 * Getter balanceOld
	 * @return {number}
	 */
	public get balanceOld(): number {
		return this._balanceOld;
	}

	/**
	 * Setter balanceOld
	 * @param {number } value
	 */
	public set balanceOld(value: number) {
		this._balanceOld = value;
	}

	/**
	 * Getter balanceNew
	 * @return {number}
	 */
	public get balanceNew(): number {
		return this._balanceNew;
	}

	/**
	 * Setter balanceNew
	 * @param {number } value
	 */
	public set balanceNew(value: number) {
		this._balanceNew = value;
	}

	/**
	 * Getter overtime
	 * @return {number}
	 */
	public get overtime(): number {
		return this._overtime;
	}

	/**
	 * Setter overtime
	 * @param {number } value
	 */
	public set overtime(value: number) {
		this._overtime = value;
	}

	/**
	 * Getter ftcAvailable
	 * @return {boolean}
	 */
	public get ftcAvailable(): boolean {
		return this._ftcAvailable;
	}

	/**
	 * Setter ftcAvailable
	 * @param {boolean } value
	 */
	public set ftcAvailable(value: boolean) {
		this._ftcAvailable = value;
	}

	/**
	 * Getter ftcHours
	 * @return {number}
	 */
	public get ftcHours(): number {
		return this._ftcHours;
	}

	/**
	 * Setter ftcHours
	 * @param {number } value
	 */
	public set ftcHours(value: number) {
		this._ftcHours = value;
	}
}
