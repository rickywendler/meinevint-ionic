import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrackerRunningPage } from './tracker-running.page';

const routes: Routes = [
  {
    path: '',
    component: TrackerRunningPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrackerRunningPageRoutingModule {}
