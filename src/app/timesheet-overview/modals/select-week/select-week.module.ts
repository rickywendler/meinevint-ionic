import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectWeekPageRoutingModule } from './select-week-routing.module';

import { SelectWeekPage } from './select-week.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectWeekPageRoutingModule
  ],
  declarations: [SelectWeekPage]
})
export class SelectWeekPageModule {}
