import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShiftPlanningPageRoutingModule } from './shift-planning-routing.module';

import { ShiftPlanningPage } from './shift-planning.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShiftPlanningPageRoutingModule
  ],
  declarations: [ShiftPlanningPage]
})
export class ShiftPlanningPageModule {}
