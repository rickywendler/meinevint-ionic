import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Project } from '../../../models/Project';
import { LanguageProvider } from 'src/providers/LanguageProvider';

@Component({
	selector: 'app-list-modal',
	templateUrl: './list-modal.page.html',
	styleUrls: ['./list-modal.page.scss']
})
export class ListModalPage implements OnInit {
	//This class will be used to display list elements and return the selected value
	//IMPORTANT: _inputlist WILL expect the elements to contain a "_name" element!!
	@Input() _header: string;
	@Input() _inputList: listObject[];
	@Input() _inputPreselected: string;
	@Input() _showSaveButton: boolean = false;
	@Input() _showReturnButton: boolean = false;

	private initilized = false;

	constructor(private modalController: ModalController, public _lang: LanguageProvider) {}

	public selectionChanged(event) {
		if (this.initilized) {
			console.log(this._showSaveButton);
			this._inputPreselected = event.target.value;
			if (!this._showSaveButton) {
				this.DismissModal(true);
			}
		}
	}

	public DismissModal(selected = false) {
		this.modalController.dismiss(
			this._inputList.find((i) => i._value == this._inputPreselected),
			selected ? 'success' : undefined
		);
	}

	async ionViewDidEnter() {
		this.initilized = true;
	}

	ngOnInit() {}
}

interface listObject {
	_name: string;
	_value: any;
}
