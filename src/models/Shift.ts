export class Shift {
	private _shiftID;
	private _shiftDate;
	private _shiftName;
	private _shiftSymbol;
	private _customerLocation;
	private _customerLocationID;
	private _projectID;
	private _projectName;

	constructor(
		shiftID?: number,
		shiftDate?: Date,
		shiftName?: string,
		shiftSymbol?: string,
		customerLocation?: string,
		customerLocationID?: number,
		projectID?: number,
		projectName?: string
	) {
		this._shiftID = shiftID ?? null;
		this._shiftDate = shiftDate ?? null;
		this._shiftName = shiftName ?? null;
		this._shiftSymbol = shiftSymbol ?? null;
		this._customerLocation = customerLocation ?? null;
		this._customerLocationID = customerLocationID ?? null;
		this._projectID = projectID ?? null;
		this._projectName = projectName ?? null;
	}

	/**
	 * Getter shiftID
	 * @return {number}
	 */
	public get shiftID(): number {
		return this._shiftID;
	}

	public set shiftID(value: number) {
		this._shiftID = value;
	}

	/**
	 * Getter shiftDate
	 * @return {Date}
	 */
	public get shiftDate(): Date {
		return this._shiftDate;
	}

	/**
	 * Setter shiftDate
	 * @param {Date } value
	 */
	public set shiftDate(value: Date) {
		this._shiftDate = value;
	}

	/**
	 * Getter shiftName
	 * @return {string}
	 */
	public get shiftName(): string {
		return this._shiftName;
	}

	/**
	 * Setter shiftName
	 * @param {string } value
	 */
	public set shiftName(value: string) {
		this._shiftName = value;
	}

	/**
	 * Getter shiftSymbol
	 * @return {string}
	 */
	public get shiftSymbol(): string {
		return this._shiftSymbol;
	}

	/**
	 * Setter shiftSymbol
	 * @param {string } value
	 */
	public set shiftSymbol(value: string) {
		this._shiftSymbol = value;
	}

	/**
	 * Getter customerLocation
	 * @return {string}
	 */
	public get customerLocation(): string {
		return this._customerLocation;
	}

	/**
	 * Setter customerLocation
	 * @param {string } value
	 */
	public set customerLocation(value: string) {
		this._customerLocation = value;
	}

	/**
	 * Getter customerLocationID
	 * @return {number}
	 */
	public get customerLocationID(): number {
		return this._customerLocationID;
	}

	/**
	 * Setter customerLocationID
	 * @param {number } value
	 */
	public set customerLocationID(value: number) {
		this._customerLocationID = value;
	}

	/**
	 * Getter projectID
	 * @return {number}
	 */
	public get projectID(): number {
		return this._projectID;
	}

	/**
	 * Setter projectID
	 * @param {number } value
	 */
	public set projectID(value: number) {
		this._projectID = value;
	}

	/**
	 * Setter projectName
	 * @param {string } value
	 */
	public set projectName(value: string) {
		this._projectName = value;
	}

	/**
	 * Getter projectName
	 * @return {string}
	 */
	public get projectName(): string {
		return this._projectName;
	}

	public get shiftNameOnly(): string {
		if (this.shiftName != null) return this.shiftName.split('(')[0].trim();
		return '';
	}

	public get shiftTimeOnly(): string {
		if (this.shiftName != null) return this.shiftName.split('(')[1].replace(')', '').replace('-', ' - ');
		return '';
	}
}
