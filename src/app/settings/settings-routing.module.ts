import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsPage } from './settings.page';

const routes: Routes = [
	{
		path: '',
		component: SettingsPage
	},
	{
		path: 'security',
		loadChildren: () => import('./pages/security/security.module').then((m) => m.SecurityPageModule)
	},
	{
		path: 'tracking',
		loadChildren: () => import('./pages/tracking/tracking.module').then((m) => m.TrackingPageModule)
	},
	{
		path: 'language-change',
		loadChildren: () => import('./pages/language-change/language-change.module').then((m) => m.LanguageChangePageModule)
	},
	{
		path: 'analysis',
		loadChildren: () => import('./pages/analysis/analysis.module').then((m) => m.AnalysisPageModule)
	},
	{
		path: 'master-data',
		loadChildren: () => import('./pages/master-data/master-data.module').then((m) => m.MasterDataPageModule)
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SettingsPageRoutingModule {}
