import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoAuthsPageRoutingModule } from './no-auths-routing.module';

import { NoAuthsPage } from './no-auths.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoAuthsPageRoutingModule
  ],
  declarations: [NoAuthsPage]
})
export class NoAuthsPageModule {}
