import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HolidayAccountPage } from './holiday-account.page';

const routes: Routes = [
	{
		path: '',
		component: HolidayAccountPage,
	},
	{
		path: 'noauths',
		loadChildren: () =>
			import('../../../no-auths/no-auths.module').then((m) => m.NoAuthsPageModule),
	},  {
    path: 'holiday-detail',
    loadChildren: () => import('./modals/holiday-detail/holiday-detail.module').then( m => m.HolidayDetailPageModule)
  },

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class HolidayAccountPageRoutingModule {}
