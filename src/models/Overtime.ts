export class Overtime {
	//#region variables

	private _oldValue: number;
	private _overtime: number;
	private _projection: number;

	//#endregion

	//#region constructor

	constructor(oldValue?: number, overtime?: number, projection?: number) {
		this._oldValue = oldValue ?? null;
		this._overtime = overtime ?? null;
		this._projection = projection ?? null;
	}

	//#endregion

	//#region getter and setter

	/**
	 * Getter oldValue
	 * @return {number}
	 */
	public get oldValue(): number {
		return this._oldValue;
	}

	/**
	 * Setter oldValue
	 * @param {number } value
	 */
	public set oldValue(value: number) {
		this._oldValue = value;
	}

	/**
	 * Getter overtime
	 * @return {number}
	 */
	public get overtime(): number {
		return this._overtime;
	}

	/**
	 * Setter overtime
	 * @param {number } value
	 */
	public set overtime(value: number) {
		this._overtime = value;
	}

	/**
	 * Getter projection
	 * @return {number}
	 */
	public get projection(): number {
		return this._projection;
	}

	/**
	 * Setter projection
	 * @param {number } value
	 */
	public set projection(value: number) {
		this._projection = value;
	}

	//#endregion
}
