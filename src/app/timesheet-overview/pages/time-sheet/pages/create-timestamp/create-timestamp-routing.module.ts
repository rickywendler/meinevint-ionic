import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTimestampPage } from './create-timestamp.page';

const routes: Routes = [
  {
    path: '',
    component: CreateTimestampPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTimestampPageRoutingModule {}
