import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HolidayRequestPageRoutingModule } from './holiday-request-routing.module';

import { HolidayRequestPage } from './holiday-request.page';
import { CustomTextareaComponent } from 'src/app/components/custom-textarea/custom-textarea.component';

@NgModule({
	imports: [CommonModule, FormsModule, IonicModule, HolidayRequestPageRoutingModule],
	declarations: [HolidayRequestPage],
})
export class HolidayRequestPageModule {}
