export class AbscenceType {
	private _id: number;
	private _name: string;
	private _type: string;

	constructor(id?: number, name?: string, type?: string) {
		this._id = id ?? null;
		this._name = name ?? null;
		this._type = type ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Setter id
	 * @param {number} value
	 */
	public set id(value: number) {
		this._id = value;
	}

	/**
	 * Getter name
	 * @return {string}
	 */
	public get name(): string {
		return this._name;
	}

	/**
	 * Setter name
	 * @param {string} value
	 */
	public set name(value: string) {
		this._name = value;
	}

	/**
	 * Getter type
	 * @return {string}
	 */
	public get type(): string {
		return this._type;
	}

	/**
	 * Setter type
	 * @param {string} value
	 */
	public set type(value: string) {
		this._type = value;
	}
}
