import { DateTime } from 'luxon';
import { v4 as uuidv4 } from 'uuid';

export class TrackingStamp {
	private _id: string;

	private _started: DateTime;
	private _breakStart: DateTime;
	private _stopped: DateTime;

	private _projectId: number = 659162;
	private _activityId: number;

	private static _interval;
	public static _isTracking = false;
	public static _isPaused = false;
	private static _trackedTime = 0;
	private static _trackedPause = 0;
	private static _timeZone = 'Europe/Berlin';

	constructor(
		id?: string,
		started?: DateTime,
		stopped?: DateTime,
		projectId?: number,
		activityId?: number
	) {
		this._id = id ?? uuidv4();
		this._started = started ?? null;
		this._stopped = stopped ?? null;

		this._projectId = 659162;
		this._activityId = activityId ?? null;
	}

	/**
	 * Getter started
	 *
	 * @return {DateTime}
	 */
	public get started(): DateTime {
		return this._started;
	}

	/**
	 * Setter started
	 * @param {DateTime } value
	 */
	public set started(value: DateTime) {
		this._started = value;
	}

	/**
	 * Getter breakStarted
	 * @return {DateTime}
	 */
	public get breakStarted(): DateTime {
		return this._breakStart;
	}

	/**
	 * Setter breakStarted
	 * @param {DateTime } value
	 */
	public set breakStarted(value: DateTime) {
		this._breakStart = value;
	}

	/**
	 * Getter stopped
	 * @return {DateTime}
	 */
	public get stopped(): DateTime {
		return this._stopped;
	}

	/**
	 * Setter stopped
	 * @param {DateTime } value
	 */
	public set stopped(value: DateTime) {
		this._stopped = value;
	}

	/**
	 * Getter projectId
	 * @return {number}
	 */
	public get projectId(): number {
		return this._projectId;
	}

	/**
	 * Setter projectId
	 * @param {number } value
	 */
	public set projectId(value: number) {
		this._projectId = 659162;
	}

	/**
	 * Getter activityId
	 * @return {number}
	 */
	public get activityId(): number {
		return this._activityId;
	}

	/**
	 * Setter keyId
	 * @param {string } value
	 */
	public set activityId(value: number) {
		this._activityId = value;
	}

	/**
	 * Getter isTracking
	 * @return {boolean}
	 */
	public get isTracking(): boolean {
		return TrackingStamp._isTracking;
	}

	/**
	 * Getter isPaused
	 * @return {boolean}
	 */
	public get isPaused(): boolean {
		return TrackingStamp._isPaused;
	}

	/**
	 * Getter trackedTime
	 * @return {number}
	 */
	public get trackedTime(): number {
		return TrackingStamp._trackedTime;
	}

	/**
	 * Getter trackedPause
	 * @return {number}
	 */
	public get trackedPause(): number {
		return TrackingStamp._trackedPause;
	}

	/**
	 * Getter id
	 * @return {string}
	 */
	public get id(): string {
		return this._id;
	}

	/**
	 * Getter staticTrackedTime
	 * @return {number}
	 */
	public static get staticTrackedTime(): number {
		return TrackingStamp._trackedTime;
	}

	/**
	 * Getter staticTrackedPause
	 * @return {number}
	 */
	public static get staticTrackedPause(): number {
		return TrackingStamp._trackedPause;
	}

	public static set timeZone(value: string) {
		TrackingStamp._timeZone = value;
	}

	/**
	 * Method to get the tracked time, if stopped has been set
	 * @returns number, if stopped is set, or null if stopped is equals null
	 */
	public getTrackedTime(): number | null {
		if (this._stopped == null) {
			return null;
		}

		return this.stopped.getTime() - this._started.getTime();
	}
}
