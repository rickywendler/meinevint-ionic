import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { LanguageProvider } from 'src/providers/LanguageProvider';
import { HolidayRequest } from 'src/models/HolidayRequest';
import { IonDatetime, ModalController } from '@ionic/angular';
import { Period } from '../../../models/Period';
import { AbscenceType } from '../../../models/AbscenceType';
import { APIProvider } from '../../../providers/APIProvider';
import { HolidayAccount } from '../../../models/HolidayAccount';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { WeekDay } from '@angular/common';
import { DateSelectionPage } from '../date-selection/date-selection.page';

@Component({
	selector: 'app-holiday-request',
	templateUrl: './holiday-request.page.html',
	styleUrls: ['./holiday-request.page.scss'],
})
export class HolidayRequestPage implements OnInit {
	@Input() _holidayAccount: HolidayAccount;
	@Input() _holidayRequests: HolidayRequest[];
	private _abscenceTypes: AbscenceType[];

	public _today = new Date().toISOString();
	public _nextYear = DateTime.now().plus({ years: 1 }).endOf('year').toISO();

	public _plannedDays: number = 0;
	public _singleDayIsWeekEnd: boolean = false;

	public _selectedMethod: number = null;
	public _selectedValue: DateTime = DateTime.now().setZone(this._settings.timeZone);
	public _oneDayComplete: boolean = true;

	public _selectedStart: DateTime = null;
	public _selectedEnd: DateTime = null;
	public _firstDayComplete: boolean = true;
	public _lastDayComplete: boolean = true;

	public _addNote: boolean = false;
	public _description: string = '';

	@ViewChild('StartPopover') popoverStart;
	@ViewChild('EndPopover') popoverEnd;

	@ViewChild('StartDateTime') startDatePicker;
	@ViewChild('EndDateTime') endDatePicker;

	constructor(
		private _api: APIProvider,
		public _lang: LanguageProvider,
		private _modalCon: ModalController,
		private _settings: SettingsProvider
	) {}

	async ngOnInit() {
		this._abscenceTypes = await this._api.getHolidayTypes();
	}

	closeModal() {
		this._modalCon.dismiss({ success: false });
	}

	public async saveRequest() {
		let note = null;
		if (this._addNote) {
			note = this._description;
		}

		const wholeDayType = this._abscenceTypes.find((a) => a.type == 'whole_day');
		const halfDayType = this._abscenceTypes.find((a) => a.type == 'half_day');

		if (this._selectedMethod == 0) {
			// if singleDay
			if (this.startAndEndAreSameDate) {
				this._api.saveHolidayReq(
					this._selectedStart,
					this._selectedEnd,
					this._lastDayComplete ? wholeDayType : halfDayType,
					note
				);
			} else {
				let start = this._selectedStart;
				let end = this._selectedEnd;

				if (!this._firstDayComplete && this.firstDayIsValid) {
					this._api.saveHolidayReq(this._selectedStart, this._selectedStart, halfDayType, note);
					start = start.plus({ days: 1 });
				}
				if (!this._lastDayComplete && this.lastDayIsValid) {
					this._api.saveHolidayReq(this._selectedEnd, this._selectedEnd, halfDayType, note);
					end = end.minus({ days: 1 });
				}
				this._api.saveHolidayReq(start, end, wholeDayType, note);
			}
		} else {
			if (this._oneDayComplete) {
				this._api.saveHolidayReq(this._selectedValue, this._selectedValue, wholeDayType, note);
			} else {
				this._api.saveHolidayReq(this._selectedValue, this._selectedValue, halfDayType, note);
			}
		}

		this._holidayAccount.requestedDays += this._plannedDays;
		this._holidayAccount.availableDays -= this._plannedDays;

		this._modalCon.dismiss({ success: true });
	}

	public async startChanges() {
		const modal = await this._modalCon.create({
			component: DateSelectionPage,
			componentProps: {
				_setStart: true,
				_selectedStart: this._selectedStart,
				_selectedEnd: this._selectedEnd,
			},
			breakpoints: [0, 0.75],
			initialBreakpoint: 0.75,
		});

		modal.present();

		const modalData = await modal.onWillDismiss();
		if (modalData.data != null) {
			if (modalData.data.success) {
				this._selectedStart = modalData.data._selectedStart;
			} else {
				this._selectedStart = null;
			}
		}

		if (this._selectedEnd != null) {
			// calculate planned days
			this.calculatePlannedDays();
		}
	}

	public get startAndEndAreSameDate(): boolean {
		if (this._selectedEnd == null || this._selectedStart == null) return false;

		return (
			this._selectedEnd.day == this._selectedStart.day &&
			this._selectedEnd.month == this._selectedStart.month &&
			this._selectedEnd.year == this._selectedStart.year
		);
	}

	public async endChanges() {
		const modal = await this._modalCon.create({
			component: DateSelectionPage,
			componentProps: {
				_setStart: false,
				_selectedStart: this._selectedStart,
				_selectedEnd: this._selectedEnd,
			},
			breakpoints: [0, 0.75],
			initialBreakpoint: 0.75,
		});

		modal.present();

		const modalData = await modal.onWillDismiss();
		if (modalData.data != null) {
			if (modalData.data.success) {
				this._selectedEnd = modalData.data._selectedEnd;
			} else {
				this._selectedEnd = null;
			}
		}
		// calculate planned days
		this.calculatePlannedDays();
	}

	private calculatePlannedDays() {
		this._plannedDays = 0;

		if (this.startAndEndAreSameDate) {
			this._lastDayComplete = this._firstDayComplete;
		}

		for (
			let day = this._selectedStart;
			this._selectedEnd.diff(day, 'days').days >= 0;
			day = day.plus({ days: 1 })
		) {
			if (day.weekday < 6) {
				this._plannedDays += 1;
			}
		}

		if (!this._firstDayComplete && this.firstDayIsValid && !this.startAndEndAreSameDate) {
			this._plannedDays -= 0.5;
		}

		if (!this._lastDayComplete && this.lastDayIsValid) {
			this._plannedDays -= 0.5;
		}
	}

	public async selectMethod(methodType: number) {
		this._selectedMethod = methodType;
		if (methodType == 0) {
			this._plannedDays = 0;
			if (this._selectedEnd != null && this._selectedStart != null) {
				this.calculatePlannedDays();
			}
		} else if (methodType == 1) {
			this._selectedValue = DateTime.now().setZone(this._settings.timeZone);
			if (this._selectedValue.weekday > 5) {
				this._singleDayIsWeekEnd = true;
				this._plannedDays = 0;
			} else {
				this._singleDayIsWeekEnd = false;
				if (this._oneDayComplete) {
					this._plannedDays = 1;
				} else {
					this._plannedDays = 0.5;
				}
			}
		} else if (methodType == 2) {
			this._selectedValue = DateTime.now().setZone(this._settings.timeZone).plus({ days: 1 });
			if (this._selectedValue.weekday > 5) {
				this._singleDayIsWeekEnd = true;
				this._plannedDays = 0;
			} else {
				this._singleDayIsWeekEnd = false;
				if (this._oneDayComplete) {
					this._plannedDays = 1;
				} else {
					this._plannedDays = 0.5;
				}
			}
		}
	}

	public get disabledSaveButton(): boolean {
		if (this._selectedMethod == null) {
			return true;
		}
		if (this._selectedMethod == 0) {
			return (
				this._selectedEnd == null ||
				this._selectedStart == null ||
				this._plannedDays > this._holidayAccount.availableDays ||
				this._plannedDays == 0
			);
		} else if (this._selectedMethod == 1 || this._selectedMethod == 2) {
			return this._selectedValue == null || this._singleDayIsWeekEnd;
		}
	}

	public async singleDayAsWholeDay(isWhole: boolean) {
		this._oneDayComplete = isWhole;
		if (isWhole) {
			this._plannedDays = 1;
		} else {
			this._plannedDays = 0.5;
		}
	}

	public async dateRangeStartAsWholeDay(isWhole: boolean) {
		if (this._plannedDays > 0) {
			if (this.firstDayIsValid) {
				if (this.startAndEndAreSameDate) {
					if (!isWhole) {
						this._plannedDays = 0.5;
					} else {
						this._plannedDays = 1;
					}
				} else {
					if (isWhole && !this._firstDayComplete) {
						this._plannedDays += 0.5;
					} else if (this._firstDayComplete && !isWhole) {
						this._plannedDays -= 0.5;
					}
				}
			}
		}
		this._firstDayComplete = isWhole;
	}

	public async dateRangeEndAsWholeDay(isWhole: boolean) {
		if (this._plannedDays > 0) {
			if (this.lastDayIsValid) {
				if (this.startAndEndAreSameDate) {
					if (!isWhole) {
						this._plannedDays = 0.5;
					} else {
						this._plannedDays = 1;
					}
				} else {
					if (isWhole && !this._lastDayComplete) {
						this._plannedDays += 0.5;
					} else if (this._lastDayComplete && !isWhole) {
						this._plannedDays -= 0.5;
					}
				}
			}
		}
		this._lastDayComplete = isWhole;
	}

	public get lastDayIsValid(): boolean {
		return this._selectedEnd != null ? this._selectedEnd.weekday < 6 : false;
	}

	public get firstDayIsValid(): boolean {
		return this._selectedStart != null ? this._selectedStart.weekday < 6 : false;
	}

	public clearDateInput(isStart) {
		if (isStart) {
			this._selectedStart = null;
		} else {
			this._selectedEnd = null;
		}
		this._plannedDays = 0;
	}
}
