import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { APIProvider } from '../../providers/APIProvider';
import { News } from 'src/models/News';
import { NewsDetailPage } from './modals/news-detail/news-detail.page';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-newsfeed',
	templateUrl: './newsfeed.page.html',
	styleUrls: ['./newsfeed.page.scss'],
})
export class NewsfeedPage implements OnInit {
	private _showOnlyMarked = false;
	private allNews: News[] = [];

	public displayedNews: News[] = [];

	constructor(
		public _lang: LanguageProvider,
		private api: APIProvider,
		private modalCon: ModalController
	) {}

	ngOnInit() {}

	async ionViewWillEnter() {
		this.allNews = await this.api.getMessages();
		if (this._showOnlyMarked) {
			this.displayedNews = this.allNews.filter((n) => n.isMarked);
		} else {
			this.displayedNews = this.allNews;
		}
	}

	async showDetail(news: News) {
		const modal = await this.modalCon.create({
			component: NewsDetailPage,
			componentProps: {
				news: news,
			},
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1,
		});

		await modal.present();
		news.isRead = true;
		this.api.readMessage(news.id);
	}

	public set showOnlyMarked(value: boolean) {
		this._showOnlyMarked = value;
		if (value) {
			this.displayedNews = this.allNews.filter((n) => n.isMarked);
		} else {
			this.displayedNews = this.allNews;
		}
	}

	public get showOnlyMarked(): boolean {
		return this._showOnlyMarked;
	}
}
