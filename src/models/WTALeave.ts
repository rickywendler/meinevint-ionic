import { DateTime } from 'luxon';
export class WTALeave {
	private _date: DateTime = null;
	private _hours: number = null;
	private _describtion: string = null;
	private _keyId: string = null;

	constructor(date?: DateTime, hours?: number, describtion?: string, keyId?: string) {
		this._date = date ?? null;
		this._hours = hours ?? null;
		this._describtion = describtion ?? null;
		this._keyId = keyId ?? null;
	}

	/**
	 * Getter date
	 * @return {DateTime }
	 */
	public get date(): DateTime {
		return this._date;
	}

	/**
	 * Getter hours
	 * @return {number }
	 */
	public get hours(): number {
		return this._hours;
	}

	/**
	 * Getter describtion
	 * @return {string }
	 */
	public get describtion(): string {
		return this._describtion;
	}

	/**
	 * Getter keyId
	 * @return {string }
	 */
	public get keyId(): string {
		return this._keyId;
	}

	/**
	 * Setter date
	 * @param {DateTime } value
	 */
	public set date(value: DateTime) {
		this._date = value;
	}

	/**
	 * Setter hours
	 * @param {number } value
	 */
	public set hours(value: number) {
		this._hours = value;
	}

	/**
	 * Setter describtion
	 * @param {string } value
	 */
	public set describtion(value: string) {
		this._describtion = value;
	}

	/**
	 * Setter keyId
	 * @param {string } value
	 */
	public set keyId(value: string) {
		this._keyId = value;
	}
}
