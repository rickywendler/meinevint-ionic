import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from 'src/providers/LanguageProvider';
import { APIProvider } from '../../../../providers/APIProvider';
import { EmployeeDataProvider } from '../../../../providers/EmployeeDataProvider';
import * as utf8 from 'utf8';
import { IonSelect } from '@ionic/angular';

@Component({
	selector: 'app-master-data',
	templateUrl: './master-data.page.html',
	styleUrls: ['./master-data.page.scss']
})
export class MasterDataPage implements OnInit {
	public houseNumber = '';
	public street = '';

	constructor(public lang: LanguageProvider, private api: APIProvider, public employee: EmployeeDataProvider) {}

	async ngOnInit() {
		await this.api.getEmployeeData();
		this.api.getEmployeeInfo();

		const adressSplitted = this.employee.adressOne.split(' ');
		this.houseNumber = adressSplitted[adressSplitted.length - 1];
		this.street = this.employee.adressOne.replace(this.houseNumber, '');
	}

	ionViewWillEnter() {}

	save() {
		this.employee.adressOne = (this.street.trim() + ' ' + this.houseNumber).trim();

		this.employee.mobilCountry = (document.getElementById('mobileSelect') as any as IonSelect).value;
		this.employee.landlineCountry = (document.getElementById('phoneSelect') as any as IonSelect).value;
		this.employee.country = (document.getElementById('countrySelect') as any as IonSelect).value;

		this.api.editEmployeeData();
	}
}
