import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { LanguageProvider } from '../providers/LanguageProvider';
import { DataProvider } from '../providers/DataProvider';
import { SettingsProvider } from '../providers/SettingsProvider';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
//import { Firebase } from '@awesome-cordova-plugins/firebase/ngx';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss'],
})
export class AppComponent {
	constructor(
		private _platform: Platform,
		private _lang: LanguageProvider,
		private _data: DataProvider,
		private _settings: SettingsProvider
	) {
		this.initializeApp();
	}

	async initializeApp() {
		setTimeout(async () => {
			await this._platform.ready();
			await this._data.init().then(() => {
				this._settings.init().then(() => {
					this._lang.setLanguage(this._settings.currentLanguage);
				});
			});
		}, 50);
		this._platform.ready().then(async () => {
			/*
			await this._settings.init();
			console.log('Grabbed language');
			console.log('settingsLanguage: ' + this._settings.currentLanguage);
			console.log('LANG ' + this._lang.getLanguage());
			console.log('Setting Language');
			this._lang.setLanguage(this._settings.currentLanguage);
			console.log('LANG ' + this._lang.getLanguage());


			this.firebase.getToken()
				.then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
				.catch(error => console.error('Error getting token', error));

			this.firebase.onNotificationOpen()
				.subscribe(data => console.log(`User opened a notification ${data}`));

			this.firebase.onTokenRefresh()
				.subscribe((token: string) => console.log(`Got a new token ${token}`));*/
		});
	}
}
