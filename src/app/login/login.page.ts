import { Component, OnInit } from '@angular/core';
import { ScreenOrientation } from '@awesome-cordova-plugins/screen-orientation/ngx';
import { DataProvider } from '../../providers/DataProvider';
import { APIProvider } from '../../providers/APIProvider';
import { NavController, ModalController, ToastController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { AlertController, Platform } from '@ionic/angular';
import * as QRious from 'qrious';
import { TouchID } from '@awesome-cordova-plugins/touch-id/ngx';
import { BiometricWrapper } from '@awesome-cordova-plugins/biometric-wrapper/ngx';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { SettingsProvider } from '../../providers/SettingsProvider';
import { DsgvoPage } from '../modal-pages/dsgvo/dsgvo.page';
import { TermsOfUsePage } from '../modal-pages/terms-of-use/terms-of-use.page';
import { Device } from '@capacitor/device';
import { SelectLanguagePage } from '../modal-pages/select-language/select-language.page';
import { Keyboard, KeyboardResizeOptions, KeyboardResize } from '@capacitor/keyboard';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
	username = '';
	password = '';
	loginText = '';

	loginState = 0;
	pinEntry = '';
	pinCounter = 0;
	firstPinSetup = ''; //this variable will be used as confirmation for the pin registration
	devicePW = '';
	seenDSGVO: boolean = false;
	textState = 0;
	_firstTry: boolean = true;

	selectedSalutation = 0;
	surName = '';
	name = '';
	email = '';
	RegPassword = '';
	passwordRepeat = '';

	ConfCode = '';

	private tries = 0;

	public pinButtonsDisabled = false;
	public _pinSuccessfull: boolean = false;
	public _pinNotRight: boolean = false;

	public _qrCodeDisplay = true;
	public _qrCodeVal = '000000';

	private _fingerprintOptions: FingerprintOptions = {
		title: 'Login',
		description: 'Zum einloggen bitte mit dem Finger den Fingerabdrucksensor berühren',
		cancelButtonTitle: 'Abbrechen'
	};

	private _keyboardEventHelper = false;

	/**
	 * Property which determines if the keyboard is shown
	 */
	public set isKeyboardShown(value) {
		if (value) {
			document.getElementById('dsgvoButton')?.classList.add('hidden');
			document.getElementById('dsgvoButtonWithKeyboard')?.classList.remove('hidden');
			document.getElementById('languageButton')?.classList.add('hidden');
		} else {
			document.getElementById('dsgvoButton')?.classList.remove('hidden');
			document.getElementById('dsgvoButtonWithKeyboard')?.classList.add('hidden');
			document.getElementById('languageButton')?.classList.remove('hidden');
		}
	}

	/**
	 * Constructor
	 * @param screenOrientation
	 * @param _dataProvider
	 * @param api
	 * @param nav
	 * @param _lang
	 * @param alert
	 * @param platform
	 * @param iosID
	 * @param androidID
	 * @param fingerprint
	 * @param _settings
	 * @param modalCtrl
	 */
	constructor(
		private screenOrientation: ScreenOrientation,
		public _dataProvider: DataProvider,
		private api: APIProvider,
		private nav: NavController,
		public _lang: LanguageProvider,
		private alert: AlertController,
		private platform: Platform,
		private iosID: TouchID,
		private androidID: BiometricWrapper,
		private fingerprint: FingerprintAIO,
		private _settings: SettingsProvider,
		private modalCtrl: ModalController,
		private toast: ToastController
	) {}

	/**
	 * standard ionic method and is used for activating the kayboard event on this page
	 */
	ionViewWillEnter() {
		this._keyboardEventHelper = true;
		let options: KeyboardResizeOptions = {
			mode: KeyboardResize.Body
		};
		Keyboard.setResizeMode(options);
	}

	/**
	 * standard ionic method and is used for preventing other pages to toggel the kayboard event
	 */
	ionViewDidLeave() {
		this._keyboardEventHelper = false;
	}

	/**
	 * Initilize the login page
	 */
	async ngOnInit() {
		this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

		const languageSet = (await this._dataProvider.getStorage('languageHasBeenSetAtStart')) as boolean;

		if (languageSet == null) {
			const langTag = (await Device.getLanguageTag()).value.split('-')[0];
			switch (langTag) {
				case 'es':
					this._settings.currentLanguage = 'spanish';
					break;
				case 'pl':
					this._settings.currentLanguage = 'polish';
					break;
				case 'fr':
					this._settings.currentLanguage = 'french';
					break;
				case 'it':
					this._settings.currentLanguage = 'italian';
					break;
				case 'de':
					this._settings.currentLanguage = 'german';
					break;
				case 'en':
				default:
					this._settings.currentLanguage = 'english';
					break;
			}
			this._dataProvider.setStorage('languageHasBeenSetAtStart', true);
		}
		window.addEventListener('keyboardWillShow', () => {
			if (this._keyboardEventHelper) {
				this.isKeyboardShown = true;
			}
		});
		window.addEventListener('keyboardWillHide', () => {
			if (this._keyboardEventHelper) {
				this.isKeyboardShown = false;
			}
		});
		//alert("I am a test");

		setTimeout(async () => {
			this.seenDSGVO = (await this._dataProvider.getStorage('seenDSGVO')) ?? false;

			const online = await this.api.tokenExpired();
			if (online == 'success') {
				this.loginState = 1;
				this.textState = 1;

				//grab employeeinfo for later use
				this.api.getEmployeeInfo();
				this.api.getAuths();
				this.api.getEmployeeData();

				try {
					const localPw = await this._dataProvider.getStorage('localpw');
					if (localPw != null) {
						//alert("successfully grabbed a local pw");
						this.devicePW = localPw;
						this.loginState = 2;
						this.textState = 2;
					} else {
					}
				} catch (e) {}
			} else if (online == 'OFFLINE') {
				const localPw = await this._dataProvider.getStorage('localpw');
				if (localPw != null) {
					//grab employeeinfo for later use
					this.api.getEmployeeInfo();
					this.api.getEmployeeData();
					this.api.getAuths();

					this.devicePW = localPw;
					this.loginState = 2;
					this.textState = 2;
				}
			}
		}, 100);
	}

	/**
	 * function to login
	 */
	async loginFunction() {
		if (!this.seenDSGVO) {
			this.seenDSGVO = (await this.showDSGVO(true)) as boolean;
		}
		if (this.seenDSGVO) {
			const username = this.username.toLowerCase();
			let response = await this.api.loginFunction(username, this.password);
			try {
				response = JSON.parse(response).webservice_token;
				this._dataProvider.setToken(response);
				this.loginState = 1;
				this.textState = 1;
			} catch (e) {
				const alert = await this.alert.create({
					message: this._lang._dict.login.LoginWrong,
					buttons: ['OK']
				});

				await alert.present();
				//alert(this._lang._dict.login.LoginWrong);
				//The login failed, so we get a bad response from the server
				//alert("False Login");
			}
		}
	}

	signUp() {
		this.loginState = 5;
	}

	createUser() {
		const response$ = this.api.registration(
			this.selectedSalutation,
			this.surName,
			this.name,
			this.email,
			this.RegPassword,
			this.passwordRepeat
		);

		/*console.log(this.selectedSalutation);
		console.log(this.surName);
		console.log(this.name);
		console.log(this.email);
		console.log(this.RegPassword);
		console.log(this.passwordRepeat);*/

		response$.subscribe(
			(data) => {
				//console.log(data);
				this.loginState = 6;
			},
			(error) => {
				console.error(error.error);

				switch (JSON.parse(error.error).error) {
					case 'passwords are not the same':
						alert(this._lang._dict.Freemium.PasswordMismatch);
						break;

					case 'no password':
						alert(this._lang._dict.Freemium.noPassword);
						break;

					case 'password does not match prerequities':
						alert(this._lang._dict.Freemium.PasswordMalformed);
						break;

					case 'error while user creation':
						alert(this._lang._dict.Freemium.userCreationError);
						break;

					case 'user already exists':
						alert(this._lang._dict.Freemium.MailExists);
						break;
				}
			}
		);

		//console.log(this.api.registration());
	}

	clearFields() {
		this.selectedSalutation = 0;
		this.surName = '';
		this.name = '';
		this.email = '';
		this.RegPassword = '';
		this.passwordRepeat = '';

		this.ConfCode = '';
	}

	activateAccount() {
		const response$ = this.api.confirmAccount(this.ConfCode, this.email);

		response$.subscribe(
			(data) => {
				console.log(data);
				this.loginState = 0;
				alert(this._lang._dict.Freemium.activationSuccessful);
			},
			(error) => {
				console.error(error.error);
				alert(this._lang._dict.Freemium.wrongCode);
			}
		);
	}

	/**
	 * adding a number to the pin
	 * @param val the number which has to be added to the pin
	 */
	addPinEntry(val) {
		//alert(val);
		this.pinEntry += val + '';
		this.pinCounter++;
		if (this.pinCounter < 4) {
			this._pinNotRight = false;
			this._pinSuccessfull = false;
		}

		if (this.pinCounter == 4) {
			this.checkPin();
		}
	}

	/**
	 * method to check the inputed pin with the stored pin and proceed when the pin is correct.
	 */
	async checkPin() {
		//alert("checkpin was called");
		if (this.loginState == 1) {
			this.textState = 1;

			//Copy pasted from legacy code and adjusted.

			if (this.firstPinSetup == '') {
				this.firstPinSetup = this.pinEntry;
				this.textState = 11;
				this.pinEntry = '';
				//alert("FIRST PIN was set");
				this.pinCounter = 0;
				this._pinSuccessfull = true;
			} else {
				if (this.firstPinSetup == this.pinEntry) {
					//The pins are the same
					this._pinSuccessfull = true;
					this._dataProvider.setStorage('localpw', this.pinEntry);

					//this.GI.localpw = this.pinInput; !!NEEDS ADJUSTMENT
					this.loginState = 2;

					this.api.getTimeZone().then((result) => {
						this._settings.timeZone = result;
					});

					await this.api.getAuths();
					await this.api.getEmployeeInfo();
					await this.api.getEmployeeData();
					this.api.getProjects();
					this.api.getActivities();
					this.nav.navigateRoot('/app/tabs/main-menu');
				} else {
					//Die Pins stimmen nicht überein
					this.textState = 3;
					this._pinNotRight = true;
					this.firstPinSetup = '';
					this.pinEntry = '';
					this.pinCounter = 0;
				}
			}
		}
		if (this.loginState == 2) {
			//local account exists and token is valid
			if (this._firstTry) {
				this._firstTry = false;

				this.textState = 2;
			}
			if (this.pinEntry == this.devicePW) {
				this._pinSuccessfull = true;
				this.api.getTimeZone().then((result) => {
					this._settings.timeZone = result;
				});

				await this.api.getAuths();
				await this.api.getEmployeeInfo();
				await this.api.getEmployeeData();
				this.api.getProjects();
				this.api.getActivities();
				this.nav.navigateRoot('/app/tabs/main-menu');
			} else {
				//False pin Entered
				this.textState = 4;
				this._pinNotRight = true;
				this.pinEntry = '';
				this.pinCounter = 0;
				this.tries++;

				if (this.tries >= 3) {
					if (this.tries < 6) {
						this.pinButtonsDisabled = true;
						const timeToWait = (this.tries - 2) * 10;
						const toast = await this.toast.create({
							message: (this._lang._dict.login.PinDisabled as string).replace('XX', timeToWait + ''),
							position: 'bottom',
							cssClass: 'fail-toast',
							duration: 2000
						});

						await toast.present();
						setTimeout(() => {
							this.pinButtonsDisabled = false;
						}, 10000 * (this.tries - 2));
					} else {
						this._settings.resetSettings();
						this._dataProvider.clearStorage();
						this.resetAllStates();
						const toast = await this.toast.create({
							message: this._lang._dict.login.ForceLogout as string,
							position: 'bottom',
							cssClass: 'fail-toast',
							duration: 2000
						});

						await toast.present();
					}
				}
			}
		}
	}

	/**
	 * Get the token of the user
	 */
	async getToken() {
		await this._dataProvider.getToken();
	}

	/**
	 * inserts the password into the textinput from the event
	 * @param event event
	 */
	pwInputAuto(event) {
		this.password = event.target.value;
	}

	/**
	 * deletes one digit of the already inputed pin
	 */
	deletePinEntry() {
		this.pinEntry = this.pinEntry.slice(0, -1);
		this.pinCounter -= 1;
		if (this.pinCounter < 0) {
			this.pinCounter = 0;
		}
	}

	async testbutton() {
		//this is really just a function to test certain functionalities.
	}

	public clickTimerTimeout;
	public clickTimerIsActive = false;

	public startTimer() {
		if (this.clickTimerTimeout == null) {
			this.clickTimerIsActive = true;
			this.clickTimerTimeout = setTimeout(() => {
				if (this.clickTimerIsActive) {
					this.test();
				}
			}, 3000);
		}
	}
	public stopTimer() {
		clearTimeout(this.clickTimerTimeout);
		this.clickTimerIsActive = false;
		this.clickTimerTimeout = null;
	}

	private async test() {
		console.log("I'm a test");
		const alert = await this.alert.create({
			header: 'Test',
			inputs: [
				{
					name: 'token',
					type: 'text',
					placeholder: 'Test'
				},
				{
					name: 'pin',
					label: 'Pin',
					type: 'text',
					placeholder: '1111',
					attributes: {
						maxlength: 4,
						minlength: 4,
						inputmode: 'numeric'
					}
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: () => {
						console.log('Confirm Cancel');
					}
				},
				{
					text: 'Ok',
					handler: async (data) => {
						console.log('Confirm Ok');
						console.log(data);
						let allSuccessfull = true;

						let token: string = data.token.trim();
						let pin: string = data.pin.trim();

						if (pin.length == 4) {
							if (pin.includes(' ') || pin.includes(',') || pin.includes('.') || pin.includes('-')) {
								allSuccessfull = false;
							}
						} else {
							if (pin.length == 0) {
								pin = '1111';
							} else {
								allSuccessfull = false;
							}
						}

						if (
							token.length == 40 &&
							token.includes('-') &&
							token.includes('.') &&
							token.split('-').length == 3 &&
							token.split('.').length == 2
						) {
							const success = await this.api.tokenExpired(token);
							if (success != 'success') {
								allSuccessfull = false;
							}
						} else {
							allSuccessfull = false;
						}

						if (allSuccessfull) {
							this._dataProvider.clearStorage().then(async () => {
								this.resetAllStates();
								this._dataProvider.setToken(token);
								await this._dataProvider.getToken();
								this._dataProvider.setStorage('localpw', pin);

								this._dataProvider.setStorage('seenDSGVO', true);
								this._dataProvider.setStorage('seenTermsOfUse', true);

								this.api.getTimeZone().then((zoneResult) => {
									this._settings.timeZone = zoneResult;
								});
								this.api.getEmployeeInfo().then((result) => {
									this.api.getAuths().then((result) => {
										this.api.getEmployeeData().then((result) => {
											this.api.getProjects();
											this.api.getActivities();
											this.nav.navigateRoot('/app/tabs/main-menu');
										});
									});
								});
							});
						}
					}
				}
			]
		});
		await alert.present();
	}

	/**
	 * deleting local settings
	 */
	async deleteLocalSettings() {
		const delAlert = await this.alert.create({
			header: 'Workforce',
			message: this._lang._dict.login.PinForgottenMessage,
			buttons: [
				{
					text: this._lang._dict.Continue,
					handler: async () => {
						await this._dataProvider.clearStorage().then(async () => {
							this.resetAllStates();
						});
					}
				},
				{
					text: this._lang._dict.Cancel,
					handler: () => {
						delAlert.dismiss();
					}
				}
			]
		});

		await delAlert.present();
	}

	/**
	 * activate the fingerprint
	 */
	private checkFaceID() {
		this.fingerprint
			.show(this._fingerprintOptions)
			.then(async (result) => {
				this.api.getTimeZone().then((zoneResult) => {
					this._settings.timeZone = zoneResult;
				});
				await this.api.getEmployeeInfo();
				await this.api.getAuths();
				await this.api.getEmployeeData();
				this.api.getProjects();
				this.api.getActivities();

				this.nav.navigateRoot('/app/tabs/main-menu');
			})
			.catch((error) => console.error('error'));
	}

	/**
	 * resetting all variables
	 */
	private resetAllStates() {
		this.username = '';
		this.password = '';
		this.loginText = '';

		this.loginState = 0;
		this.pinEntry = '';
		this.pinCounter = 0;
		this.firstPinSetup = ''; //this variable will be used as confirmation for the pin registration
		this.devicePW = '';
		this.textState = 0;
		this.tries = 0;

		this._pinNotRight = false;
		this._pinSuccessfull = false;
	}

	/**
	 * ionic default method which is used for the workflow which first start checking for QR and then fingerprint
	 */
	async ionViewDidEnter() {
		//Check whether QRCode is enabled
		await this.checkQRCode();

		//Check whether fingerprint is enabled
		if (!this._qrCodeDisplay) {
			setTimeout(() => {
				this.fingerprint
					.isAvailable()
					.then(async (data) => {
						if (this._settings.useFingerprint) {
							this.checkFaceID();
						}
					})
					.catch((data) => {
						console.error(data);
					});
			}, 50);
		}
	}

	// a method which displayes the language selection as modal
	async selectLanguage() {
		const modal = await this.modalCtrl.create({
			component: SelectLanguagePage,
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});
		modal.present();
	}

	/**
	 * MEthod to handle the next step after the QR Code has been cancled and then checks for fingerprint
	 */
	qrCodeCancelled() {
		this._qrCodeDisplay = false;
		this.fingerprint
			.isAvailable()
			.then(async (data) => {
				if (this._settings.useFingerprint) {
					this.checkFaceID();
				}
			})
			.catch((data) => {
				console.error(data);
			});
	}

	/**
	 * Opens the BRowser and navigates to the forgotten password side
	 */
	async forgotPW() {
		await Browser.open({
			url: 'https://workforce.compleet.app/main/module/index.php?evint-login=true&reset=true'
		});
	}

	async checkQRCode() {
		this._qrCodeDisplay = (await this._dataProvider.getStorage('QRCodeSetting')) ?? false;
		//alert(this._QRCodeDisplay);
		this._qrCodeVal = (await this._dataProvider.getStorage('QRCode')) ?? '000000';

		let qrcode = new QRious({
			element: document.getElementById('qrcode'),
			background: '#ffffff',
			backgroundAlpha: 1,
			foreground: '#000000',
			foregroundAlpha: 1,
			level: 'H',
			padding: 0,
			size: 128,
			value: this._qrCodeVal + ''
		});
	}

	/**
	 * shows the dsgvo modal
	 * @param userNeedsToAccept boolean to togle the accept button in the dsgvo display
	 * @returns boolean, which indicates if the user has accepted the dsgvo
	 */
	async showDSGVO(userNeedsToAccept = false): Promise<boolean | void> {
		const modal = await this.modalCtrl.create({
			component: DsgvoPage,
			componentProps: {
				_userNeedToAccept: userNeedsToAccept
			},
			handle: true,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();

		if (userNeedsToAccept) {
			return (await modal.onWillDismiss()).data.accepted;
		}
	}

	/**
	 * shows the terms of use modal
	 */
	async showTermsOfUse() {
		const modal = await this.modalCtrl.create({
			component: TermsOfUsePage,
			handle: true,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();
	}
}
