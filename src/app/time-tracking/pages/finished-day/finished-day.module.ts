import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinishedDayPageRoutingModule } from './finished-day-routing.module';

import { FinishedDayPage } from './finished-day.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinishedDayPageRoutingModule
  ],
  declarations: [FinishedDayPage]
})
export class FinishedDayPageModule {}
