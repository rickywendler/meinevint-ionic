import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PinChangePageRoutingModule } from './pin-change-routing.module';

import { PinChangePage } from './pin-change.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PinChangePageRoutingModule
  ],
  declarations: [PinChangePage]
})
export class PinChangePageModule {}
