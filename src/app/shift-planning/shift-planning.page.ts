import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataProvider } from '../../providers/DataProvider';
import { APIProvider } from '../../providers/APIProvider';
import { Shift } from '../../models/Shift';
import { registerLocaleData, WeekDay } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../providers/SettingsProvider';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { TimeTrackingProvider } from '../../providers/TimeTrackingProvider';
import { SelectMonthPage } from './modals/select-month/select-month.page';
import { ShiftDetailPage } from '../modal-pages/shift-detail/shift-detail.page';
registerLocaleData(localeDe, 'de');

@Component({
	selector: 'app-shift-planning',
	templateUrl: './shift-planning.page.html',
	styleUrls: ['./shift-planning.page.scss']
})
export class ShiftPlanningPage implements OnInit {
	public _shifts: Shift[] = [];
	public _monthDates: DateTime[] = [];
	public selectedMonth: DateTime;
	public _currentDay;

	constructor(
		private _api: APIProvider,
		private _data: DataProvider,
		public _settings: SettingsProvider,
		public _lang: LanguageProvider,
		public _tracking: TimeTrackingProvider,
		private modalCon: ModalController
	) {}

	async ngOnInit() {
		const now = DateTime.now().setZone(this._settings.timeZone).setLocale(this._lang.locale);

		this.selectedMonth = now;

		let endOfMonth = now.endOf('month').setLocale(this._lang.locale).toISO();
		let nowISO = now.toISO();
		nowISO = nowISO.split('T')[0];
		this._currentDay = nowISO;
		console.log(this._currentDay);
		endOfMonth = endOfMonth.split('T')[0];

		//this.calculateMonthDays();

		this._shifts = await this._api.getShifts(nowISO, endOfMonth);
		console.log('In Shift-planning:');
		console.log(this._shifts);
	}

	public async selectMonth() {
		const modal = await this.modalCon.create({
			component: SelectMonthPage,
			componentProps: {
				_initalMonth: this.selectedMonth.month + ''
			},
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		await modal.present();

		modal.onWillDismiss().then(async (result) => {
			let startISO = '';
			let endISO = '';
			let endOfMonth;
			let startOfMonth;

			if (result.data != null) {
				console.log(result.data);
				if (result.data.monthNumber == DateTime.now().month) {
					endOfMonth = DateTime.now().endOf('month').setLocale(this._lang.locale).toISO();
					startOfMonth = DateTime.now().toISO();
					console.log('Selected Current Month');
					this.selectedMonth = DateTime.now();
				} else {
					endOfMonth = DateTime.fromObject(
						{
							day: 10,
							month: result.data.monthNumber, //ggf. +1 dran hängen
							year: DateTime.now().year
						},
						{
							locale: this._lang.locale,
							zone: this._settings.timeZone
						}
					)
						.endOf('month')
						.toISO();

					startOfMonth = DateTime.fromObject(
						{
							day: 10,
							month: result.data.monthNumber, //ggf. +1 dran hängen
							year: DateTime.now().year
						},
						{
							locale: this._lang.locale,
							zone: this._settings.timeZone
						}
					).startOf('month');
					console.log('selected different month');

					this.selectedMonth = startOfMonth;

					startOfMonth = startOfMonth.toISO();
				}

				console.log('start of month ISO: ' + startOfMonth);

				startISO = startOfMonth.split('T')[0];
				endISO = endOfMonth.split('T')[0];
				this._shifts = await this._api.getShifts(startISO, endISO);
			}
		});
	}

	public async openDetails(shift: Shift) {
		const modal = await this.modalCon.create({
			component: ShiftDetailPage,
			componentProps: {
				_shift: shift
			},
			cssClass: 'auto-height',
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		await modal.present();
	}
}
