import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectWeekPage } from './select-week.page';

const routes: Routes = [
  {
    path: '',
    component: SelectWeekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectWeekPageRoutingModule {}
