import { Component, Input, OnInit } from '@angular/core';
import { News } from 'src/models/News';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { ModalController } from '@ionic/angular';
import { DataProvider } from '../../../../providers/DataProvider';

@Component({
	selector: 'app-news-detail',
	templateUrl: './news-detail.page.html',
	styleUrls: ['./news-detail.page.scss']
})
export class NewsDetailPage implements OnInit {
	@Input() news: News = new News();

	constructor(public _lang: LanguageProvider, private modalCon: ModalController, private data: DataProvider) {}

	ngOnInit() {}

	changeNewsMarker() {
		this.news.isMarked = !this.news.isMarked;
		this.data.setStorage('NewsMarked-' + this.news.id, this.news.isMarked);
	}

	DismissModal() {
		this.modalCon.dismiss({ changed: false }, 'canceled');
	}
}
