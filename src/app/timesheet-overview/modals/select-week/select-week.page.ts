import { Component, OnInit, Input } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { DateTime } from 'luxon';
import { ModalController } from '@ionic/angular';
import { SettingsProvider } from '../../../../providers/SettingsProvider';

@Component({
	selector: 'app-select-week',
	templateUrl: './select-week.page.html',
	styleUrls: ['./select-week.page.scss'],
})
export class SelectWeekPage implements OnInit {
	@Input() _initalWeek = '';
	public _weeks: weekSelection[] = [];
	private _selectedWeek: string = '';

	constructor(
		public _lang: LanguageProvider,
		private modal: ModalController,
		private _settings: SettingsProvider
	) {}

	async ngOnInit() {
		let i = DateTime.now().setLocale('de-DE');
		let j = i.weekNumber;
		while (j > 0) {
			let start: DateTime = null;
			let end: DateTime = null;

			if (i.weekday == 7) {
				end = DateTime.fromISO(i, {
					zone: this._settings.timeZone,
					locale: 'de-DE',
				});
			} else if (i.weekday == 1) {
				start = DateTime.fromISO(i, {
					zone: this._settings.timeZone,
					locale: 'de-DE',
				});
			}

			if (start == null) {
				start = i.minus((i.weekday - 1) * 86400000);
			}
			if (end == null) {
				end = i.plus((7 - i.weekday) * 86400000);
			}

			this._weeks.push({
				display:
					(i.weekNumber < 10 ? '0' + i.weekNumber : i.weekNumber) +
					' (' +
					start.toFormat('dd.MM.') +
					' - ' +
					end.toFormat('dd.MM') +
					')',
				value: i.weekNumber,
			});
			i = i.minus(604800000);
			j--;
		}
		this.selectedWeek = this._initalWeek;
	}

	/**
	 * Getter selectedWeek
	 * @return {string }
	 */
	public get selectedWeek(): string {
		return this._selectedWeek;
	}

	/**
	 * Setter selectedWeek
	 * @param {string } value
	 */
	public set selectedWeek(value: string) {
		this._selectedWeek = value;
		this.modal.dismiss({ weekNumber: this.selectedWeek });
	}
}
interface weekSelection {
	display: string;
	value: string;
}
