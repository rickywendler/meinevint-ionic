import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainMenuPage } from './main-menu.page';
import { NewsfeedPage } from "../newsfeed/newsfeed.page";

const routes: Routes = [
	{
		path: '',
		component: MainMenuPage
	},
	{
		path: 'holiday-account',
		loadChildren: () =>
			import('../main-menu/pages/holiday-account/holiday-account.module').then((m) => m.HolidayAccountPageModule)
	},
	{
		path: 'worktime-account',
		loadChildren: () =>
			import('../main-menu/pages/worktime-account/worktime-account.module').then((m) => m.WorktimeAccountPageModule)
	},
	{
		path: 'newsfeed',
		loadChildren: () =>
			import('../newsfeed/newsfeed.module').then((m) => m.NewsfeedPageModule)
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainMenuPageRoutingModule {}
