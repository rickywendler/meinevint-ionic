import { Component, Input, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../../../providers/LanguageProvider';
import { Booking } from '../../../../../../models/Booking';
import { TrackingStamp } from '../../../../../../models/TrackingStamp';
import { Project } from '../../../../../../models/Project';
import { Activity } from '../../../../../../models/Activity';
import { DateTime } from 'luxon';
import { APIProvider } from '../../../../../../providers/APIProvider';
import { AlertProvider } from '../../../../../../providers/AlertProvider';
import { PopoverController, NavController } from '@ionic/angular';
import { SelectTimePopoverComponent } from 'src/app/components/select-time-popover/select-time-popover.component';
import { v4 as uuidv4 } from 'uuid';
import { SettingsProvider } from '../../../../../../providers/SettingsProvider';
import { EmployeeDataProvider } from '../../../../../../providers/EmployeeDataProvider';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-create-timestamp',
	templateUrl: './create-timestamp.page.html',
	styleUrls: ['./create-timestamp.page.scss']
})
export class CreateTimestampPage implements OnInit {
	public stamps: StampWrapper[] = [];
	public projects: Project[] = [];
	public activities: Activity[] = [];

	private date: DateTime = null;
	private bookings: Booking[] = [];
	private projectList: object[];

	private keyboardEventHelper: boolean = false;
	public isKeyboardShown: boolean = false;

	constructor(
		public _lang: LanguageProvider,
		private api: APIProvider,
		private alertPro: AlertProvider,
		private popoverCon: PopoverController,
		private employee: EmployeeDataProvider,
		public settings: SettingsProvider,
		private route: ActivatedRoute,
		private navCon: NavController
	) {
		this.route.queryParams.subscribe((params) => {
			if (Object.keys(params).length > 0) {
				this.date = DateTime.fromISO(params.date);
			} else {
				this.date = DateTime.now();
			}
		});
		window.addEventListener('keyboardWillShow', () => {
			if (this.keyboardEventHelper) {
				this.isKeyboardShown = true;
			}
		});
		window.addEventListener('keyboardWillHide', () => {
			if (this.keyboardEventHelper) {
				this.isKeyboardShown = false;
			}
		});
	}

	async ngOnInit() {
		this.stamps = [];
		this.stamps.push(new StampWrapper());
		this.projects = this.employee.projects;
		this.activities = this.employee.activities;
		this.projectList = [];

		this.projects.forEach((p) => {
			this.projectList.push({ _name: p.name, _value: p.id });
		});

		//console.log("Current Platform: " + this._settings.platformClassModifier("bottom-fixed"));
	}

	goBack() {
		this.navCon.pop();
	}

	async ionViewWillEnter() {
		this.keyboardEventHelper = true;
		this.bookings = await this.api.getBookings(this.date.toJSDate());
	}

	async ionViewWillLeave() {
		this.keyboardEventHelper = false;
	}

	async editProject(stamp: StampWrapper) {
		const modalData = await this.alertPro.ShowListModal(
			this.projectList,
			this._lang._dict.TimeTracking.ChooseProject,
			stamp.project ? stamp.project.id : null,
			false
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			stamp.project = this.projects.find((p) => p.id == modalData.data._value);
			if (stamp.activity) {
				if (stamp.activity.projectId != stamp.project.id) {
					stamp.activity = this.activities.find(
						(a) => a.name == stamp.activity.name && a.projectId == stamp.project.id
					);
					if (!stamp.activity) {
						stamp.activity = this.activities.find((a) => a.name == stamp.activity.name && a.projectId == 0);
					}
				}
			}
		}
	}

	async editActivity(stamp: StampWrapper) {
		let activityList = [];

		if (stamp.project != null) {
			this.activities.forEach((a) => {
				if (a.projectId == stamp.project.id) {
					activityList.push({ _name: a.name, _value: a.id });
				}
			});
		}

		if (activityList.length == 0) {
			this.activities.forEach((a) => {
				if (a.projectId == 0) {
					activityList.push({ _name: a.name, _value: a.id });
				}
			});
		}

		if (activityList.length != 0) {
			const modalData = await this.alertPro.ShowListModal(
				activityList,
				this._lang._dict.TimeTracking.ChooseActivity,
				stamp.activity ? stamp.activity.id : null,
				false
			);

			if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
				stamp.activity = this.activities.find((a) => a.id == modalData.data._value);
			}
		}
	}

	public async editTimestamp(e, isStart, stamp: StampWrapper) {
		let initialValue;
		if (isStart) {
			if (stamp.start) {
				initialValue = stamp.start;
			} else {
				initialValue = DateTime.fromObject({
					day: this.date.day,
					month: this.date.month,
					year: this.date.year,
					hour: 8,
					minute: 0,
					second: 0,
					millisecond: 0
				});
			}
		} else {
			if (stamp.end) {
				initialValue = stamp.end;
			} else {
				initialValue = DateTime.fromObject({
					day: this.date.day,
					month: this.date.month,
					year: this.date.year,
					hour: stamp.start ? stamp.start.hour : 8,
					minute: stamp.start ? stamp.start.minute : 0,
					second: 0,
					millisecond: 0
				});
			}
		}

		const popover = await this.popoverCon.create({
			component: SelectTimePopoverComponent,
			event: e,
			reference: e.target,
			alignment: 'center',
			size: 'auto',
			arrow: true,
			componentProps: {
				_time: initialValue
			}
		});

		await popover.present();

		const { data, role } = await popover.onDidDismiss();
		if (role == 'success') {
			if (isStart) {
				stamp.start = DateTime.fromISO(data.time);
			} else {
				stamp.end = DateTime.fromISO(data.time);
			}
		}
	}

	noteChanged(e, stamp: StampWrapper) {
		stamp.note = e.detail.value;
	}

	async addnote(stamp: StampWrapper, removeNote = false) {
		stamp.noteIsAdded = !removeNote;
	}

	async savenote(stamp: StampWrapper) {
		stamp.noteIsAdded = true;
	}

	async deleteStamp(first: boolean, last: boolean, index: number) {
		const success = await this.alertPro.ShowAreYouSureAlert(this._lang._dict.TimeSheet.EditStamp.DeleteStampAlert);

		if (success) {
			// TODO: remove stamp from array
			if (first) {
				this.stamps = this.stamps.slice(1, undefined);
			} else if (last) {
				this.stamps.pop();
			} else {
				this.stamps = [].concat(this.stamps.slice(0, index), this.stamps.slice(index + 1, undefined));
			}
		}
	}

	addStamp(last: boolean, index: number) {
		if (last) {
			this.stamps.push(new StampWrapper());
		} else {
			this.stamps = [].concat(
				this.stamps.slice(0, index + 1),
				[new StampWrapper()],
				this.stamps.slice(index + 1, undefined)
			);
		}
	}

	saveChanges() {
		this.stamps.forEach((s) => {
			const start = DateTime.fromObject(
				{
					day: this.date.day,
					month: this.date.month,
					year: this.date.year,
					hour: s.start.hour,
					minute: s.start.minute,
					second: 0,
					millisecond: 0
				},
				{ zone: this.settings.timeZone }
			);
			const end = DateTime.fromObject(
				{
					day: this.date.day,
					month: this.date.month,
					year: this.date.year,
					hour: s.end.hour,
					minute: s.end.minute,
					second: 0,
					millisecond: 0
				},
				{ zone: this.settings.timeZone }
			);

			this.api.saveTrackingStamp(
				new TrackingStamp(null, start, end, s.project.id, s.activity != null ? s.activity.id : null)
			);

			if (s.noteIsAdded) {
				s.note = s.note.trim();
				if (s.note.length == 0) {
					s.note = null;
				}

				let b = new Booking(
					false,
					this.settings.timeZone,
					this.date.toJSDate(),
					s.start,
					s.end,
					s.project.id,
					s.activity != null ? s.activity.id : null
				);
				b.description = s.note;

				this.api.assignDescriptionToBooking(b);
			}
		});
		this.navCon.pop();
	}

	public get isSaveButtonDisabled(): boolean {
		for (const index in this.stamps) {
			let stamp = this.stamps[index];
			if (stamp != null) {
				if (this.projects ? this.projects.length > 0 : false) {
					if (stamp.project == null) {
						return true;
					}
				}

				if (stamp.end && stamp.start) {
					const endIsAfterStart = stamp.end.diff(stamp.start, 'milliseconds').toObject().milliseconds > 0;
					if (!endIsAfterStart) {
						return true;
					}
					const now = DateTime.fromObject({});
					if (this.date.day == now.day && this.date.month == now.month && this.date.year == now.year) {
						const startIsBevoreNow = stamp.start.diff(now, 'milliseconds').toObject().milliseconds < 0;
						if (!startIsBevoreNow) {
							return true;
						}
						const endIsBevoreNow = stamp.end.diff(now, 'milliseconds').toObject().milliseconds < 0;
						if (!endIsBevoreNow) {
							return true;
						}
					}
				} else {
					return true;
				}
			}
		}
		return false;
	}

	public get isDeleteButtonDisabled(): boolean {
		return this.stamps.length == 1;
	}
}

class StampWrapper {
	public project: Project;
	public activity: Activity;
	public start: DateTime;
	public end: DateTime;
	public note: string;
	public noteIsAdded: boolean;

	constructor(project?, activity?, start?, end?, note?, noteIsAdded?) {
		this.project = project ?? new Project;
		this.activity = activity ?? null;
		this.start = start ?? null;
		this.end = end ?? null;
		this.note = note ?? null;
		this.noteIsAdded = noteIsAdded ?? false;
	}
}
