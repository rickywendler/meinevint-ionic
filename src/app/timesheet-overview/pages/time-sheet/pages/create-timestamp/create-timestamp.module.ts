import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateTimestampPageRoutingModule } from './create-timestamp-routing.module';

import { CreateTimestampPage } from './create-timestamp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateTimestampPageRoutingModule
  ],
  declarations: [CreateTimestampPage]
})
export class CreateTimestampPageModule {}
