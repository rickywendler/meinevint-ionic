import { DateTime } from 'luxon';

export class News {
	private _id: number;

	private _isRead: boolean;
	private _isToday: boolean;

	private _date: DateTime;
	private _message: string;
	private _sender: string;

	private _isMarked: boolean;

	constructor(
		id?: number,
		date?: DateTime,
		message?: string,
		sender?: string,
		isMarked?: boolean,
		isRead?: boolean
	) {
		this._id = id ?? null;
		this._isRead = isRead ?? null;
		this._isMarked = isMarked ?? null;
		this._sender = sender ?? null;
		this._message = message ?? null;
		this._date = date ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Getter isRead
	 * @return {boolean}
	 */
	public get isRead(): boolean {
		if (!this._isRead) this._isRead = false;
		return this._isRead;
	}

	/**
	 * Getter isToday
	 * @return {boolean}
	 */
	public get isToday(): boolean {
		if (!this._isToday) {
			const now = DateTime.now();
			this._isToday =
				now.day == this._date.day && this._date.month == now.month && now.year == this._date.year;
		}
		return this._isToday;
	}

	/**
	 * Getter date
	 * @return {DateTime}
	 */
	public get date(): DateTime {
		return this._date;
	}

	/**
	 * Getter message
	 * @return {string}
	 */
	public get message(): string {
		return this._message;
	}

	/**
	 * Getter sender
	 * @return {string}
	 */
	public get sender(): string {
		return this._sender;
	}

	/**
	 * Getter isMarked
	 * @return {boolean}
	 */
	public get isMarked(): boolean {
		return this._isMarked;
	}

	/**
	 * Getter initals
	 * @return {string}
	 */
	public get initals(): string {
		return this._sender.charAt(0) + this._sender.split(' ')[1].charAt(0);
	}

	/**
	 * Setter isRead
	 * @param {boolean} value
	 */
	public set isRead(value: boolean) {
		this._isRead = value;
	}

	/**
	 * Setter isToday
	 * @param {boolean} value
	 */
	public set isToday(value: boolean) {
		this._isToday = value;
	}

	/**
	 * Setter date
	 * @param {DateTime} value
	 */
	public set date(value: DateTime) {
		this._date = value;
	}

	/**
	 * Setter message
	 * @param {string} value
	 */
	public set message(value: string) {
		this._message = value;
	}

	/**
	 * Setter sender
	 * @param {string} value
	 */
	public set sender(value: string) {
		this._sender = value;
	}

	/**
	 * Setter isMarked
	 * @param {boolean} value
	 */
	public set isMarked(value: boolean) {
		this._isMarked = value;
	}
}
