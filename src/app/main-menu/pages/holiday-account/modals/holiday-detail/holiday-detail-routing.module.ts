import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HolidayDetailPage } from './holiday-detail.page';

const routes: Routes = [
  {
    path: '',
    component: HolidayDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HolidayDetailPageRoutingModule {}
