import { HolidayDay } from './HolidayDay';
import { Period } from './Period';
import { DateTime } from 'luxon';

export class HolidayRequest {
	private _id: number;

	private _costCenter: string;
	private _owner: string;
	private _pnr: string;

	private _status: number;
	private _statusType: string;

	private _period: Period[];

	private _days: HolidayDay[];

	private _requestedBy: string;
	private _requestedOn: Date;
	private _requestedLocation: string;

	private _requestComment: string;
	private _responseComment: string;

	private _countOfHolidays: number = null;
	private _firstDay: Date = null;
	private _lastDay: Date = null;

	constructor(
		id?: number,
		costCenter?: string,
		owner?: string,
		pnr?: string,
		status?: number,
		statusType?: string,
		period?: Period[],
		days?: HolidayDay[],
		requestedBy?: string,
		requestedOn?: Date,
		requestedLocation?: string,
		requestComment?: string,
		responseComment?: string
	) {
		this._id = id ?? null;
		this._costCenter = costCenter ?? null;
		this._owner = owner ?? null;
		this._pnr = pnr ?? null;
		this._status = status ?? null;
		this._statusType = statusType ?? null;
		this._period = period ?? null;
		this._days = days ?? null;
		this._requestedBy = requestedBy ?? null;
		this._requestedOn = requestedOn ?? null;
		this._requestedLocation = requestedLocation ?? null;
		this._requestComment = requestComment ?? null;
		this._responseComment = responseComment ?? null;
	}

	/**
	 * Getter id
	 * @return {number}
	 */
	public get id(): number {
		return this._id;
	}

	/**
	 * Setter id
	 * @param {number} value
	 */
	public set id(value: number) {
		this._id = value;
	}

	/**
	 * Getter responseComment
	 * @return {string}
	 */
	public get responseComment(): string {
		return this._responseComment;
	}

	/**
	 * Setter responseComment
	 * @param {string } value
	 */
	public set responseComment(value: string) {
		this._responseComment = value;
	}

	/**
	 * Getter requestComment
	 * @return {string}
	 */
	public get requestComment(): string {
		return this._requestComment;
	}

	/**
	 * Setter requestedComment
	 * @param {string } value
	 */
	public set requestComment(value: string) {
		this._requestComment = value;
	}

	/**
	 * Getter requestLocation
	 * @return {string}
	 */
	public get requestedLocation(): string {
		return this._requestedLocation;
	}

	/**
	 * Setter requestedLocation
	 * @param {string } value
	 */
	public set requestedLocation(value: string) {
		this._requestedLocation = value;
	}

	/**
	 * Getter requestedOn
	 * @return {Date}
	 */
	public get requestedOn(): Date {
		return this._requestedOn;
	}

	/**
	 * Setter requestedOn
	 * @param {string } value
	 */
	public set requestedOn(value: Date) {
		this._requestedOn = value;
	}

	/**
	 * Getter requestedBy
	 * @return {string}
	 */
	public get requestedBy(): string {
		return this._requestedBy;
	}

	/**
	 * Setter requestedBy
	 * @param {string } value
	 */
	public set requestedBy(value: string) {
		this._requestedBy = value;
	}

	/**
	 * Getter days
	 * @return {HolidayDay[]}
	 */
	public get days(): HolidayDay[] {
		return this._days;
	}

	/**
	 * Setter days
	 * @param {HolidayDay[] } value
	 */
	public set days(value: HolidayDay[]) {
		this._days = value;
	}

	/**
	 * Getter period
	 * @return {Period[]}
	 */
	public get period(): Period[] {
		return this._period;
	}

	/**
	 * Setter period
	 * @param {Period[] } value
	 */
	public set period(value: Period[]) {
		this._period = value;
	}

	/**
	 * Getter statusType
	 * @return {string}
	 */
	public get statusType(): string {
		return this._statusType;
	}

	/**
	 * Setter statusType
	 * @param {string } value
	 */
	public set statusType(value: string) {
		this._statusType = value;
	}

	/**
	 * Getter status
	 * @return {number}
	 */
	public get status(): number {
		return this._status;
	}

	/**
	 * Setter status
	 * @param {number } value
	 */
	public set status(value: number) {
		this._status = value;
	}

	/**
	 * Getter pnr
	 * @return {string}
	 */
	public get pnr(): string {
		return this._pnr;
	}

	/**
	 * Setter pnr
	 * @param {string } value
	 */
	public set pnr(value: string) {
		this._pnr = value;
	}

	/**
	 * Getter owner
	 * @return {string}
	 */
	public get owner(): string {
		return this._owner;
	}

	/**
	 * Setter owner
	 * @param {string } value
	 */
	public set owner(value: string) {
		this._owner = value;
	}

	/**
	 * Getter costCenter
	 * @return {string}
	 */
	public get costCenter(): string {
		return this._costCenter;
	}

	/**
	 * Setter costCenter
	 * @param {string } value
	 */
	public set costCenter(value: string) {
		this._costCenter = value;
	}

	/**
	 * Getter countOfHolidays
	 * @return {number}
	 */
	public get countOfHolidays(): number {
		if (this._countOfHolidays == null) {
			this._days
				.filter((d) => d.typeId == 1)
				.forEach((day) => {
					this._countOfHolidays += -day.amount;
				});
		}
		return this._countOfHolidays;
	}

	/**
	 * Getter firstDay
	 * @return {Date}
	 */
	public get firstDay(): Date {
		if (this._firstDay != null) {
			return this._firstDay;
		}

		this._firstDay = this.period[0].start;
		this._period.forEach((p) => {
			if (p.start.valueOf() < this._firstDay.valueOf()) {
				this._firstDay = p.start;
			}
		});
		return this._firstDay;
	}

	/**
	 * Getter lastDay
	 * @return {Date}
	 */
	public get lastDay(): Date {
		if (this._lastDay != null) {
			return this._lastDay;
		}

		this._lastDay = this.period[0].start;
		this._period.forEach((p) => {
			if (p.end.valueOf() > this._lastDay.valueOf()) {
				this._lastDay = p.end;
			}
		});
		return this._lastDay;
	}
}
