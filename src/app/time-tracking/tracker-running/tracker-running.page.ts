import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { TrackingStamp } from '../../../models/TrackingStamp';
import { AlertProvider } from '../../../providers/AlertProvider';
import { ModalController, GestureController, Gesture, IonRouterOutlet } from '@ionic/angular';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { DateTime } from 'luxon';
import { Booking } from '../../../models/Booking';
import { APIProvider } from '../../../providers/APIProvider';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { Time } from '@angular/common';
import { Activity } from '../../../models/Activity';
import { Project } from '../../../models/Project';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { BookingDetailsPage } from '../../modal-pages/booking-details/booking-details.page';
import { TimeSheetPage } from '../../timesheet-overview/pages/time-sheet/time-sheet.page';
import { DayHistoryPage } from '../pages/day-history/day-history.page';
import { TimeTrackingProvider } from '../../../providers/TimeTrackingProvider';
import { EmployeeDataProvider } from '../../../providers/EmployeeDataProvider';

@Component({
	selector: 'app-tracker-running',
	templateUrl: './tracker-running.page.html',
	styleUrls: ['./tracker-running.page.scss']
})
export class TrackerRunningPage implements OnInit {
	@ViewChild('.startSlider') _startSlider: ElementRef;
	// public _trackingStamp: TrackingStamp;

	public _actualTime: string = DateTime.now().setLocale(this._lang.locale).toLocaleString({
		hour: '2-digit',
		minute: '2-digit',
		hourCycle: 'h23'
	});

	public _breakTime: string = DateTime.now().setLocale(this._lang.locale).toLocaleString({
		hour: '2-digit',
		minute: '2-digit',
		hourCycle: 'h23'
	});

	public _selectedProject;
	public _selectedActivity;
	public _availableProjects: Project[];
	public _availableActivities: Activity[];
	// public _breakActive = false;
	slideButton;

	private _allActivities: Activity[] = null;

	private sliderCalledOnce = false;

	constructor(
		private _alertProvider: AlertProvider,
		private _modalController: ModalController,
		public _lang: LanguageProvider,
		public _api: APIProvider,
		public _settings: SettingsProvider,
		private _employee: EmployeeDataProvider,
		private route: ActivatedRoute,
		private router: Router,
		public _tracker: TimeTrackingProvider,
		private _gestureCtrl: GestureController,
		private _routerOutlet: IonRouterOutlet
	) {
		this.route.queryParams.subscribe((params) => {
			if (this.router.getCurrentNavigation().extras.queryParams) {
				// this._trackingStamp = this.router.getCurrentNavigation().extras.queryParams.tracking;
				// this._trackingStamp.stopBreak();
			}
		});
	}

	async ngOnInit() {
		this._tracker.init();
		console.log('INIT LOL XXXXX');
		console.log(this._tracker);
		if (!this._tracker.isTracking && !this._tracker.isPause) {
			this._tracker.startTracking();
		}

		this._availableProjects = this._employee.projects;
		this._allActivities = this._employee.activities;

		this.assignActivitiesToSelect();

		// this.startTime();

		this._selectedProject = this._availableProjects.find((data) => data.id == this._tracker.projectId).name;
		const activityFound = this._allActivities.find((data) => data.id == this._tracker.activityId);
		this._selectedActivity = activityFound != null ? activityFound.name : this._selectedActivity;
	}

	public async resumeTimer() {
		if (this.projectsAreAvailable && !this._tracker.onlyQrIsUsed) {
			const projectAssigned = await this.assignProject();
			if (this._availableActivities.length > 0 && projectAssigned) {
				const activ = await this._availableActivities.find(
					(a) => a.projectId == this._tracker.projectId || a.projectId == 0
				);

				if (activ != undefined) {
					await this.changeSelectedActivity();
					//
				}
			}
		}

		this._tracker.stopBreak(true);

		this._alertProvider.ShowAlertPrompt('trackerRunningAlert', '', 'none');
		// this.startTime();
		this.InitSlider();
	}

	ionViewDidEnter() {
		this._routerOutlet.swipeGesture = false;
		//this.calculateBreakTime();
		this.InitSlider();
	}

	ionViewDidLeave() {
		this._routerOutlet.swipeGesture = true;
	}

	//Public Methods

	public stopTracking(isPause = false) {
		if (this._tracker.isTracking) {
			// this._trackingStamp.stopped = DateTime.fromObject({}, { zone: this._settings.timeZone });
			if (isPause) {
				this._tracker.startBreak();
				this._alertProvider.ShowAlertPrompt(
					'trackerRunningAlert',
					this._lang._dict.TimeTracking.AlertInPause,
					'warning',
					'warning'
				);
			} else {
				this._tracker.stopTracking();
				let navExtras: NavigationExtras = {
					queryParams: {
						projectName: this._selectedProject,
						activityName: this._selectedActivity,
						init: true
					},
					replaceUrl: true
				};
				//this._modalController.dismiss();
				this.router.navigate(['/app/tabs/time-tracking/finished-day'], navExtras);
			}
			/*
				this._api.saveTrackingStamp(this._trackingStamp).then(() => {
					this._trackingStamp.started = null;
					if (!isPause) {
						let navExtras: NavigationExtras = {
							queryParams: {
								projectName: this._selectedProject,
								activityName: this._selectedActivity,
							},
						};
						//this._modalController.dismiss();
						this.router.navigate(['/app/tabs/time-tracking/finished-day'], navExtras);
					} else {
						this._breakActive = true;
						this._trackingStamp.breakStarted = DateTime.fromObject(
							{},
							{ zone: this._settings.timeZone }
						);
						this._trackingStamp.startBreak();
						this._alertProvider.ShowAlertPrompt(
							this._lang._dict.TimeTracking.AlertInPause,
							'warning'
						);
					}
				});
				*/
		}
	}

	public async assignProject(): Promise<boolean> {
		let projectList = [];

		this._availableProjects.forEach((a) => {
			projectList.push({ _name: a.name, _value: a.id });
		});

		const modalData = await this._alertProvider.ShowListModal(
			projectList,
			this._lang._dict.TimeTracking.ChooseProject,
			this._tracker.projectId,
			true
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._selectedProject = modalData.data._name ?? null;
			this._tracker.projectId = modalData.data._value ?? null;
			this._tracker.activityId = null;
			this._selectedActivity = null;
			this.assignActivitiesToSelect();
			return true;
		}
		return false;
	}

	public async changeSelectedActivity() {
		let activityList = [];

		this._availableActivities.forEach((a) => {
			activityList.push({ _name: a.name, _value: a.id });
		});
		const modalData = await this._alertProvider.ShowListModal(
			activityList,
			this._lang._dict.TimeTracking.ChooseActivity,
			this._tracker.activityId,
			true
		);

		if (modalData.role != 'backdrop' && modalData.role != 'gesture' && modalData.data != undefined) {
			this._selectedActivity = modalData.data._name ?? null;
			this._tracker.activityId = modalData.data._value ?? null;
		}
	}

	//Private Methods

	private assignActivitiesToSelect() {
		this._availableActivities = [];
		let value = 0;

		if (this._tracker.projectId != null) {
			value = this._tracker.projectId;
		}

		this._allActivities.forEach((activity) => {
			if (activity.projectId == value) {
				this._availableActivities.push(activity);
			}
		});

		if (this._availableActivities.length == 0) {
			this._allActivities.forEach((activity) => {
				if (activity.projectId == 0) {
					this._availableActivities.push(activity);
				}
			});
		}
	}

	public async goToBooking() {
		/*let navExtras: NavigationExtras = {
			queryParams: {
				bookingDate: new Date(),
			},
		};

		this.router.navigate(['/app/tabs/timesheet-overview/timesheet'], navExtras); */

		const modal = await this._modalController.create({
			component: DayHistoryPage,
			canDismiss: true,
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		modal.present();
	}

	checkForSlider(detail) {
		if (detail.startX < 100) {
			this.slideButton.style.left = detail.currentX - 40 + 'px';
			if (detail.currentX > 280 && !this.sliderCalledOnce) {
				this.sliderCalledOnce = true;
				//The slider was dragged to the right
				console.log('SliderCalledOnce: ' + this.sliderCalledOnce);
				this.stopTracking();
			}
		}
	}

	resetSlider() {
		this.slideButton.style.left = '5px';
		this.sliderCalledOnce = false;
	}

	InitSlider() {
		console.log('InitSlider called');
		this.sliderCalledOnce = false;
		setTimeout(() => {
			//Unfortunately, we have to wait for the breakactive variable to be set
			this.slideButton = document.querySelector('.end_sliderButton') as any;

			const gesture = this._gestureCtrl.create({
				el: document.querySelector('.endSlider'),
				gestureName: 'test',
				onMove: (detail) => this.checkForSlider(detail),
				onEnd: (detail) => this.resetSlider()
			});

			gesture.enable();
		}, 100);
	}

	getDate() {
		let today = new Date();
		let dd = String(today.getDate()).padStart(2, '0');
		let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		let yyyy = today.getFullYear();

		let dateString = dd + '.' + mm + '.' + yyyy;

		let options = new Intl.DateTimeFormat(this._lang.locale, { weekday: 'short' }).format(today);

		return options + ', ' + dateString;
	}

	public get projectsAreAvailable(): boolean {
		if (this._availableProjects == null) return false;
		return this._availableProjects.length > 0;
	}

	public totalWorkTime(): number {
		return this._tracker.totalWorkTime / 60000;
	}

	public totalBreakTime(): number {
		return this._tracker.totalBreakTime / 60000;
	}

	public get activitiesAreAvailable(): boolean {
		return this._availableActivities.length > 0;
	}
}
