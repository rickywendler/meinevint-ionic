import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimesheetOverviewPageRoutingModule } from './timesheet-overview-routing.module';

import { TimesheetOverviewPage } from './timesheet-overview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimesheetOverviewPageRoutingModule
  ],
  declarations: [TimesheetOverviewPage]
})
export class TimesheetOverviewPageModule {}
