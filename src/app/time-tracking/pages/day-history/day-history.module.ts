import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DayHistoryPageRoutingModule } from './day-history-routing.module';

import { DayHistoryPage } from './day-history.page';

@NgModule({
	imports: [CommonModule, FormsModule, IonicModule, DayHistoryPageRoutingModule],
	declarations: [DayHistoryPage],
})
export class DayHistoryPageModule {}
