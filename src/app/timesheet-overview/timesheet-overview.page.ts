import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { LanguageProvider } from '../../providers/LanguageProvider';
import { registerLocaleData, WeekDay } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { Booking } from '../../models/Booking';
import { APIProvider } from '../../providers/APIProvider';
import { SelectWeekPage } from './modals/select-week/select-week.page';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../providers/SettingsProvider';
registerLocaleData(localeDe, 'de');

@Component({
	selector: 'app-timesheet-overview',
	templateUrl: './timesheet-overview.page.html',
	styleUrls: ['./timesheet-overview.page.scss']
})
export class TimesheetOverviewPage implements OnInit {
	private _everythingCalculated = false;
	private _weekWorkTotalTime = 0;
	private _weekPauseTotalTime = 0;

	public _weekDates: DateTime[] = [];
	public _weekWorkTimeHours = 0;
	public _weekWorkTimeMinutes = 0;
	public _weekPauseTimeHours = 0;
	public _weekPauseTimeMinutes = 0;
	public _weekStart = DateTime.now().setZone(this._settings.timeZone).setLocale(this._lang.locale);
	public _weekEnd = DateTime.now().setZone(this._settings.timeZone).setLocale(this._lang.locale);
	public _displayedWeek = DateTime.now().setZone(this._settings.timeZone).setLocale(this._lang.locale);
	public _weekBookings: Booking[] = [];
	public _dayTimes: number[] = [];

	constructor(
		private modalCon: ModalController,
		private routernav: Router,
		public _lang: LanguageProvider,
		private _api: APIProvider,
		public _settings: SettingsProvider,
		private _platform: Platform
	) {}

	ngOnInit() {}

	ionViewWillEnter() {
		this.Init();
	}

	async Init() {
		if (!this._everythingCalculated) {
			this._weekStart = this._displayedWeek.startOf('week').setLocale(this._lang.locale);
			this._weekEnd = this._displayedWeek.endOf('week').setLocale(this._lang.locale);
			this._weekDates = [];
			this._weekWorkTotalTime = 0;
			this._weekWorkTimeHours = 0;
			this._weekWorkTimeMinutes = 0;

			this._weekPauseTotalTime = 0;
			this._weekPauseTimeHours = 0;
			this._weekPauseTimeMinutes = 0;

			this._dayTimes = [];

			this.createWeeksArrayForDetails().then((weekDates) => {
				this._weekDates = weekDates;
				this.calculateDurationForBookings();
			});

			this._everythingCalculated = true;
		}
	}

	/*Navigates to the timesheet page for the according day */
	async goToBooking(i) {
		let navExtras: NavigationExtras = {
			queryParams: {
				bookingDate: this._weekDates[i]
			}
		};

		this.routernav.navigate(['/app/tabs/timesheet-overview/timesheet'], navExtras);
		this._everythingCalculated = false;

		//this.navCtrl.navigateForward("/app/tabs/timesheetDetails");

		/*console.log(event);
		const modal = await this.modalCon.create({
			component: TimeSheetPage,
			componentProps: {
				_selectedDate: event
			},
			swipeToClose: true,
		});

		modal.present(); */
	}

	public async selectWeek() {
		const modal = await this.modalCon.create({
			component: SelectWeekPage,
			componentProps: {
				_initalWeek: this._displayedWeek.weekNumber + ''
			},
			breakpoints: [0, 1],
			initialBreakpoint: 1
		});

		await modal.present();

		modal.onWillDismiss().then((result) => {
			if (result.data != null) {
				const selectedWeek = result.data.weekNumber;
				this._displayedWeek = DateTime.fromObject(
					{
						weekYear: DateTime.now().year,
						weekNumber: selectedWeek
					},
					{ locale: this._lang.locale }
				);
				this._everythingCalculated = false;
				/*
				const weekDiff = this._displayedWeek.weekNumber - selectedWeek;
				this._displayedWeek = this._displayedWeek.minus(weekDiff * 604800000);

				const rightNow = DateTime.now();
				if (this._displayedWeek.valueOf() > rightNow.valueOf()) {
					this._displayedWeek = rightNow;
				} else {
					while (
						this._displayedWeek.weekday < 7 &&
						this._displayedWeek.valueOf() < rightNow.valueOf()
					) {
						this._displayedWeek = this._displayedWeek.endOf('week');
					}
				}
				*/
				this.Init();
			}
		});
	}

	public async addBooking(value: Booking, weekDay: DateTime) {
		this._weekBookings.push(value);
		if (value.isPauseEntry) {
			this._weekPauseTotalTime += value.duration / 1000;

			let hours = (this._weekPauseTotalTime / 3600).toString().split('.')[0];
			let minutes = ((this._weekPauseTotalTime % 3600) / 60).toString().split('.')[0];

			this._weekPauseTimeHours = +hours;
			this._weekPauseTimeMinutes = +minutes;
		} else {
			this._weekWorkTotalTime += value.duration / 1000;

			let hours = (this._weekWorkTotalTime / 3600).toString().split('.')[0];
			let minutes = ((this._weekWorkTotalTime % 3600) / 60).toString().split('.')[0];

			this._weekWorkTimeHours = +hours;
			this._weekWorkTimeMinutes = +minutes;
		}

		if (this._dayTimes[weekDay.toISO()] != null && this._dayTimes[weekDay.toISO()] != undefined) {
			this._dayTimes[weekDay.toISO()] += value.duration;
		} else {
			this._dayTimes[weekDay.toISO()] = value.duration;
		}
	}

	private async calculateDurationForBookings() {
		this._weekDates.forEach((weekDay: DateTime) => {
			this._api.getBookings(weekDay).then((bookingArray) => {
				bookingArray.forEach((element) => {
					if (element.start && element.end) this.addBooking(element, weekDay);
				});
			});
		});
	}

	private async createWeeksArrayForDetails(): Promise<DateTime[]> {
		let i = 0;
		let newDate: DateTime = null;
		let returnArray = [];
		let isoDate;
		const now = DateTime.now();
		if (this._weekEnd.valueOf() > now.valueOf()) {
			isoDate = now.toISO();
		} else {
			isoDate = this._weekEnd.toISO();
		}

		do {
			newDate = DateTime.fromISO(isoDate, {
				locale: this._lang.locale
			});

			newDate = newDate.minus({ days: i });

			returnArray.push(newDate);
			i++;
		} while (newDate.weekday > 1);
		return returnArray;
	}
}
