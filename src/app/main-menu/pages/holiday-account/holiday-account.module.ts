import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HolidayAccountPageRoutingModule } from './holiday-account-routing.module';

import { HolidayAccountPage } from './holiday-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HolidayAccountPageRoutingModule
  ],
  declarations: [HolidayAccountPage]
})
export class HolidayAccountPageModule {}
