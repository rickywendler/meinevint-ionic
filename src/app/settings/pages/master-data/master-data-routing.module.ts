import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterDataPage } from './master-data.page';

const routes: Routes = [
  {
    path: '',
    component: MasterDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterDataPageRoutingModule {}
