import { Component, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../../providers/LanguageProvider';
import { APIProvider } from '../../../../providers/APIProvider';
import { WorkTimeAccount } from '../../../../models/WorkTimeAccount';
import { Overtime } from '../../../../models/Overtime';
import { FTARequest } from '../../../../models/FTARequest';
import { ModalController, Platform } from '@ionic/angular';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../../providers/SettingsProvider';
import { WTA } from '../../../../models/WTA';
import { AlertProvider } from '../../../../providers/AlertProvider';

@Component({
	selector: 'app-worktime-account',
	templateUrl: './worktime-account.page.html',
	styleUrls: ['./worktime-account.page.scss'],
})
export class WorktimeAccountPage implements OnInit {
	//#region variables

	private _allWta: WTA[] = [];
	public _wta: WTA = new WTA();
	public _overtime: Overtime = new Overtime();
	public _ftaRequests: FTARequest[] = [];
	public _showFTARequests: boolean = false;
	public _now: DateTime = DateTime.now().setLocale('de').setZone(this._settings.timeZone);

	private _countOfPeriodWeeks = 0;
	private _selectedPeriod = 0;
	private _datesOfPeriods = [];
	private _predictedMinutes = null;

	//#endregion

	//#region system methods

	constructor(
		public _lang: LanguageProvider,
		private _api: APIProvider,
		private _modalCon: ModalController,
		private _platform: Platform,
		public _settings: SettingsProvider,
		private _alertProvider: AlertProvider
	) {}

	async ngOnInit() {
		this._wta = (await this._api.getWTA(this._now)) ?? new WTA();
		this._countOfPeriodWeeks = this._wta.periodEnd.diff(this._wta.periodStart, ['days']).days;
		this._datesOfPeriods = [];
	}

	async ionViewWillEnter() {}

	//#endregion

	//#region public methods

	public async showFTARequests() {
		if (this._showFTARequests) {
			this._showFTARequests = false;
		} else {
			this._ftaRequests = await this._api.getFTARequests();

			if (this._ftaRequests.length > 0) {
				this._showFTARequests = true;
			}
		}
	}

	public async selectPeriod() {
		if (this._datesOfPeriods.length == 0) {
			let i = 0;

			while (true) {
				let periodStart = this._wta.periodStart.minus({
					days: this._countOfPeriodWeeks * i,
				});
				let periodEnd = this._wta.periodEnd.minus({ days: this._countOfPeriodWeeks * i });
				if (periodStart.year != this._now.year) {
					break;
				}
				this._datesOfPeriods.push({
					_name: periodStart.toFormat('dd.MM.yyyy') + ' - ' + periodEnd.toFormat('dd.MM.yyyy'),
					_value: i,
				});
				i++;
			}
		}

		const modalData = await this._alertProvider.ShowListModal(
			this._datesOfPeriods,
			this._lang._dict.HolidayAccount.SelectYear,
			this._selectedPeriod
		);

		if (
			modalData.role != 'backdrop' &&
			modalData.role != 'gesture' &&
			modalData.data != undefined
		) {
			this._selectedPeriod = modalData.data._value;
			this._wta = await this._api.getWTA(
				this._now.minus({ days: this._countOfPeriodWeeks * this._selectedPeriod })
			);
		}
	}

	public get predictedMinutes(): number {
		if (this._predictedMinutes == null && this._wta.predicted != null) {
			const tail = ('0.' + this._wta.predicted.toString().split('.')[1]) as unknown as number;

			this._predictedMinutes = (tail * 60).toString().split('.')[0];
		}
		return this._predictedMinutes;
	}

	public showInfo(header, text) {
		this._alertProvider.ShowInfo(header, text);
	}

	//#endregion
}
