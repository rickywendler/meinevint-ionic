import { Component, Input, OnInit } from '@angular/core';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { ModalController } from '@ionic/angular';
import { Shift } from '../../../models/Shift';

@Component({
	selector: 'app-shift-detail',
	templateUrl: './shift-detail.page.html',
	styleUrls: ['./shift-detail.page.scss'],
})
export class ShiftDetailPage implements OnInit {
	@Input() _shift: Shift = new Shift();

	constructor(public _lang: LanguageProvider, private _modalCon: ModalController) {}

	ngOnInit() {}

	DismissModal() {
		this._modalCon.dismiss({ changed: false }, 'canceled');
	}
}
