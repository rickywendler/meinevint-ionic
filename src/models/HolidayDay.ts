export class HolidayDay {
	private _date: Date;
	private _type: string;
	private _typeId: number;
	private _amount: number;

	private _periodId: number;

	constructor(date?: Date, type?: string, typeId?: number, amount?: number, periodId?: number) {
		this._date = date ?? null;
		this._type = type ?? null;
		this._typeId = typeId ?? null;
		this._amount = amount ?? null;
		this._periodId = periodId ?? null;
	}

	/**
	 * Getter date
	 * @return {Date}
	 */
	public get date(): Date {
		return this._date;
	}

	/**
	 * Setter date
	 * @param {Date } value
	 */
	public set date(value: Date) {
		this._date = value;
	}

	/**
	 * Getter type
	 * @return {string}
	 */
	public get type(): string {
		return this._type;
	}

	/**
	 * Setter type
	 * @param {string } value
	 */
	public set type(value: string) {
		this._type = value;
	}

	/**
	 * Getter typeId
	 * @return {number}
	 */
	public get typeId(): number {
		return this._typeId;
	}

	/**
	 * Setter typeId
	 * @param {number } value
	 */
	public set typeId(value: number) {
		this._typeId = value;
	}

	/**
	 * Getter amount
	 * @return {number}
	 */
	public get amount(): number {
		return this._amount;
	}

	/**
	 * Setter ammount
	 * @param {number } value
	 */
	public set amount(value: number) {
		this._amount = value;
	}

	/**
	 * Getter periodId
	 * @return {number}
	 */
	public get periodId(): number {
		return this._periodId;
	}
}
