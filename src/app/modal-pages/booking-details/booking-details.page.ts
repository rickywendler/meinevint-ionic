import { Component, OnInit, Input } from '@angular/core';
import { Booking } from '../../../models/Booking';
import { APIProvider } from '../../../providers/APIProvider';
import { Project } from '../../../models/Project';
import { LanguageProvider } from '../../../providers/LanguageProvider';
import { Activity } from '../../../models/Activity';
import { IonSelect } from '@ionic/angular';
import { TrackingStamp } from '../../../models/TrackingStamp';
import { DateTime } from 'luxon';
import { SettingsProvider } from '../../../providers/SettingsProvider';
import { EmployeeDataProvider } from '../../../providers/EmployeeDataProvider';

@Component({
	selector: 'app-booking-details',
	templateUrl: './booking-details.page.html',
	styleUrls: ['./booking-details.page.scss'],
})
export class BookingDetailsPage implements OnInit {
	@Input() _selectedBooking: Booking;

	public _availableProjects: Project[];
	public _selectedProject: Project = new Project();

	public _availableActivities: Activity[];
	public _selectedActivity: Activity = new Activity();

	constructor(
		private _api: APIProvider,
		public _lang: LanguageProvider,
		private employee: EmployeeDataProvider,
		private _settings: SettingsProvider
	) {}

	async ionViewDidEnter() {}

	async ngOnInit() {
		this._availableProjects = this.employee.projects;
		this._availableActivities = this.employee.activities;

		this._selectedProject = this.employee.getProjectById(this._selectedBooking.projectId);
		this._selectedActivity = this.employee.getActivityById(this._selectedBooking.activityId);
	}

	/**
	 * Method for selected project changed event
	 * @param data event data
	 */
	selectedProjectChanged(data) {
		const projectSelect = data.target as HTMLIonSelectElement;
		this._selectedBooking.projectId = (projectSelect.value as number) ?? null;
	}

	/**
	 * Method for selected activity changed event
	 * @param data event data
	 */
	public selectedActivityChanged(data) {
		const activSelect = data.target as HTMLIonSelectElement;
		this._selectedBooking.activityId = (activSelect.value as number) ?? null;
	}

	CheckChanges() {
		console.log(this._selectedBooking);
	}

	async SaveChanges() {
		await this._api.deleteTimestamp(this._selectedBooking);
		if (this._selectedBooking.endStamp != null) {
			await this._api.deleteTimestamp(this._selectedBooking);
		}

		let saveStamp: TrackingStamp = new TrackingStamp();
		saveStamp.started = this._selectedBooking.start;
		saveStamp.stopped = this._selectedBooking.end;

		saveStamp.activityId = this._selectedBooking.activityId;
		saveStamp.projectId = this._selectedBooking.projectId;

		console.log('Savestamp: ');
		console.log(saveStamp);

		await this._api.saveTrackingStamp(saveStamp);
	}
}
